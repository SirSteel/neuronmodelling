from abc import abstractmethod, ABCMeta

from neuroflow.utilities import verify_type

from neuroflow.model.ensembles import NeuronsEnsemble, SynapsesEnsemble

from neuroflow.units import NeuronABC, SynapseABC

from neuroflow.model.model_manager.interfaces import \
    EnsembleFactoryInterface, NeuronsBuilderInterface, SynapsesBuilderInterface, \
    NeuronModifierInterface, SynapseModifierInterface, ModelStorageInterface


#########################
# Abstract Base Classes #
#########################

# region Abstract Base Classes


class EnsembleFactoryABC(EnsembleFactoryInterface, metaclass=ABCMeta):

    ####################
    # Concrete Methods #
    ####################

    # region Concrete Methods

    def create_neuron_ensemble(self, neuron_type):
        if not issubclass(neuron_type, NeuronABC):
            raise TypeError("Must pass class type of NeuronABC")

        type_name = neuron_type.__class__.__name__
        if type_name in self.synapse_ensembles:
            raise ValueError("The ensemble of this type already exists!")
        else:
            self.synapse_ensembles[type_name] = NeuronsEnsemble(neuron_type=neuron_type)

    def create_synapse_ensemble(self, synapse_type):
        if not issubclass(synapse_type, SynapseABC):
            raise TypeError("Must pass class type of SynapseABC")

        type_name = synapse_type.__class__.__name__
        if type_name in self.synapse_ensembles:
            raise ValueError("The ensemble of this type already exists!")
        else:
            self.synapse_ensembles[type_name] = SynapsesEnsemble(synapse_type=synapse_type)

    def _remove_ensemble(self, type_name, ensemble_type_name):
        if type_name in getattr(self, ensemble_type_name):
            del self.neuron_ensembles[type_name]
        else:
            error_msg = "No ensemble with the type" \
                        " {type} exists in {ens}!".format(type=type_name, ens=ensemble_type_name)

            raise ValueError(error_msg)

    def remove_neuron_ensemble(self, neuron_type: str):
        self._remove_ensemble(neuron_type, "synapse_ensembles")

    def remove_synapse_ensemble(self, synapse_type: str):
        self._remove_ensemble(synapse_type, "synapse_ensembles")

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region Abstract Methods

    @property
    @abstractmethod
    def neuron_ensembles(self) -> dict:
        raise NotImplementedError

    @property
    @abstractmethod
    def synapse_ensembles(self) -> dict:
        raise NotImplementedError

    # endregion


class NeuronsBuilderABC(NeuronsBuilderInterface, metaclass=ABCMeta):

    ####################
    # Concrete Methods #
    ####################

    # region Concrete Methods

    def add_neuron(self, neuron_obj) -> None:

        # first check if neuron object is an instance of NeuronABC
        verify_type("neuron_obj", neuron_obj, NeuronABC)

        # get the type of the neuron name
        neuron_type = neuron_obj.__class__.__name__

        # if such types already exist in the architect
        if neuron_type in self.neuron_ensembles:

            # if they already exist, add using fallback on the ensembles add method
            self.neuron_ensembles[neuron_type].add(neuron_obj)
        else:

            self.create_neuron_ensemble(neuron_obj.__class__)
            self.neuron_ensembles[neuron_type].add(neuron_obj)

    def create_neuron(self, neuron_type: str, neuron_name: str, **neuron_parameters):
        new_neuron_obj = self.neuron_factory.create_unit(neuron_type=neuron_type,
                                                         neuron_name=neuron_name,
                                                         **neuron_parameters)
        self.add_neuron(new_neuron_obj)
        return new_neuron_obj

    def add_neurons(self, neuron_obj_list) -> None:
        for neuron_obj in neuron_obj_list:
            self.add_neuron(neuron_obj)

    def create_neurons(self, neurons_tuple_list) -> list:
        created_neurons = list()
        for neuron_type, neuron_name, neuron_paramaters in neurons_tuple_list:
            curr_new_neuron = self.create_neuron(neuron_type=neuron_type,
                                                 neuron_name=neuron_name,
                                                 **neuron_paramaters)
            self.add_neuron(curr_new_neuron)
            created_neurons.append(curr_new_neuron)
        return created_neurons

    def remove_neuron(self, neuron_name) -> None:
        verify_type("neuron_name", neuron_name, str)

        if neuron_name not in self.neurons:
            raise KeyError("No such neuron exists!")

        # get the neurons type
        neuron_type = self.get_neuron_type(neuron_name)

        # clean its synapses  CLEANUP: Must happen first because of existance tests!
        self._clean_up(neuron_name)

        # finally remove the neuron as well
        self.neuron_ensembles[neuron_type].discard(neuron_name)

    def remove_neuron_type(self, neuron_type_name) -> None:
        verify_type("neuron_type_name", neuron_type_name, str)

        if neuron_type_name not in self.neuron_ensembles:
            raise KeyError("No such type exists in the architect!")
        else:
            self.remove_neuron_ensemble(neuron_type_name)

    def remove_neurons(self, neuron_names_list) -> None:
        """ removes all neurons from the list using the remove neuron function """
        for neuron_name in neuron_names_list:
            self.remove_neuron(neuron_name=neuron_name)

    def _clean_up(self, neuron_name) -> None:
        verify_type("neuron_name", neuron_name, str)

        if neuron_name not in self.neurons:
            raise KeyError("No such neuron exists in this architect!")
        else:
            self.remove_all_neuron_syn(neuron_name)

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region Abstract Methods

    @property
    @abstractmethod
    def neurons(self) -> dict:
        raise NotImplementedError

    @abstractmethod
    def get_neuron_type(self, neuron_name) -> str:
        raise NotImplementedError

    @property
    @abstractmethod
    def neuron_ensembles(self) -> dict:
        raise NotImplementedError

    @property
    @abstractmethod
    def neuron_factory(self):
        raise NotImplementedError

    @abstractmethod
    def create_neuron_ensemble(self, neuron_type):
        raise NotImplementedError

    @abstractmethod
    def remove_neuron_ensemble(self, neuron_type):
        raise NotImplementedError

    @abstractmethod
    def remove_all_neuron_syn(self, neuron_name):
        """ remove all incoming synapses for a neuron """
        raise NotImplementedError

    # endregion


class SynapsesBuilderABC(SynapsesBuilderInterface, metaclass=ABCMeta):

    ####################
    # Concrete Methods #
    ####################

    # region Concrete Methods

    def add_synapse(self, synapse_obj) -> None:

        # first check if neuron object is an instance of NeuronABC
        verify_type("neuron_obj", synapse_obj, SynapseABC)

        # get the type of the neuron name
        synapse_type = synapse_obj.__class__.__name__

        # if such types already exist in the architect
        if synapse_type in self.synapse_ensembles:

            # if they already exist, add using fallback on the ensembles add method
            self.synapse_ensembles[synapse_type].add(synapse_obj)
        else:

            self.create_synapse_ensemble(synapse_obj.__class__)
            self.synapse_ensembles[synapse_type].add(synapse_obj)

    def create_synapse(self, synapse_type: str, pre_neuron: str, post_neuron: str, **synapse_parameters):
        new_synapse_obj = self.synapse_factory.create_unit(synapse_type=synapse_type,
                                                           pre=pre_neuron,
                                                           post=post_neuron,
                                                           **synapse_parameters)
        self.add_synapse(new_synapse_obj)
        return new_synapse_obj

    def add_synapses(self, synapse_obj_list) -> None:
        for synapse_obj in synapse_obj_list:
            self.add_synapse(synapse_obj)

    def create_synapses(self, synapses_tuple_list) -> list:
        created_synapses = list()
        for synapse_type, pre_neuron, post_neuron, synapse_parameters in synapses_tuple_list:

            curr_new_neuron = self.create_synapse(synapse_type=synapse_type,
                                                  pre_neuron=pre_neuron,
                                                  post_neuron=post_neuron,
                                                  **synapse_parameters)
            self.add_synapse(curr_new_neuron)
            created_synapses.append(curr_new_neuron)
        return created_synapses

    def remove_synapse(self, synapse_id_namedtuple) -> None:
        # TODO: UID issues

        synapse_type, _, _, _ = synapse_id_namedtuple

        # finally remove the neuron as well
        self.synapse_ensembles[synapse_type].discard(synapse_id_namedtuple)

    def remove_connections(self, neuron1_name, neuron2_name):
        verify_type("neuron1_name", neuron1_name, str)
        verify_type("neuron2_name", neuron2_name, str)
        pass

    def remove_outgoing_neuron_syn(self, neuron_name):
        verify_type("neuron_name", neuron_name, str)
        pass

    def remove_incoming_neuron_syn(self, neuron_name):
        verify_type("neuron_name", neuron_name, str)
        pass

    def remove_recursive_neuron_syn(self, neuron_name):
        verify_type("neuron_name", neuron_name, str)
        pass

    def remove_all_neuron_syn(self, neuron_name):
        verify_type("neuron_name", neuron_name, str)

        if neuron_name in self.neurons:

            self.remove_recursive_neuron_syn(neuron_name=neuron_name)
            self.remove_incoming_neuron_syn(neuron_name=neuron_name)
            self.remove_outgoing_neuron_syn(neuron_name=neuron_name)

        else:
            raise KeyError("No such neuron found in the architect!")

    def remove_synapses(self, synapse_ids_list) -> None:
        for synapse_id in synapse_ids_list:
            self.remove_synapse(synapse_id_namedtuple=synapse_id)

    def remove_synapses_by_type(self, synapse_type_name) -> None:
        verify_type("synapse_type_name", synapse_type_name, str)

        if synapse_type_name not in self.synapse_ensembles:
            raise KeyError("No such type exists in the architect!")
        else:
            self.remove_synapse_ensemble(synapse_type_name)

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region Abstract Methods

    @property
    @abstractmethod
    def neurons(self) -> dict:
        raise NotImplementedError

    @abstractmethod
    def get_synapse_type(self, neuron_name) -> str:
        raise NotImplementedError

    @property
    @abstractmethod
    def synapse_ensembles(self) -> dict:
        raise NotImplementedError

    @property
    @abstractmethod
    def synapse_factory(self):
        raise NotImplementedError

    @abstractmethod
    def create_synapse_ensemble(self, neuron_type):
        raise NotImplementedError

    @abstractmethod
    def remove_synapse_ensemble(self, neuron_type):
        raise NotImplementedError

    @abstractmethod
    def get_neurons_all_synapses(self):
        raise NotImplementedError

    @abstractmethod
    def get_neurons_out_synapses(self):
        raise NotImplementedError

    @abstractmethod
    def get_neurons_in_synapses(self):
        raise NotImplementedError

    @abstractmethod
    def get_neurons_rec_synapses(self):
        raise NotImplementedError

    # endregion


class ModifierContextABC(metaclass=ABCMeta):
    def __init__(self):
        self._context_state = False

    def __enter__(self):
        # entering a with block, editing of neurons is enabled
        self.context_state = True

    def __exit__(self, exc_type, exc_val, exc_tb):
        # we want the getting of neurons to be disabled when not in a with block
        self.context_state = False

        if exc_type is not None:
            return False
        else:
            self.release_neurons()
            self.release_synapses()
            # TODO: grab(), and release(), need to be implemented

    @property
    def context_state(self):
        return self._context_state

    @context_state.setter
    def context_state(self, in_context_value):
        verify_type("in_context_value", in_context_value, bool)
        self._context_state = in_context_value

    """
    Just some example of a wrapper to understand it better
    def ntimes(n):
        def inner(f):
            def wrapper(*args, **kwargs):
                for _ in range(n):
                    print('running {.__name__}'.format(f))
                    rv = f(*args, **kwargs)
                return rv
            return wrapper
        return inner
    """

    @staticmethod
    def context_wrap():
        def inner(f):
            def wrapper(self, *args, **kwargs):
                if not self.context_state:
                    raise RuntimeError("This operation can only be performed within a with block of a context-manager!")
                else:
                    wrapd = f(self, *args, **kwargs)
                return wrapd
            return wrapper
        return inner

    @abstractmethod
    def release_neurons(self):
        # self.grabbed_neurons.add(grabbed_neuron)
        raise NotImplementedError

    @abstractmethod
    def release_synapses(self):
        # self.grabbed_neurons.add(grabbed_neuron)
        raise NotImplementedError


class NeuronModifierABC(NeuronModifierInterface, metaclass=ABCMeta):

    @ModifierContextABC.context_wrap()
    def get_neuron(self, neuron_name):
        verify_type("neuron_name", neuron_name, str)

        return self.grab_neuron(neuron_name)

    @ModifierContextABC.context_wrap()
    def mutate_neuron_type(self, neuron_type_name):
        verify_type("neuron_type_name", neuron_type_name, str)

        self.neuron_ensembles[neuron_type_name].mutate()
        # TODO: Make a mutation thing in the ensemble (fallback to neuron)

    @property
    @abstractmethod
    def neuron_ensembles(self) -> dict:
        raise NotImplementedError

    @abstractmethod
    def grab_neuron(self, neuron_name):
        # self.grabbed_neurons.add(grabbed_neuron)
        raise NotImplementedError

    @abstractmethod
    def release_neurons(self):
        # self.grabbed_neurons.add(grabbed_neuron)
        raise NotImplementedError


class SynapseModifierABC(SynapseModifierInterface, metaclass=ABCMeta):

    @ModifierContextABC.context_wrap()
    def get_synapse(self, synapse_id_namedtuple):

        # verify_type("synapse_id_namedtuple", synapse_id_namedtuple, tuple)

        return self.grab_synapse(synapse_id_namedtuple)

    @ModifierContextABC.context_wrap()
    def mutate_synapse_type(self, synapse_type_name):
        verify_type("synapse_type_name", synapse_type_name, str)

        self.synapse_ensembles[synapse_type_name].mutate()
        # TODO: Make a mutation thing in the ensemble (fallback to neuron)

    @property
    @abstractmethod
    def synapse_ensembles(self) -> dict:
        raise NotImplementedError

    @abstractmethod
    def grab_synapse(self, synapse_id_namedtuple):
        # self.grabbed_neurons.add(grabbed_neuron)
        raise NotImplementedError

    @abstractmethod
    def release_synapses(self):
        # self.grabbed_neurons.add(grabbed_neuron)
        raise NotImplementedError


class ModelHDF5StorageABC(ModelStorageInterface, metaclass=ABCMeta):

    def save_model(self):

        for neuron_type, ensemble_obj in self.neuron_ensembles.items():
            save_path = "/models/{model_name}/neurons_ensembles/{neuron_type}".format(model_name=self.model_name,
                                                                                      neuron_type=neuron_type)

            self.storage_manager.save_df(save_path, ensemble_obj.ensemble_df)

        for synapse_type, ensemble_obj in self.synapse_ensembles.items():
            save_path = "/models/{model_name}/synapses_ensembles/{synapse_type}".format(model_name=self.model_name,
                                                                                        synapse_type=synapse_type)

            self.storage_manager.save_df(save_path, ensemble_obj.ensemble_df)

    def load_model(self, model_name):
        pass
        # search_path = "/models/{model_name}/neurons_ensembles/"

        # self.storage_manager.load_df(df_name)

        # self.neuron_ensembles = neuron_ensembles
        # self.synapse_ensembles = synapse_ensembles

    @property
    @abstractmethod
    def model_name(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def storage_manager(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def neuron_ensembles(self) -> dict:
        raise NotImplementedError

    @neuron_ensembles.setter
    @abstractmethod
    def neuron_ensembles(self, neuron_ensembles):
        raise NotImplementedError

    @property
    @abstractmethod
    def synapse_ensembles(self) -> dict:
        raise NotImplementedError

    @synapse_ensembles.setter
    @abstractmethod
    def synapse_ensembles(self, synapse_ensembles) -> dict:
        raise NotImplementedError


# endregion

if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
