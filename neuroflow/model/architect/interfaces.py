from abc import abstractmethod, ABCMeta

##############################
# Interface Abstract Classes #
##############################

# region Interface Abstract Classes


class ModelBasicInterface(metaclass=ABCMeta):

    @property
    @abstractmethod
    def model_name(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def neuron_ensembles(self) -> dict:
        raise NotImplementedError

    @property
    @abstractmethod
    def synapse_ensembles(self) -> dict:
        raise NotImplementedError

    @property
    @abstractmethod
    def neurons(self) -> dict:
        raise NotImplementedError

    @abstractmethod
    def get_neuron_type(self, neuron_name) -> str:
        raise NotImplementedError


class EnsembleFactoryInterface(metaclass=ABCMeta):
    """ A architect core must be able to work with creations and deletions of the ensembles """

    @abstractmethod
    def create_neuron_ensemble(self, neuron_type):
        raise NotImplementedError

    @abstractmethod
    def create_synapse_ensemble(self, synapse_type):
        raise NotImplementedError

    @abstractmethod
    def remove_neuron_ensemble(self, neuron_type):
        raise NotImplementedError

    @abstractmethod
    def remove_synapse_ensemble(self, neuron_type):
        raise NotImplementedError


class NeuronsBuilderInterface(metaclass=ABCMeta):
    """  The builder interface base class, which defines the functionality the builder is responsible
        main responsibility is the addition and removal of neurons, if anything about the neurons creation, this
        architect should change too

        Addition

        - add neuron (add existing NeuronABC object)
        - create neuron (fall back to Synapse factory)
        - mass adding of neuron (list of NeuronABC objects)
        - mass creation of neurons (creating from a DataFrame) TODO: Must have checks

        Removal
        - removing neurons
        - mass removal of neurons
        - mass removal of neurons by type
        - clean up! (once a neuron is removed, remove all incoming and outgoing synapses of all types)

    """

    @abstractmethod
    def add_neuron(self, neuron_obj) -> None:
        """ Should add the neuron object to the architect"""
        raise NotImplementedError

    @abstractmethod
    def replace_neuron(self, neuron_obj):
        """ Should remove the previous neuron and replace it with this one """
        raise NotImplementedError

    @abstractmethod
    def create_neuron(self, *args, **kwargs) -> object: # returns the neuron object
        """ Should add the neuron to the architect and return its object """
        raise NotImplementedError

    @abstractmethod
    def add_neurons(self, neuron_obj_list) -> None:
        """ Should add neuron objects from that list """
        raise NotImplementedError

    @abstractmethod
    def create_neurons(self, neurons_df) -> list:
        """ Should add neurons and return their object instances as a list """
        raise NotImplementedError

    @abstractmethod
    def remove_neuron(self, neuron_name) -> None:
        """ Should remove a neuron by its name """
        raise NotImplementedError

    @abstractmethod
    def remove_neuron_type(self, neuron_type_name) -> None:
        """ Should remove all neurons of a specific type """
        raise NotImplementedError

    @abstractmethod
    def remove_neurons(self, neuron_names_list) -> None:
        """ Should remove all neurons from the list of names """
        raise NotImplementedError

    @abstractmethod
    def _clean_up(self, neuron_name) -> None:
        """ Should handle the cleaning up after neuron removal, removing the orphaned synapses """
        raise NotImplementedError


class SynapsesBuilderInterface(metaclass=ABCMeta):
    """  The architect interface base class, which defines the functionality the architect is responsible
        main responsibility is the management of synapses, if anything about the synapses changes, this
        architect should change too

        Addition

        - add synapse (add existing SynapseABC object)
        - create synapse (fall back to Synapse factory)
        - mass adding of synapses (list of SynapseABC objects)
        - mass creation of synapses (creating from a DataFrame) TODO: Must have checks

        Removal
        - removing synapses
        - mass removal of synapses
        - removing all synapses for a neuron
            - outgoing
            - incoming
            - recursive
            - all
        - removing all synapses of a specific type

    """

    @abstractmethod
    def add_synapse(self, synapse_obj) -> None:
        """ Should add the synapse object to the architect"""
        raise NotImplementedError

    @abstractmethod
    def replace_synapse(self, synapse_obj):
        """ Should remove the previous synapse and replace it with the new one """
        raise NotImplementedError

    @abstractmethod
    def create_synapse(self, *args, **kwargs) -> object:  # returns the neuron object
        """ Should add the synapse to the architect and return its object """
        raise NotImplementedError

    @abstractmethod
    def add_synapses(self, synapse_obj_list) -> None:
        """ Should add synapse objects from that list """
        raise NotImplementedError

    @abstractmethod
    def create_synapses(self, synapses_df) -> list:
        """ Should add synapses and return their object instances as a list """
        raise NotImplementedError

    @abstractmethod
    def remove_synapse(self, synapse_id_namedtuple) -> None:
        """ Should remove a synapse by its named id """
        raise NotImplementedError

    @abstractmethod
    def remove_connections(self, neuron1_name, neuron2_name):
        """ Should remove all synapses between the neurons """
        raise NotImplementedError

    @abstractmethod
    def remove_outgoing_neuron_syn(self, neuron_name):
        """ remove all outgoing synapses for a neuron """
        raise NotImplementedError

    @abstractmethod
    def remove_incoming_neuron_syn(self, neuron_name):
        """ remove all incoming synapses for a neuron """
        raise NotImplementedError

    @abstractmethod
    def remove_recursive_neuron_syn(self, neuron_name):
        """ remove all incoming synapses for a neuron """
        raise NotImplementedError

    @abstractmethod
    def remove_all_neuron_syn(self, neuron_name):
        """ remove all incoming synapses for a neuron """
        raise NotImplementedError

    @abstractmethod
    def remove_synapses(self, synapse_ids_list) -> None:
        """ Should remove all synapses from the list of synapse ids """
        raise NotImplementedError

    @abstractmethod
    def remove_synapses_by_type(self, synapse_type_name) -> None:
        """ Should remove all synapses of this type"""
        raise NotImplementedError


class NeuronModifierInterface(metaclass=ABCMeta):
    """  The modifier interface base class, which defines the functionality the modifier is responsible
        main responsibility is the modification of the neurons, which means an observer to check for changes

        Modification

        - pulling a neuron object out
        - changing neuron parameters
        - mass changing of parameters by type (DataFrame injection), related to mutation
            TODO: Related to parameter generation

    """

    @abstractmethod
    def __enter__(self):
        raise NotImplementedError

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError

    @abstractmethod
    def get_neuron(self, neuron_name):
        raise NotImplementedError

    @abstractmethod
    def mutate_neuron_type(self, neuron_type_name):
        raise NotImplementedError


class SynapseModifierInterface(metaclass=ABCMeta):
    """  The modifier interface base class, which defines the functionality the modifier is responsible
        main responsibility is the modification of the synapses, which means an observer to check for changes

        Modification

        - pulling a synapse object out
        - changing synapse parameters
        - mass changing of parameters by type (DataFrame injection), related to mutation
            TODO: Related to parameter generation

    """

    @abstractmethod
    def __enter__(self):
        raise NotImplementedError

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError

    @abstractmethod
    def get_synapse(self, synapse_id_namedtuple):
        raise NotImplementedError

    @abstractmethod
    def mutate_synapse_type(self, synapse_type_name):
        raise NotImplementedError


class ModelStorageInterface(metaclass=ABCMeta):

    @property
    @abstractmethod
    def storage_manager(self):
        raise NotImplementedError

    @abstractmethod
    def save_model(self):
        raise NotImplementedError

    @abstractmethod
    def load_model(self, model_name):
        raise NotImplementedError


class ModelArchitectInterface(ModelBasicInterface,
                              EnsembleFactoryInterface,
                              NeuronsBuilderInterface,
                              SynapsesBuilderInterface,
                              NeuronModifierInterface,
                              SynapseModifierInterface,
                              ModelStorageInterface,
                              metaclass=ABCMeta):
    pass

# endregion


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
