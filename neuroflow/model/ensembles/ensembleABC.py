import pandas as pd
import warnings
import inspect
from abc import ABCMeta, abstractmethod
from neuroflow import units
from neuroflow import utilities


class UnitEnsembleABC(metaclass=ABCMeta):

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __new__(cls, units_class, ensemble_type, *args, **kwargs):
        """ The object crreation:
            - sets the class of unit invariants for the set ensemble
            - create the pandas DataFrame where the unit data will be populated

        :param units_class: must be a the subclass of the supercalass
        :param object_superclass: must be a subclass of UnitABC
        """
        if not inspect.isclass(units_class):
            raise TypeError("The argument units_class must be a class!")

        if not inspect.isclass(ensemble_type):
            raise TypeError("The argument ensemble_type must be a class!")

        if not issubclass(units_class, units.ModelUnitABC):
            raise TypeError("The argument units_class must be a subclass of ModelUnitABC!")

        # create the fresh instance
        new_instance = ensemble_type.__new__(cls)

        # set the neuron type, so that the ensemble_columns property works
        setattr(new_instance, "_units_class", units_class)

        # create the dataframe with the columns
        prepared_df = pd.DataFrame(columns=new_instance.ensemble_columns)

        # set the dataframe to the appropriate location
        setattr(new_instance, "_ensemble_df", prepared_df)

        setattr(new_instance, "_next_item", 0)

        return new_instance

    def __len__(self):
        """ Gets the number of neurons currently stored """
        return len(self.ensemble_df.index)

    def __iter__(self):
        return self   # pragma: no cover

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError("Can only compare ensembles of the same class!")

        if not self.units_class == other.units_class:
            return False
        else:
            # pandas equals comparison
            return self.ensemble_df.equals(other.ensemble_df)

    def __ne__(self, other):
        return not self.__eq__(other)   # pragma: no cover

    def __repr__(self):
        """ A repr implementation  that returns how the object was instantiated,
        for what object types it is for, and how many objects are currently in the set

        """
        return '{ensemble_name}(units_class="<{units_class}>")'\
            .format(ensemble_name=self.__class__.__name__,
                    units_class=self.units_class.__name__)

    def __str__(self):
        """ A simple string representation """
        return "{ensemble_name}({object_type}) #{number_of_objects}".format(ensemble_name=self.__class__.__name__,
                                                                            object_type=self.units_class.__name__,
                                                                            number_of_objects=len(self))

    # endregion

    ##################
    # Public Methods #
    ##################

    # region Public Methods

    @property
    def ensemble_df(self):
        return getattr(self, "_ensemble_df")

    @ensemble_df.setter
    def ensemble_df(self, new_df):
        utilities.verify_type("new_df", new_df, pd.DataFrame)

        if set(new_df.columns.values) != set(self.ensemble_columns):
            raise ValueError("The dataframe you are passing must have the exact same columns as the original!")

        elif len(new_df) != len(self):
            warnings.warn("Overriding the ensable_df with new neurons is "
                          "undesirable, better use proper methods, like add, discard, del!", RuntimeWarning)

        setattr(self, "_ensemble_df", new_df[self.ensemble_columns])

    @property
    def units_class(self):
        return getattr(self, "_units_class")

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region AbstractMethods

    @abstractmethod
    def __next__(self):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __contains__(self, item):
        raise NotImplementedError  # pragma: no cover

    @property
    @abstractmethod
    def ensemble_columns(self) -> list:
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def add(self, unit_object):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def discard(self, unit_object):
        raise NotImplementedError  # pragma: no cover

    # endregion


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
