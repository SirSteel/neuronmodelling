import pandas as pd

from abc import abstractmethod, ABCMeta

from collections.abc import MutableMapping

from neuroflow import units
from neuroflow import utilities

from neuroflow.model.ensembles import ensembleABC


class NeuronsEnsembleInterface(metaclass=ABCMeta):
    @abstractmethod
    def __contains__(self, neuron_name):
        """
        Verifies that the neuron exists

        ParametersStore
        ----------
        neuron_name: must be a string

        Returns
        -------

        True if neuron is present in the index of the records

        """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __next__(self):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __getitem__(self, neuron_name):
        """
        Gets the neuron by name, by creating a new instance of it

        ParametersStore
        ----------
        neuron_name

        Returns
        -------
        an instance of class_of_neurons, which is a subclass of NeuronAbstract

        """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __setitem__(self, neuron_name, neuron_obj):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __delitem__(self, neuron_name):
        """ If a neuron is to be deleted, it removes the row from the record

        ParametersStore
        ----------
        neuron_name must be a string

        """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __repr__(self):
        """ A repr implementation  that returns how the object was instantiated,
        for what neuron types it is for, and how many neurons are currently in the set
        Returns
        -------

        """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __str__(self):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def add(self, neuron_obj):
        """ The default adding mechanism
         - checks if object is of the type, that is the type of the ensemble instance
         - checks if it already exists
         - if its new, adds the record
         - deletes the object

        Object is deleted so that further manipulation is handled by the ensemble

        ParametersStore
        ----------
        neuron_obj Must be an object of the correct type

        """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def discard(self, neuron_name):
        """ Fallback to delitem """
        raise NotImplementedError  # pragma: no cover


class NeuronsEnsemble(ensembleABC.UnitEnsembleABC, NeuronsEnsembleInterface):
    """ This is a class that enables instances of the same type of Neuron class to be stored

    However, this still behaves like a set, ensuring all neurons have unique names
    It implements:
        - adding neurons
        - removing neurons
        - setting neurons
        - iterating over neurons

    The basic idea of the operation is:

        Any type of adding:
            - The NeuronAbstract like object is parsed into a row of the dataframe
            - The object itself is then discarded

        Any type of retreival:
            - the object is instantiated via the object class __init__ method

    """

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __new__(cls, neuron_type, *args, **kwargs):
        """ The object creation:
            - sets the class of neurons invariant of the current instance
            - create the pandas DataFrame where the neurons will be populated

        :param neuron_type: must be a NeuronABC subclass
        """
        if not issubclass(neuron_type, units.NeuronABC):
            raise TypeError("Argument neuron_type must be a subclass of NeuronABC")

        new_instance = super().__new__(cls,
                                       units_class=neuron_type,
                                       ensemble_type=MutableMapping)

        return new_instance

    def __contains__(self, neuron_name):
        """
        Verifies that the neuron exists

        ParametersStore
        ----------
        neuron_name: must be a string

        Returns
        -------

        True if neuron is present in the index of the records

        """
        if isinstance(neuron_name, str):
            return neuron_name in self.neurons
        else:
            raise ValueError("Argument neuron_name to __contains__ must be a string!")

    # region Iteration

    def __next__(self):
        next_item = getattr(self, "_next_item")
        if next_item < len(self):
            current_neuron_name = self.neurons[next_item]
            setattr(self, "_next_item", next_item + 1)
            return self[current_neuron_name]
        else:
            raise StopIteration

    # endregion

    # region Getter, Setter, Deleter

    def __getitem__(self, neuron_name):
        """
        Gets the neuron by name, by creating a new instance of it

        ParametersStore
        ----------
        neuron_name

        Returns
        -------
        an instance of class_of_neurons, which is a subclass of NeuronAbstract

        """

        # check if neuron exists
        if neuron_name in self:

            # turn the values in the row into a dict to use as kwarg
            neuron_row = self.ensemble_df[self.ensemble_df["neuron_name"] == neuron_name]
            neuron_parameters = neuron_row.loc[0, self.ensemble_df.columns != "neuron_name"].to_dict()

            return self.units_class(neuron_name, **neuron_parameters)
        else:
            raise KeyError("No such neuron in the ensemble")

    def __setitem__(self, neuron_name, neuron_obj):

        # make sure neuron obj, is of proper class
        if not isinstance(neuron_obj, self.units_class):

            # if its not the right type, raise error
            raise TypeError("The passed object must be of correct type {}".format(self.units_class))

        elif not neuron_name == neuron_obj.neuron_name:

            # the name that tired to be set does not match the objects name
            raise ValueError("Tried to set neuron {}, "
                             "but the neuron object was named {}".format(neuron_name,
                                                                         neuron_obj.neuron_name))
        elif neuron_name not in self:

            # warnings.warn("The prefered method of adding new neurons, is with .add!", RuntimeWarning)
            # self.add(neuron_obj)
            raise KeyError("Neuron {} does not exists, cannot add neurons this way!".format(neuron_obj.neuron_name))

        else:

            # if neuron already exists, replace it, delete previous, and add new
            del self[neuron_name]
            self.add(neuron_obj)

    def __delitem__(self, neuron_name):
        """ If a neuron is to be deleted, it removes the row from the record

        ParametersStore
        ----------
        neuron_name must be a string

        """
        # neuron name must be a string
        if not isinstance(neuron_name, str):
            raise ValueError("Neuron_name argument for discarding must be a string!")

        # neuron must already exist
        if neuron_name not in self:
            raise KeyError("There is no neuron named {} in the ensamble!".format(neuron_name))

        # remove the row
        new_df = self.ensemble_df[self.ensemble_df["neuron_name"] != neuron_name]

        setattr(self, "_ensemble_df", new_df[self.ensemble_columns])

    # endregion

    # endregion

    ##################
    # Public Methods #
    ##################

    # region Public Methods

    @property
    def ensemble_columns(self) -> list:
        return ["neuron_name"] + list(self.units_class.expected_parameters())

    @property
    def neurons_type(self):
        return self.units_class

    @property
    def neurons(self):
        return list(self.ensemble_df["neuron_name"].values)

    def _store_neuron(self, neuron_obj):
        """ Auxiliary method, to store the neuron, should use the neuron classes own unpack function, which
        should implement all the details for turning it into a row

        ParametersStore
        ----------
        neuron_obj must be of correct type

        """
        utilities.verify_type("neuron_obj", neuron_obj, self.units_class)

        _, neuron_name, neuron_params = neuron_obj.unpack
        row_data = neuron_params
        row_data["neuron_name"] = neuron_name

        framed_neuron = pd.Series(row_data).to_frame().T

        new_df = pd.concat([self.ensemble_df, framed_neuron])

        setattr(self, "_ensemble_df", new_df[self.ensemble_columns])

    def add(self, neuron_obj):
        """ The default adding mechanism
         - checks if object is of the type, that is the type of the ensemble instance
         - checks if it already exists
         - if its new, adds the record
         - deletes the object

        Object is deleted so that further manipulation is handled by the ensemble

        ParametersStore
        ----------
        neuron_obj Must be an object of the correct type

        """

        # check if the the object matches the predefined type
        utilities.verify_type("neuron_obj", neuron_obj, self.units_class)

        # check if neuron does not already exits (important that all neurons are unique
        if neuron_obj.neuron_name in self:
            raise ValueError("The neuron {} already exists in this ensemble! "
                             "Neuron names must be unique!".format(neuron_obj.neuron_name))

        # store all the relevant variables in the pandas
        self._store_neuron(neuron_obj)

    def discard(self, neuron_name):
        """ Fallback to delitem """

        del self[neuron_name]

    # endregion Public Methods


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
