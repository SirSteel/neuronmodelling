"""
This module contains the SynapseEnsembleInterface and synapseEnsemble the concrete implementation
of said interface

Ensembles enable the storage of synapses in the format of a pandas dataframe row

"""

import pandas as pd

from abc import abstractmethod, ABCMeta
from collections.abc import MutableSet

from neuroflow import units
from neuroflow import utilities
from neuroflow.model.ensembles import ensembleABC


class SynapsesEnsembleInterface(metaclass=ABCMeta):
    """ Synapse Ensemble Interface defines the interface for the synapse ensemble
    all the methods expected for the SynapseEnsemble must be listed here

    Any use of synapse Ensemble must programm against this interface and not the concrete
    implementation

    """
    @abstractmethod
    def __contains__(self, synapse_id):
        """ Verifies that a synapse exists in the ensemble """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __next__(self):
        """ ensures ensemble can be iterated over """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def add(self, synapse_obj):
        """ Adds the synapse to the ensemble storing it as a row in the dataframe """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def discard(self, synapse_id):
        """ Removes the synapse from the ensemble based on its ID """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def get_synapse(self, synapse_id):
        """ Returns the synapse object based on its ID"""
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def get_all_neuron_connections(self, neuron_name):
        """ Get the list of connections for the neuron """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def get_outgoing_neuron_connections(self, neuron_name):
        """ Get all the outgoing connections for neuron """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def get_incoming_neuron_connections(self, neuron_name):
        """ Get all the incoming connections for neuron """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def get_recurrent_neuron_connections(self, neuron_name):
        """ Get all the recurrent connections for neuron """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def connection_exists(self, neuron1_name, neuron2_name):
        """ check if a connection between two neurons exists """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def get_connections_between(self, neuron1_name, neuron2_name):
        """ get a list of all connections between two neurons """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def remove_connection_between(self, neuron1_name, neuron2_name):
        """ remove all connection between two neurons"""
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def remove_incoming_conn_for_neuron(self, neuron_name):
        """ remove all incoming connections to a neuron """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def remove_outgoing_conn_for_neuron(self, neuron_name):
        """ remove all outgoing connections to a neuron """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def remove_recurrent_conn_for_neuron(self, neuron_name):
        """ remove all recurrent connections to a neuron """
        raise NotImplementedError  # pragma: no cover


class SynapsesEnsemble(ensembleABC.UnitEnsembleABC, SynapsesEnsembleInterface):
    """ The concrete implementation of the synapse ensemble.

    This is a class that enables instances of the same type of Synapse class to be stored as rows
    in a pandas dataframe.

    However, this still behaves like a set, ensuring all synapses are unique, via UID

    It implements:
        - adding synapses
        - removing synapses
        - setting synapses
        - iterating over synapses
        - getting of connections between neurons
        - removing connections between neurons

    The basic idea of the operation is:

        Any type of adding:
            - The SynapseABC like object is parsed into a row of the dataframe

        Any type of retreival:
            - the object is instantiated via the objects own class __new_ method

    """

    SynapseUnitID = units.SynapseABC.SynapseUnitID
    SynapseUnitForm = units.SynapseABC.SynapseUnitForm

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __new__(cls, synapse_type, *args, **kwargs):
        """ The object crreation:
            - sets the class of synapses invariant of the current instance
            - create the pandas DataFrame where the synapses will be populated

        :param synapse_type: must be a SynapseABC subclass
        """
        if not issubclass(synapse_type, units.SynapseABC):
            raise TypeError("Argument synapse_type must be a subclass of SynapseABC")

        new_instance = super().__new__(cls,
                                       units_class=synapse_type,
                                       ensemble_type=MutableSet)

        return new_instance

    # region Collection Protocol

    def __contains__(self, synapse_id):
        """
        Verifies that a synapse exists

        :param synapse_id: must be a SynapseUnitForm
        namedtuple('SynapseUnitForm', ['type', 'uid',  'pre', 'post', 'parameters'])

        Returns
        -------

        True if synapse exists

        """
        if isinstance(synapse_id, self.SynapseUnitID):

            uid, pre_neuron, post_neuron = synapse_id

            existing = self.ensemble_df[(self.ensemble_df["uid"] == uid) &
                                        (self.ensemble_df["pre_syn"] == pre_neuron) &
                                        (self.ensemble_df["post_syn"] == post_neuron)]

            return True if len(existing) > 0 else False

        else:
            raise TypeError("Argument synapse_id to __contains__ must be a SynapseUnitID named tuple!")

    # endregion

    # region Iteration

    def __next__(self):
        next_item = getattr(self, "_next_item")
        if next_item < len(self):
            current_synapse_id = self.synapses[next_item]
            setattr(self, "_next_item", next_item + 1)
            return self.get_synapse(current_synapse_id)
        else:
            raise StopIteration

    # endregion

    # endregion

    ##################
    # Public Methods #
    ##################

    # region Public Methods

    @property
    def synapses_type(self):
        return self.units_class

    @property
    def ensemble_columns(self) -> list:
        return ["uid", "pre_syn", "post_syn"] + list(self.synapses_type.expected_parameters())

    @property
    def synapses(self):
        return [self._syn_unit_id_from_row(current_row=curr_row)
                for _, curr_row in self.ensemble_df.loc[:, ["uid", "pre_syn", "post_syn"]].iterrows()]

    def _syn_unit_id_from_row(self, current_row):
        return self.SynapseUnitID(current_row["uid"], current_row["pre_syn"], current_row["post_syn"])

    def _store_synapse(self, synapse_obj):
        """ Auxiliary method, to store the neuron, should use the neuron classes own unpack function, which
        should implement all the details for turning it into a row

        ParametersStore
        ----------
        synapse_obj must be of correct type

        """
        utilities.verify_type("synapse_obj", synapse_obj, self.synapses_type)

        # extract the data from the synapse object
        _, uid, pre_neuron, post_neuron, synapse_params = synapse_obj.unpack

        # creates a dictionary, to pass into the Series
        row_data = synapse_params.copy()
        row_data["uid"] = uid
        row_data["pre_syn"] = pre_neuron
        row_data["post_syn"] = post_neuron

        # this part is why I hate pandas
        framed_synapse = pd.Series(row_data).to_frame().T

        # append the thing
        # new_df = self.ensemble_df.append(framed_synapse, verify_integrity=True)
        new_df = pd.concat([self.ensemble_df, framed_synapse])

        # the columns part is so that the order of the columns is kept!
        setattr(self, "_ensemble_df", new_df[self.ensemble_columns])

    def add(self, synapse_obj):
        """ The default adding mechanism
         - checks if object is of the type, that is the type of the ensemble instance
         - checks if it already exists
         - if its new, adds the record
         - deletes the object

        Object is deleted so that further manipulation is handled by the ensemble

        ParametersStore
        ----------
        synapse_obj Must be an object of the correct type

        """

        # check if the the object matches the predefined type
        utilities.verify_type("synapse_obj", synapse_obj, self.synapses_type)

        # check if neuron does not already exits (important that all neurons are unique
        _, uid, pre, post, _ = synapse_obj.unpack
        curr_syn_id = self.SynapseUnitID(uid, pre, post)
        if curr_syn_id in self:
            raise ValueError("The synapse {} already exists in this ensemble! "
                             "Synapses must must be unique, by UID and post, pre neurons!".format(repr(synapse_obj)))

        # store all the relevant variables in the pandas
        self._store_synapse(synapse_obj)

    def discard(self, synapse_id):
        """ Removes the synapse from the ensemble based on its ID """

        # neuron name must be a string
        utilities.verify_type("synapse_id", synapse_id, self.SynapseUnitID)

        # neuron must already exist
        if synapse_id not in self:
            raise KeyError("There is no synapse with such an ID in the ensamble!")

        uid, pre, post = synapse_id

        # remove the row
        new_df = self.ensemble_df[(self.ensemble_df["uid"] != uid) |
                                  (self.ensemble_df["pre_syn"] != pre) |
                                  (self.ensemble_df["post_syn"] != post)]

        setattr(self, "_ensemble_df", new_df[self.ensemble_columns])

    def get_synapse(self, synapse_id):
        if synapse_id not in self:
            raise KeyError("No such synapse in the ensemble")

        uid, pre, post = synapse_id
        synapse_row = self.ensemble_df[(self.ensemble_df["uid"] == uid) &
                                       (self.ensemble_df["pre_syn"] == pre) &
                                       (self.ensemble_df["post_syn"] == post)]

        synapse_parameters = synapse_row.drop(["uid", "pre_syn", "post_syn"], axis=1).loc[0, :].to_dict()

        return self.units_class(uid=uid, pre=pre, post=post, **synapse_parameters)

    def get_all_neuron_connections(self, neuron_name):
        """ Get the list of connections for the neuron

        :param neuron_name: a string of neurons name
        :return: a list of synapse ID tuples
        """
        utilities.verify_type("neuron_name", neuron_name, str)

        neuron_conn_rows = self.ensemble_df[(self.ensemble_df["pre_syn"] == neuron_name) |
                                            (self.ensemble_df["post_syn"] == neuron_name)]

        return [self._syn_unit_id_from_row(current_row=curr_row) for _, curr_row in neuron_conn_rows.iterrows()]

    def get_outgoing_neuron_connections(self, neuron_name):
        """

        :param neuron_name:
        :return list: a list of all outoing connections
        """
        return filter(lambda conn_id: conn_id.pre == neuron_name and conn_id.pre != conn_id.post,
                      self.get_all_neuron_connections(neuron_name))

    def get_incoming_neuron_connections(self, neuron_name):
        return filter(lambda conn_id: conn_id.post == neuron_name and conn_id.pre != conn_id.post,
                      self.get_all_neuron_connections(neuron_name))

    def get_recurrent_neuron_connections(self, neuron_name):
        return filter(lambda conn_id: conn_id.pre == neuron_name and conn_id.pre == conn_id.post,
                      self.get_all_neuron_connections(neuron_name))

    def connection_exists(self, neuron1_name, neuron2_name) -> bool:
        utilities.verify_type("neuron_name", neuron1_name, str)
        utilities.verify_type("neuron_name", neuron2_name, str)

        return len(list(self.get_connections_between(neuron1_name, neuron2_name))) > 0

    def get_connections_between(self, neuron1_name, neuron2_name):
        utilities.verify_type("neuron_name", neuron1_name, str)
        utilities.verify_type("neuron_name", neuron2_name, str)

        return filter(lambda conn_id: (conn_id.pre == neuron1_name and
                                       conn_id.post == neuron2_name) or
                                      (conn_id.post == neuron1_name and
                                       conn_id.pre == neuron2_name),
                      self.get_all_neuron_connections(neuron1_name))

    def remove_connection_between(self, neuron1_name, neuron2_name):
        for conn in self.get_connections_between(neuron1_name, neuron2_name):
            self.discard(conn)

    def remove_incoming_conn_for_neuron(self, neuron_name):
        utilities.verify_type("neuron_name", neuron_name, str)

        for conn in self.get_incoming_neuron_connections(neuron_name):
            self.discard(conn)

    def remove_outgoing_conn_for_neuron(self, neuron_name):
        utilities.verify_type("neuron_name", neuron_name, str)

        for conn in self.get_outgoing_neuron_connections(neuron_name):
            self.discard(conn)

    def remove_recurrent_conn_for_neuron(self, neuron_name):
        utilities.verify_type("neuron_name", neuron_name, str)

        for conn in self.get_recurrent_neuron_connections(neuron_name):
            self.discard(conn)

    # endregion Public Methods


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
