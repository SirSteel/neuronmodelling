import numpy as np
import matplotlib.pylab as plt

# x = np.arange(-100, 100, 0.1)
# plt.plot(x, np.sin(x/3))
# plt.xlabel('t(s)')
# plt.ylabel('sin(x)')
# # plt.axis('tight')
# plt.show()


def number_of_boxes(total_size, spacing_size, box_size):
    return (total_size - spacing_size)//(box_size + spacing_size)


def size_of_insert(number_of_boxes, spacing_size, box_size):
    return number_of_boxes*box_size + (number_of_boxes+1)*spacing_size


def get_padding(total_size, size_of_insert):
    total_padding = total_size - size_of_insert
    end_padding = total_padding // 2
    start_padding = total_padding - end_padding
    return start_padding, end_padding


def get_boxing_instructions(total_size, spacing_size, box_size):
    num_box = number_of_boxes(total_size, spacing_size, box_size)
    assert num_box > 0, "Not ok!"
    ins_size = size_of_insert(num_box, spacing_size, box_size)
    start_p, end_p = get_padding(total_size, ins_size)
    return num_box, ins_size, start_p, end_p


def generate_spaced_box_inputs(total_size, spacing_size, box_size, max_amplitude, base_amplitude):
    num_box, ins_size, start_p, end_p = get_boxing_instructions(total_size, spacing_size, box_size)
    # num_spaces = num_box + 1
    # assert total_size == ins_size, "Input inconsistent! total_size must be at least min_length"
    # box_total = total_size - (num_spaces * spacing_size)
    # box_size = box_total // num_boxes

    stops = [(spacing_size+box_size)*(i+1) for i in range(num_box)]
    starts = [stop_i - box_size for stop_i in stops]

    values = []

    start_padding = [base_amplitude] * start_p
    end_padding = [base_amplitude] * end_p

    box_state = False
    for el in range(ins_size):
        if not box_state and el in starts:
            box_state = True
        elif box_state and el in stops:
            box_state = False

        if box_state:
            values.append(max_amplitude)
        else:
            values.append(base_amplitude)

    if len(start_padding) > 0 and len(end_padding) > 0:
        new_values = start_padding + values + end_padding
    elif len(start_padding) > 0:
        new_values = start_padding + values
    elif len(end_padding) > 0:
        new_values = values + end_padding
    else:
        new_values = values

    return new_values


if __name__ == "__main__":
    x = generate_spaced_box_inputs(100, 20, 30, 100, 20)
    print(x)
