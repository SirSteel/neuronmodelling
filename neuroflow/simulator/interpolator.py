import numpy as np
import scipy as scp
import scipy.interpolate as scp_interpolate
import matplotlib.pyplot as plt

x1 = np.linspace(0, 1000, 1000)


def f(x):
    y = 0
    result = []
    for _ in x:
        result.append(y)
        y += np.random.normal(scale=.02)
    return np.array(result)


values = f(x1)

plt.plot(x1, f(x1))
plt.show()


def create_interpolator(vector, kind="cubic"):
    vector_len = len(vector)
    sample_points = np.linspace(0, vector_len, num=vector_len, endpoint=True)
    return scp_interpolate.interp1d(sample_points, vector, kind=kind)


# x = np.linspace(0, 1000, num=1000, endpoint=True)
# y = values
# f = scp_interpolate.interp1d(x, y)
# f2 = scp_interpolate.interp1d(x, y, kind='cubic')


xnew = scp.arange(0.0, 450.0, 0.01)

f = create_interpolator(values, kind="linear")
f2 = create_interpolator(values)


# xnew = np.linspace(0, 1000, num=41, endpoint=True)

plt.plot(x1, values, 'o', xnew, f(xnew), '-', xnew, f2(xnew), '--')
plt.legend(['data', 'linear', 'cubic'], loc='best')
plt.show()