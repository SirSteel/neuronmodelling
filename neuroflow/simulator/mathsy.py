import numpy as np

#          synapse =>  1  2  3  4  5  6  7  8  9 10 11
post_conn = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],   # n1 A
                      [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],   # n2 B
                      [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],   # n3 C
                      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],   # n4 D
                      [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

#         synapse =>  1  2  3  4  5  6  7  8  9 10 11
pre_conn = np.array([[1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],   # n1 A
                     [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],   # n2 B
                     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],   # n3 C
                     [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],   # n4 D
                     [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

syn_currents = np.array([-70, 32, 5, 22, 18, 17, 10, 15, 30, 25, 20])
neuron_voltages = np.array([-50, 20, 30, -20, -10])


post_curr = post_conn.dot(syn_currents)
syn_pre = neuron_voltages.dot(pre_conn)
syn_post = neuron_voltages.dot(post_conn)

print(post_conn)