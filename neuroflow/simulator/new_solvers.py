import numpy as np
import scipy as sp
import pylab as plt
import random
import seaborn as sns
import pandas as pd
import numba

from functools import partial
from scipy.integrate import Radau


def create_partials(func, params_list):
    return [partial(func, params=params) for params in params_list]


def solve_system_equation(neuron_params_list,
                          neuron_equation,
                          synapse_params_list,
                          synapse_equation,
                          post_connectivity_matrix,
                          pre_connectivity_matrix):

    nrn_eqs = create_partials(neuron_equation, neuron_params_list)
    syn_eqs = create_partials(synapse_equation, synapse_params_list)

    def runner_syn(fun, tpl_zip):
        return fun((tpl_zip[0], tpl_zip[1]), tpl_zip[2])

    def runner_nrn(fun, tpl_zip):
        return fun((tpl_zip[0], tpl_zip[1]), tpl_zip[2])

    def eqs(timestep, input):

        pre_synapse_inputs = input.T.dot(pre_connectivity_matrix)
        post_synapse_inputs = input.T.dot(post_connectivity_matrix)

        # didt = syn_eqs((pre_synapse_inputs, post_synapse_inputs), timestep)

        num_in = len(syn_eqs)
        timesteps = [timestep for _ in range(num_in)]

        didt = map(runner_syn,
                   syn_eqs,
                   zip(pre_synapse_inputs,
                       post_synapse_inputs,
                       timesteps))

        # dot product of the connectivity matrix and synapse_vectors
        synapse_input_vector = post_connectivity_matrix.dot(np.array(list(didt)).T)

        # dvdt = nrn_eqs((input[0], synapse_input_vector), timestep)

        dvdt = map(runner_nrn,
                   nrn_eqs,
                   zip(input,
                       synapse_input_vector,
                       timesteps))

        return np.array(list(dvdt))

    return eqs


def model_equation_vec(neuron_params,
                       neuron_equation,
                       synapse_params,
                       synapse_equation,
                       post_connectivity_matrix,
                       pre_connectivity_matrix):

    nrn_eqs = partial(neuron_equation, params=neuron_params)
    syn_eqs = partial(synapse_equation, params=synapse_params)

    def eqs(timestep, input):

        pre_synapse_inputs = input.dot(pre_connectivity_matrix)
        post_synapse_inputs = input.dot(post_connectivity_matrix)

        num_syn = len(synapse_params)
        syn_timesteps = [timestep for _ in range(num_syn)]

        didt = syn_eqs(timestep=syn_timesteps,
                       input=(pre_synapse_inputs, post_synapse_inputs))

        # dot product of the connectivity matrix and synapse_vectors
        synapse_input_vector = post_connectivity_matrix.dot(didt)

        # dvdt = nrn_eqs((input[0], synapse_input_vector), timestep)

        num_nrn = len(neuron_params)
        nrn_timesteps = [timestep for _ in range(num_nrn)]

        dvdt = nrn_eqs(timestep=nrn_timesteps, input=(input, synapse_input_vector))

        return dvdt

    return eqs


def model_equation_vec1(neuron_params,
                        neuron_equation,
                        synapse_params,
                        synapse_equation,
                        post_connectivity_matrix,
                        pre_connectivity_matrix):

    nrn_eqs = partial(neuron_equation, params=neuron_params)
    syn_eqs = partial(synapse_equation, params=synapse_params)

    def eqs(timestep, voltage_values):

        voltage_values_vec = voltage_values.reshape((len(voltage_values),))

        pre_synapse_inputs = voltage_values_vec.dot(pre_connectivity_matrix)
        post_synapse_inputs = voltage_values_vec.dot(post_connectivity_matrix)

        num_syn = len(synapse_params)
        syn_timesteps = [timestep for _ in range(num_syn)]

        didt = syn_eqs(timestep=syn_timesteps,
                       input=(pre_synapse_inputs, post_synapse_inputs))

        # dot product of the connectivity matrix and synapse_vectors
        synapse_input_vector = post_connectivity_matrix.dot(didt)

        # dvdt = nrn_eqs((voltage_values[0], synapse_input_vector), timestep)

        num_nrn = len(neuron_params)
        nrn_timesteps = [timestep for _ in range(num_nrn)]

        dvdt = nrn_eqs(timestep=nrn_timesteps, input=(voltage_values_vec, synapse_input_vector))

        return dvdt

    return eqs


@numba.jit
def _neuron_eq_vec(cell_capacitance, g_leak, v_leak, curr_voltage, curr_syn_input):
    intrinsic = np.multiply(np.divide(g_leak, cell_capacitance), np.subtract(v_leak, curr_voltage))
    external = np.divide(curr_syn_input, cell_capacitance)
    dvdt = np.add(intrinsic, external)

    return dvdt


def neuron_equation_vec(timestep, input, params):
    cell_capacitance = params["cell_capacitance"].values
    g_leak = params["g_leak"].values
    v_leak = params["v_leak"].values

    curr_voltage = input[0]
    curr_syn_input = input[1]

    # intrinsic = np.multiply(np.divide(g_leak, cell_capacitance), np.subtract(v_leak, curr_voltage))
    # external = np.divide(curr_syn_input, cell_capacitance)
    # dvdt = np.add(intrinsic, external)

    # return dvdt
    return _neuron_eq_vec(cell_capacitance, g_leak, v_leak, curr_voltage, curr_syn_input)


@numba.jit
def g_vec(v_pre, gmax, tau, meu):
    exponent_part = np.exp(np.subtract(np.multiply(tau, v_pre), meu))
    return np.divide(gmax, np.add(1, exponent_part))


@numba.jit
def _synapse_equation_vec(pre_syn, post_syn, gmax, tau, meu, e):
    gpart = g_vec(pre_syn, gmax, tau, meu)
    didt = np.multiply(gpart, np.subtract(e, post_syn))
    return didt


def synapse_equation_vec(timestep, input, params):
    gmax = params["gmax"].values
    tau = params["tau"].values
    meu = params["meu"].values
    e = params["e"].values

    pre_syn = input[0]
    post_syn = input[1]

    # gpart = g_vec(pre_syn, gmax, tau, meu)

    # didt = np.multiply(gpart, np.subtract(e, post_syn))
    # return didt
    return _synapse_equation_vec(pre_syn, post_syn, gmax, tau, meu, e)


def define_model(neuron_equations: list, synapse_equations: list, connectivities: tuple):
    pass


def prepare_model_function():

    @numba.jit
    def _neuron_equation1(curr_voltage, curr_syn_input, cell_capacitance, g_leak, v_leak):
        dvdt = (g_leak / cell_capacitance) * (v_leak - curr_voltage) + curr_syn_input / cell_capacitance
        return dvdt

    def neuron_equation1(input, timestep, params):
        cell_capacitance = params["cell_capacitance"]
        g_leak = params["g_leak"]
        v_leak = params["v_leak"]

        curr_voltage = input[0]
        curr_syn_input = input[1]

        # dvdt = (g_leak / cell_capacitance) * (v_leak - curr_voltage) + curr_syn_input / cell_capacitance
        # return dvdt
        return _neuron_eq_vec(curr_voltage, curr_syn_input, cell_capacitance, g_leak, v_leak)

    @numba.jit
    def g(v_pre, gmax, tau, meu):
        return gmax / (1 + np.exp(tau * v_pre - meu))

    @numba.jit
    def _synapse_equation1(pre_syn, post_syn, gmax, tau, meu, e):
        g_fun = partial(g, gmax=gmax, tau=tau, meu=meu)

        didt = g_fun(pre_syn) * (e - post_syn)
        return didt

    def synapse_equation1(input, timestep, params):
        gmax = params["gmax"]
        tau = params["tau"]
        meu = params["meu"]
        e = params["e"]

        pre_syn = input[0]
        post_syn = input[1]

        # g_fun = partial(g, gmax=gmax, tau=tau, meu=meu)

        # didt = g_fun(pre_syn) * (e - post_syn)
        # return didt
        return _synapse_equation1(pre_syn, post_syn, gmax, tau, meu, e)

    #          synapse =>  1  2  3  4  5  6  7  8  9 10 11
    post_conn = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # n1 A
                          [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],  # n2 B
                          [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],  # n3 C
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],  # n4 D
                          [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

    #         synapse =>  1  2  3  4  5  6  7  8  9 10 11
    pre_conn = np.array([[1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],  # n1 A
                         [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],  # n2 B
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # n3 C
                         [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],  # n4 D
                         [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

    neuron_params = [{"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                     {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                     {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                     {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                     {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50}, ]

    # gmax = params["gmax"]
    # tau = params["tau"]
    # meu = params["meu"]
    # e = params["e"]

    synapse_param = [{"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                     {"gmax": 1, "tau": 1, "meu": -50, "e": -50}]

    model_function = solve_system_equation(neuron_params_list=neuron_params,
                                           neuron_equation=neuron_equation1,
                                           synapse_params_list=synapse_param,
                                           synapse_equation=synapse_equation1,
                                           post_connectivity_matrix=post_conn,
                                           pre_connectivity_matrix=pre_conn)

    return model_function


def prepare_solver(model_function, init_voltages):
    return Radau(model_function, t0=0, y0=init_voltages, t_bound=450, max_step=0.01, vectorized=True)


def get_results(solver):
    results = []
    while True:
        solver.step()
        results.append(solver.y)
        if solver.status == "finished":
            break
        elif solver.status == "failed":
            break

    return results


def make_simple_plot(results):
    mat = np.array(results)
    # plt.imshow(mat, cmap='hot', interpolation='nearest')
    t = np.linspace(0, mat.shape[0], mat.shape[0])
    plt.plot(t, mat)
    plt.xlabel("time 'steps'")
    plt.ylabel("Voltage (mV)")
    plt.show()


def main(select):
    if select == 1:
        pass
        # neuron_equations = create_neuron_equations()
        # synapse_equations =

    elif select == 2:
        mf = prepare_model_function()

        neuron_voltages = np.array([-50, 20, 30, -20, -10])

        r = prepare_solver(mf, neuron_voltages)
        results = get_results(r)
        make_simple_plot(results)
        # pass

    elif select == 3:
        params_nrn = pd.DataFrame(data=[{"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},])

        params_syn = pd.DataFrame(data=[{"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50}])



        neuron_voltages = np.array([-50, 20, 30, -20, -10])

        #         synapse =>  1  2  3  4  5  6  7  8  9 10 11
        pre_conn = np.array([[1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],   # n1 A
                             [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],   # n2 B
                             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],   # n3 C
                             [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],   # n4 D
                             [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

        #          synapse =>  1  2  3  4  5  6  7  8  9 10 11
        post_conn = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],   # n1 A
                              [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],   # n2 B
                              [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],   # n3 C
                              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],   # n4 D
                              [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

        syn_out = neuron_voltages.dot(pre_conn)

        syn_inp = post_conn.dot(syn_out)

        num_in = len(syn_inp)
        timesteps = [0 for _ in range(num_in)]

        nrn_result = neuron_equation_vec(timestep=timesteps,
                                         input=(neuron_voltages, syn_inp),
                                         params=params_nrn)

        pre_synapse_inputs = nrn_result.dot(pre_conn)
        post_synapse_inputs = nrn_result.dot(post_conn)

        syn_result = synapse_equation_vec(timestep=timesteps,
                                          input=(pre_synapse_inputs, post_synapse_inputs),
                                          params=params_syn)

    elif select == 4:
        params_nrn = pd.DataFrame(data=[{"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                 {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},])

        params_syn = pd.DataFrame(data=[{"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                         {"gmax": 1, "tau": 1, "meu": -50, "e": -50}])

        neuron_voltages = np.array([-50, 20, 30, -20, -10])

        #         synapse =>  1  2  3  4  5  6  7  8  9 10 11
        pre_conn = np.array([[1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],   # n1 A
                             [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],   # n2 B
                             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],   # n3 C
                             [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],   # n4 D
                             [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

        #          synapse =>  1  2  3  4  5  6  7  8  9 10 11
        post_conn = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],   # n1 A
                              [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],   # n2 B
                              [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],   # n3 C
                              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],   # n4 D
                              [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

        m = model_equation_vec(neuron_params=params_nrn,
                               neuron_equation=neuron_equation_vec,
                               synapse_params=params_syn,
                               synapse_equation=synapse_equation_vec,
                               pre_connectivity_matrix=pre_conn,
                               post_connectivity_matrix=post_conn)

        r = Radau(m, t0=0, y0=neuron_voltages, t_bound=450, max_step=0.01, vectorized=False)

        results = get_results(r)

        make_simple_plot(results)


if __name__ == "__main__":
    main(select=4)

