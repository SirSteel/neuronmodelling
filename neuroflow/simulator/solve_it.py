import numpy as np
import scipy as sp
import pylab as plt
import random

import scipy.interpolate as scp_interpolate
from scipy.integrate import odeint
from functools import partial


def solve_system_equation(input_values,
                          timestep,
                          neuron_params,
                          neuron_equation,
                          synapse_params,
                          synapse_equation,
                          post_connectivity_matrix,
                          pre_connectivity_matrix):

    nrn_eqs = partial(neuron_equation, params=neuron_params)
    syn_eqs = partial(synapse_equation, params=synapse_params)

    def eqs(input, timestep):

        pre_synapse_inputs = pre_connectivity_matrix.dot(input[0])
        post_synapse_inputs = post_connectivity_matrix.dot(input[0])

        didt = syn_eqs((pre_synapse_inputs, post_synapse_inputs), timestep)

        # dot product of the connectivity matrix and synapse_vectors
        synapse_input_vector = post_connectivity_matrix.dot(didt)

        dvdt = nrn_eqs((input[0], synapse_input_vector), timestep)

        return dvdt, didt

    return eqs


neuron_params1 = {"cell_capacitance": 2,
                  "g_leak": 0.1,
                  "v_leak": -50}


def neuron_equation1(input, timestep, params):
    cell_capacitance = params["cell_capacitance"]
    g_leak = params["g_leak"]
    v_leak = params["v_leak"]

    curr_voltage = input[0]
    curr_syn_input = input[1]

    dvdt = (g_leak / cell_capacitance) * (v_leak - curr_voltage) + curr_syn_input / cell_capacitance
    return dvdt


synapse_params1 = {"gmax": 1,
                   "tau": 1,
                   "meu": 30,
                   "e": -50}


def g(v_pre, gmax, tau, meu):
    return gmax / (1 + np.exp(tau*v_pre - meu))


def synapse_equation1(input, timestep, params):

    gmax = params["gmax"]
    tau = params["tau"]
    meu = params["meu"]
    e = params["e"]

    pre_syn = input[0]
    post_syn = input[1]

    g_fun = partial(g, gmax=gmax, tau=tau, meu=meu)

    didt = g_fun(pre_syn) * (e - post_syn)
    return didt


t = sp.arange(0.0, 450.0, 0.01)
""" The time to integrate over """

post_connectivity_matrix1 = np.array([[0, 1, 0],
                                      [0, 1, 0],
                                      [0, 0, 1]])

pre_connectivity_matrix1 = np.array([[1, 0, 0],
                                     [0, 0, 1],
                                     [0, 1, 0]])

nrn_start = np.array([-50, -50, -50])
syn_start = np.array([-50, -50, -50])


v = odeint(solve_system_equation,
           [nrn_start, syn_start],
           t,
           args=(neuron_params1,
                 neuron_equation1,
                 synapse_params1,
                 synapse_equation1,
                 post_connectivity_matrix1,
                 pre_connectivity_matrix1))


v1 = v[:, 0]
# v2 = v[:, 1]

# v3 = np.vectorize(my_neuron.input_voltage)(t)

plt.figure()
plt.title('Example Neuron')
plt.plot(t, v1)
# plt.plot(t, v2)
# plt.plot(t, v3)
plt.ylabel('V (mV)')
plt.show()