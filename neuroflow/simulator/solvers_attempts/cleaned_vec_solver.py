import numpy as np
# import scipy as sp
import pylab as plt
# import random
# import seaborn as sns
import pandas as pd
import numba

# from functools import partial
from scipy.integrate import Radau


def model_equation_vec(neuron_params,
                       synapse_params,
                       post_connectivity_matrix,
                       pre_connectivity_matrix):

    nrn_eqs = get_neuron_eq_vec(params=neuron_params)
    syn_eqs = get_synapse_eq_vec(params=synapse_params)

    def eqs(timestep, neuron_voltages):

        pre_synapse_inputs = neuron_voltages.dot(pre_connectivity_matrix)
        post_synapse_inputs = neuron_voltages.dot(post_connectivity_matrix)

        didt = syn_eqs(timestep=timestep, pre_syn=pre_synapse_inputs, post_syn=post_synapse_inputs)

        synapse_input_vector = post_connectivity_matrix.dot(didt)

        dvdt = nrn_eqs(timestep=timestep, curr_voltage=neuron_voltages, curr_syn_input=synapse_input_vector)

        return dvdt

    return eqs


def get_neuron_eq_vec(params):
    cell_capacitance = params["cell_capacitance"].values
    g_leak = params["g_leak"].values
    v_leak = params["v_leak"].values

    @numba.jit
    def _neuron_eq(timestep, curr_voltage, curr_syn_input):
        intrinsic = np.multiply(np.divide(g_leak, cell_capacitance), np.subtract(v_leak, curr_voltage))
        external = np.divide(curr_syn_input, cell_capacitance)
        dvdt = np.add(intrinsic, external)

        return dvdt

    return _neuron_eq


def get_synapse_eq_vec(params):
    gmax = params["gmax"].values
    tau = params["tau"].values
    meu = params["meu"].values
    e = params["e"].values

    @numba.jit
    def _g_vec(v_pre):
        exponent_part = np.exp(np.subtract(np.multiply(tau, v_pre), meu))
        return np.divide(gmax, np.add(1, exponent_part))

    @numba.jit
    def _synapse_eq(timestep, pre_syn, post_syn):
        return np.multiply(_g_vec(pre_syn), np.subtract(e, post_syn))

    return _synapse_eq


def get_results(solver):
    results = []
    while True:
        solver.step()
        results.append(solver.y)
        if solver.status == "finished":
            break
        elif solver.status == "failed":
            break

    return results


def make_simple_plot(results):
    mat = np.array(results)
    # plt.imshow(mat, cmap='hot', interpolation='nearest')
    t = np.linspace(0, mat.shape[0], mat.shape[0])
    plt.plot(t, mat)
    plt.xlabel("time 'steps'")
    plt.ylabel("Voltage (mV)")
    plt.show()


def prepare_ns():
    params_nrn = pd.DataFrame(data=[{"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50}, ])

    params_syn = pd.DataFrame(data=[{"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50}])

    #         synapse =>  1  2  3  4  5  6  7  8  9 10 11
    pre_conn = np.array([[1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],   # n1 A
                         [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],   # n2 B
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],   # n3 C
                         [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],   # n4 D
                         [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

    #          synapse =>  1  2  3  4  5  6  7  8  9 10 11
    post_conn = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],   # n1 A
                          [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],   # n2 B
                          [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],   # n3 C
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],   # n4 D
                          [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0]])  # n5 E

    return params_nrn, params_syn, pre_conn, post_conn


def prepare_ns1():
    params_nrn = pd.DataFrame(data=[{"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    {"cell_capacitance": 1, "g_leak": 0.1, "v_leak": -50},
                                    ])

    params_syn = pd.DataFrame(data=[{"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50},
                                    {"gmax": 1, "tau": 1, "meu": -50, "e": -50}
                                    ])

    pre_conn = \
        np.array([[1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
                  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                  [1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
                  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                  [1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
                  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
                  ])

    post_conn = \
        np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                  [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                  [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                  [1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
                  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
                  ])

    return params_nrn, params_syn, pre_conn, post_conn


def main(select):

    if select == 1:
        params_nrn, params_syn, pre_conn, post_conn = prepare_ns()

        neuron_voltages = np.array([-50, 20, 30, -20, -10])

        m = model_equation_vec(neuron_params=params_nrn,
                               synapse_params=params_syn,
                               pre_connectivity_matrix=pre_conn,
                               post_connectivity_matrix=post_conn)

        r = Radau(m, t0=0, y0=neuron_voltages, t_bound=450, max_step=0.01, vectorized=False)

        results = get_results(r)

        make_simple_plot(results)

    elif select == 2:
        params_nrn, params_syn, pre_conn, post_conn = prepare_ns1()

        neuron_voltages = np.array([-50, 20, 30, -20, -10, -55, 25, 35, -25, -15, -45, 15, 25, -25, -5])

        m = model_equation_vec(neuron_params=params_nrn,
                               synapse_params=params_syn,
                               pre_connectivity_matrix=pre_conn,
                               post_connectivity_matrix=post_conn)

        r = Radau(m, t0=0, y0=neuron_voltages, t_bound=450, max_step=0.01, vectorized=False)

        results = get_results(r)

        make_simple_plot(results)


if __name__ == "__main__":
    main(select=1)
