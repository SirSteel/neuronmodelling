import numpy as np
import scipy.interpolate as scp_interpolate
import numba


def get_interpolators(input_vectors):
    def _create_interpolator(vector, kind="cubic"):
        vector_len = len(vector)
        sample_points = np.linspace(0, vector_len, num=vector_len, endpoint=True)
        return scp_interpolate.interp1d(sample_points, vector, kind=kind)

    def _interpolator_helper(vector):
        if vector is False:
            return lambda _: 0
        elif isinstance(vector, np.ndarray):
            return _create_interpolator(vector)
        else:
            raise TypeError

    return list(map(_interpolator_helper, input_vectors))


def get_interpolator(input_vectors):

    _interpolators = get_interpolators(input_vectors)
    _num_inter = len(_interpolators)

    def _inter_of_t(t):
        return np.array(list(map(lambda func, t_step: func(t_step), _interpolators, [t] * _num_inter)))

    return _inter_of_t


def f(x):
    y = 0
    result = []
    for _ in x:
        result.append(y)
        y += np.random.normal(scale=1)
    return np.array(result)


x1 = np.linspace(0, 1000, 1000)
values = f(x1)


vectors_list = [values,
                False,
                False,
                False,
                False]

if __name__ == "__main__":

    # interpolators = get_interpolators(vectors_list)
    inter = get_interpolator(vectors_list)

    test_values = []

    for t_s in range(0, 10):
        # curr_val = np.array(list(map(lambda func, t_step: func(t_step), interpolators, [t] * num_inter)))
        test_values.append(inter(t_s))

    # print(test_values)
