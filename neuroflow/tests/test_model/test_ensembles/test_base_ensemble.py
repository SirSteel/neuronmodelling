import pytest
from collections import MutableSet, MutableMapping
from abc import abstractmethod
from neuroflow.model.ensembles import ensembleABC
from neuroflow import units
from neuroflow.units import parameters as param_pkg


@pytest.fixture
def restrictions_full():
    rest = param_pkg.PreparedConstraintsCollection()
    rest.register("p", ">0")
    rest.register("q", ">3")
    rest.register("a", ">3")
    return rest


@pytest.fixture
def parameters_full():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           }
    return par


@pytest.fixture()
def mock_unit_class(restrictions_full):
    class MockModelUnit(units.ModelUnitABC):
        def __hash__(self):
            pass

        def __repr__(self):
            pass

        def __str__(self):
            pass

        def __eq__(self, other):
            pass

        @property
        def unpack(self) -> tuple:
            return 1, 1

        @classmethod
        def main_equation(cls, *args, **kwargs):
            return 1

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

    return MockModelUnit


@pytest.fixture()
def mock_ensemble_class():
    class MockEns(ensembleABC.UnitEnsembleABC):
        def __new__(cls, *args, **kwargs):
            return super().__new__(cls, *args, **kwargs)

        def __next__(self):
            pass

        def __contains__(self, item):
            pass

        @property
        def ensemble_columns(self) -> list:
            return []

        def add(self, unit_object):
            pass

        def discard(self, unit_object):
            pass

    return MockEns

@pytest.fixture()
def mock_ensemble_other_class():
    class MockOtherEns(ensembleABC.UnitEnsembleABC):
        def __new__(cls, *args, **kwargs):
            return super().__new__(cls, *args, **kwargs)

        def __next__(self):
            pass

        def __contains__(self, item):
            pass

        @property
        def ensemble_columns(self) -> list:
            return []

        def add(self, unit_object):
            pass

        def discard(self, unit_object):
            pass

    return MockOtherEns


def test_ensemble_base_constructor(mock_ensemble_class, mock_unit_class):

    # test that it only takes class defs, these calls do not make sense yet
    with pytest.raises(TypeError) as err:
        _ = mock_ensemble_class(units_class=int, ensemble_type="str")
    assert str(err.value) == "The argument ensemble_type must be a class!"

    # members class must be a class
    with pytest.raises(TypeError) as err:
        _ = mock_ensemble_class(units_class="str", ensemble_type=int)
    assert str(err.value) == "The argument units_class must be a class!"

    # this should not work because the object class must be a subclass of superclass ModelUnitABC
    with pytest.raises(TypeError) as err:
        _ = mock_ensemble_class(units_class=str, ensemble_type=int)
    assert str(err.value) == "The argument units_class must be a subclass of ModelUnitABC!"

    x = mock_ensemble_class(units_class=mock_unit_class,
                            ensemble_type=MutableSet)

    assert x.units_class == mock_unit_class


def test_ensemble_base_str_repr(mock_ensemble_class, mock_unit_class):
    x = mock_ensemble_class(units_class=mock_unit_class,
                            ensemble_type=MutableSet)

    assert repr(x) == 'MockEns(units_class="<MockModelUnit>")'
    assert str(x) == 'MockEns(MockModelUnit) #0'


def test_base_ensemble_comparisons(mock_ensemble_class, mock_unit_class):
    x = mock_ensemble_class(units_class=mock_unit_class,
                            ensemble_type=MutableSet)

    with pytest.raises(TypeError) as err:
        assert x == 1
        assert x == str
    assert str(err.value) == "Can only compare ensembles of the same class!"