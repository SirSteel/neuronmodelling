import pytest
from neuroflow.model.ensembles import NeuronsEnsemble
from neuroflow.units.neurons.simple_neuron import SimpleNeuron
from neuroflow.units.neurons.hodgin_huxley import HodginHuxley


@pytest.fixture()
def three_neurons():
    n1 = SimpleNeuron("AVA", g_max=1, tau=2, lambda_i=3)
    n2 = SimpleNeuron("AVB", g_max=2, tau=4, lambda_i=9)
    n3 = SimpleNeuron("AVC", g_max=3, tau=6, lambda_i=27)

    return n1, n2, n3


def test_ensemble_creation_and_basics():
    x = NeuronsEnsemble(SimpleNeuron)
    assert len(x) == 0
    assert iter(x) is x
    assert x.neurons_type == SimpleNeuron

    with pytest.raises(TypeError, match="Argument neuron_type must be a subclass of NeuronABC"):
        _ = NeuronsEnsemble(int)


def test_adding_of_neurons(three_neurons):
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons

    ns.add(n1)
    ns.add(n2)
    ns.add(n3)

    assert "AVA" in ns
    assert "AVB" in ns
    assert "AVC" in ns

    with pytest.raises(TypeError):
        ns.add(3)

    with pytest.raises(TypeError):
        ns.add("AVA")

    with pytest.raises(ValueError):
        assert "AVA" in ns
        assert len(ns) > 0
        ns.add(n1)

    with pytest.raises(ValueError):
        assert "AVA" in ns
        SimpleNeuron("AVA", g_max=1, tau=2, lambda_i=3)
        ns.add(n1)


def test_ensemble_contains_checks(three_neurons):

    # prepare
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)

    # test
    assert "AVA" in ns
    assert "AVB" in ns
    assert "AVC" in ns
    assert "PVC" not in ns

    with pytest.raises(ValueError):
        assert 3 in ns


def test_ensemble_inequality(three_neurons):
    ns1 = NeuronsEnsemble(SimpleNeuron)
    ns2 = NeuronsEnsemble(SimpleNeuron)
    ns3 = NeuronsEnsemble(HodginHuxley)

    assert ns1 == ns2
    assert ns1 != ns3
    assert ns2 != ns3

    # prepare
    ns3 = NeuronsEnsemble(SimpleNeuron)
    ns4 = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons

    ns3.add(n1)
    ns3.add(n2)
    ns3.add(n3)

    ns4.add(n1)
    ns4.add(n2)
    ns4.add(n3)

    assert ns3 == ns4
    assert ns1 != ns3


def test_ensemble_repr(three_neurons):
    # prepare
    ns0 = NeuronsEnsemble(SimpleNeuron)
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)


    assert repr(ns) == 'NeuronsEnsemble(units_class="<SimpleNeuron>")'
    assert repr(ns0) == 'NeuronsEnsemble(units_class="<SimpleNeuron>")'

    assert str(ns) == 'NeuronsEnsemble(SimpleNeuron) #3'
    assert str(ns0) == 'NeuronsEnsemble(SimpleNeuron) #0'


def test_neuron_deletions_discards(three_neurons):

    # prepare
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)

    # test deletion
    assert len(ns) == 3
    del ns["AVA"]
    assert "AVA" not in ns
    assert len(ns) == 2

    with pytest.raises(ValueError):
        del ns[3]

    with pytest.raises(ValueError):
        del ns[3.5]

    with pytest.raises(KeyError):
        assert "PVC" not in ns
        del ns["PVC"]

    assert len(ns) == 2
    assert "AVB" in ns
    ns.discard("AVB")
    assert "AVB" not in ns
    assert len(ns) == 1


def test_ensemble_neuron_getting(three_neurons):
    # prepare
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)

    neu = ns["AVA"]

    assert isinstance(neu, SimpleNeuron)
    assert neu == n1

    with pytest.raises(KeyError):
        assert "PVA" not in ns
        _ = ns["PVA"]


def test_ensemble_iterator(three_neurons):
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)

    for neuron_obj in ns:
        assert isinstance(neuron_obj, SimpleNeuron)
        # assert neuron_name == neuron_obj.neuron_name


def test_neuron_ensemble_df_setter(three_neurons):
    ns = NeuronsEnsemble(SimpleNeuron)
    nse = NeuronsEnsemble(SimpleNeuron)
    nse1 = NeuronsEnsemble(SimpleNeuron)
    nsu = NeuronsEnsemble(HodginHuxley)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)

    assert len(ns) == 3
    df1 = ns.ensemble_df

    assert len(nse) == 0
    nse.ensemble_df = df1
    assert len(nse) == 3

    err_msg = "The dataframe you are passing must have the exact same columns as the original!"
    with pytest.raises(ValueError, match=err_msg):
        nsu.ensemble_df = df1

    warn_text = "Overriding the ensable_df with new neurons is " \
                "undesirable, better use proper methods, like add, discard, del!"
    with pytest.warns(RuntimeWarning, match=warn_text):
        nse1.ensemble_df = df1


def test_neuron_ensemble_element_setter(three_neurons):

    # prepare
    ns = NeuronsEnsemble(SimpleNeuron)
    n1, n2, n3 = three_neurons
    ns.add(n1)
    ns.add(n2)
    ns.add(n3)
    neuron1 = SimpleNeuron("PVA", g_max=3, tau=6, lambda_i=27)
    neuronAVArep = SimpleNeuron("AVA", g_max=4, tau=4, lambda_i=4)

    assert len(ns) == 3

    # wrong type
    with pytest.raises(TypeError):
        ns["AVB"] = 3

    # naming conflict
    with pytest.raises(ValueError):
        ns["AVA"] = neuron1

    # should not add neurons like this
    with pytest.raises(KeyError):
        assert "PVA" not in ns
        ns["PVA"] = neuron1
        assert "PVA" in ns

    assert "AVA" in ns
    ns["AVA"] = neuronAVArep
    assert ns["AVA"] == neuronAVArep









