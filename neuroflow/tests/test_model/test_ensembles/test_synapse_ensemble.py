import pytest
from copy import deepcopy
from neuroflow.model.ensembles import SynapsesEnsemble
from neuroflow.units.synapses.simple_synapse import SimpleSynapse


@pytest.fixture()
def nine_synapses():
    syn1 = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn2 = SimpleSynapse(uid=2, pre="AVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn3 = SimpleSynapse(uid=3, pre="AVD", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn4 = SimpleSynapse(uid=4, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn5 = SimpleSynapse(uid=5, pre="AVC", post="PVC", g_max=1, tau=2, lambda_i=3)
    syn6 = SimpleSynapse(uid=6, pre="PVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn7 = SimpleSynapse(uid=7, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn8 = SimpleSynapse(uid=8, pre="AVB", post="AVA", g_max=1, tau=2, lambda_i=3)
    syn9 = SimpleSynapse(uid=9, pre="AVA", post="AVA", g_max=1, tau=2, lambda_i=3)

    return [syn1, syn2, syn3, syn4, syn5, syn6, syn7, syn8, syn9]


def test_synapse_ensemble_creation_and_basics():
    x = SynapsesEnsemble(SimpleSynapse)
    assert len(x) == 0
    assert iter(x) is x
    assert x.synapses_type == SimpleSynapse

    with pytest.raises(TypeError, match="Argument synapse_type must be a subclass of SynapseABC"):
        _ = SynapsesEnsemble(int)


def test_adding_of_synapses(nine_synapses):
    syn_ens = SynapsesEnsemble(SimpleSynapse)
    syn1 = nine_synapses[0]
    syn2 = nine_synapses[1]
    syn3 = nine_synapses[2]
    syn4 = nine_synapses[3]
    syn5 = nine_synapses[4]
    syn6 = nine_synapses[5]
    syn7 = nine_synapses[6]
    syn8 = nine_synapses[7]
    syn9 = nine_synapses[8]

    syn_ens.add(syn1)
    syn_ens.add(syn2)
    syn_ens.add(syn3)
    syn_ens.add(syn4)
    syn_ens.add(syn5)
    syn_ens.add(syn6)
    syn_ens.add(syn7)
    syn_ens.add(syn8)
    syn_ens.add(syn9)

    assert len(syn_ens) == 9

    unit_id_form = SimpleSynapse.SynapseUnitID
    for current_synapse in [syn1, syn2, syn3, syn4, syn5, syn6, syn7, syn8, syn9]:
        assert current_synapse.get_id in syn_ens

    with pytest.raises(TypeError):
        syn_ens.add(3)

    with pytest.raises(TypeError):
        syn_ens.add("AVA")

    with pytest.raises(ValueError):
        assert syn1.get_id in syn_ens
        assert len(syn_ens) > 0
        syn_ens.add(syn1)


@pytest.fixture()
def mock_synapse_ensemble(nine_synapses):
    syn_ens = SynapsesEnsemble(SimpleSynapse)
    syn1 = nine_synapses[0]
    syn2 = nine_synapses[1]
    syn3 = nine_synapses[2]
    syn4 = nine_synapses[3]
    syn5 = nine_synapses[4]
    syn6 = nine_synapses[5]
    syn7 = nine_synapses[6]
    syn8 = nine_synapses[7]
    syn9 = nine_synapses[8]

    syn_ens.add(syn1)
    syn_ens.add(syn2)
    syn_ens.add(syn3)
    syn_ens.add(syn4)
    syn_ens.add(syn5)
    syn_ens.add(syn6)
    syn_ens.add(syn7)
    syn_ens.add(syn8)
    syn_ens.add(syn9)

    return syn_ens


def test_ensemble_contains_checks(mock_synapse_ensemble, nine_synapses):

    # prepare
    syn1 = nine_synapses[0]
    syn2 = nine_synapses[1]
    syn3 = nine_synapses[2]
    syn4 = nine_synapses[3]
    syn5 = nine_synapses[4]
    syn6 = nine_synapses[5]
    syn7 = nine_synapses[6]
    syn8 = nine_synapses[7]
    syn9 = nine_synapses[8]

    # test
    assert syn1.get_id in mock_synapse_ensemble
    assert syn2.get_id in mock_synapse_ensemble
    assert syn3.get_id in mock_synapse_ensemble
    assert syn4.get_id in mock_synapse_ensemble
    assert syn5.get_id in mock_synapse_ensemble
    assert syn6.get_id in mock_synapse_ensemble
    assert syn7.get_id in mock_synapse_ensemble
    assert syn8.get_id in mock_synapse_ensemble
    assert syn9.get_id in mock_synapse_ensemble

    with pytest.raises(TypeError):
        assert 3 in mock_synapse_ensemble


def test_ensemble_equalities(nine_synapses):
    syn_ens1 = SynapsesEnsemble(SimpleSynapse)
    syn_ens2 = SynapsesEnsemble(SimpleSynapse)

    assert syn_ens1 == syn_ens2

    # prepare
    syn_ens3 = SynapsesEnsemble(SimpleSynapse)
    syn_end4 = SynapsesEnsemble(SimpleSynapse)

    syn1 = nine_synapses[0]
    syn2 = nine_synapses[1]
    syn3 = nine_synapses[2]

    syn_ens3.add(syn1)
    syn_ens3.add(syn2)
    syn_ens3.add(syn3)

    syn_end4.add(syn1)
    syn_end4.add(syn2)
    syn_end4.add(syn3)

    assert syn_ens3 == syn_end4
    assert syn_ens1 != syn_ens3


def test_synapse_ensemble_repr_str(nine_synapses):
    # prepare
    ens0 = SynapsesEnsemble(SimpleSynapse)
    ens = SynapsesEnsemble(SimpleSynapse)

    syn1 = nine_synapses[0]
    syn2 = nine_synapses[1]
    syn3 = nine_synapses[2]

    ens.add(syn1)
    ens.add(syn2)
    ens.add(syn3)

    text = 'SynapsesEnsemble(units_class="<SimpleSynapse>")'
    assert text == repr(ens)

    text = 'SynapsesEnsemble(units_class="<SimpleSynapse>")'
    assert text == repr(ens0)

    assert str(ens) == 'SynapsesEnsemble(SimpleSynapse) #3'
    assert str(ens0) == 'SynapsesEnsemble(SimpleSynapse) #0'


def test_synapse_deletions_discards(nine_synapses):

    # prepare
    syn_ens = SynapsesEnsemble(SimpleSynapse)
    syn1, syn2, syn3 = nine_synapses[0:3]
    syn_ens.add(syn1)
    syn_ens.add(syn2)
    syn_ens.add(syn3)

    # test deletion
    assert len(syn_ens) == 3
    syn_ens.discard(syn1.get_id)
    assert syn1.get_id not in syn_ens
    assert len(syn_ens) == 2

    with pytest.raises(TypeError):
        syn_ens.discard(syn_ens[3])

    with pytest.raises(TypeError):
        syn_ens.discard(syn_ens[3.5])

    with pytest.raises(KeyError):
        synX = nine_synapses[5]
        assert synX.get_id not in syn_ens
        syn_ens.discard(synX.get_id)

    assert len(syn_ens) == 2
    assert syn3.get_id in syn_ens
    syn_ens.discard(syn3.get_id)
    assert syn3.get_id not in syn_ens
    assert len(syn_ens) == 1


def test_ensemble_synapse_getting(nine_synapses):
    # prepare
    syn_ens = SynapsesEnsemble(SimpleSynapse)
    syn1, syn2, syn3 = nine_synapses[0:3]
    syn_ens.add(syn1)
    syn_ens.add(syn2)
    syn_ens.add(syn3)

    # test reset
    syn1_id_row = deepcopy(syn1.get_id)
    syn_out = syn_ens.get_synapse(syn1_id_row)
    assert isinstance(syn_out, SimpleSynapse)

    with pytest.raises(KeyError):
        synX = nine_synapses[5]
        assert synX.get_id not in syn_ens
        _ = syn_ens.get_synapse(synX.get_id)


def test_ensemble_iterator(nine_synapses):
    syn_ens = SynapsesEnsemble(SimpleSynapse)
    syn1, syn2, syn3 = nine_synapses[0:3]
    syn_ens.add(syn1)
    syn_ens.add(syn2)
    syn_ens.add(syn3)

    for synapse_obj in syn_ens:
        assert isinstance(synapse_obj, SimpleSynapse)
        # assert synapse_name == synapse_obj.synapse_name


def test_get_connections_for_neuron(mock_synapse_ensemble):
    """
    syn1 = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn2 = SimpleSynapse(uid=2, pre="AVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn3 = SimpleSynapse(uid=3, pre="AVD", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn4 = SimpleSynapse(uid=4, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn5 = SimpleSynapse(uid=5, pre="AVC", post="PVC", g_max=1, tau=2, lambda_i=3)
    syn6 = SimpleSynapse(uid=6, pre="PVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn7 = SimpleSynapse(uid=7, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn8 = SimpleSynapse(uid=8, pre="AVB", post="AVA", g_max=1, tau=2, lambda_i=3)
    syn9 = SimpleSynapse(uid=9, pre="AVA", post="AVA", g_max=1, tau=2, lambda_i=3)
    """
    conns_all = mock_synapse_ensemble.get_all_neuron_connections("AVA")
    conns_out = mock_synapse_ensemble.get_outgoing_neuron_connections("AVA")
    conns_in = mock_synapse_ensemble.get_incoming_neuron_connections("AVA")
    conns_rec = mock_synapse_ensemble.get_recurrent_neuron_connections("AVA")

    assert len(list(conns_all)) == 5
    assert len(list(conns_out)) == 3
    assert len(list(conns_in)) == 1
    assert len(list(conns_rec)) == 1

    conns_all = mock_synapse_ensemble.get_all_neuron_connections("AVB")
    conns_out = mock_synapse_ensemble.get_outgoing_neuron_connections("AVB")
    conns_in = mock_synapse_ensemble.get_incoming_neuron_connections("AVB")
    conns_rec = mock_synapse_ensemble.get_recurrent_neuron_connections("AVB")

    assert len(list(conns_all)) == 7
    assert len(list(conns_out)) == 1
    assert len(list(conns_in)) == 6
    assert len(list(conns_rec)) == 0

    conns_out = mock_synapse_ensemble.get_outgoing_neuron_connections("AVD")
    conns_in = mock_synapse_ensemble.get_incoming_neuron_connections("PVC")

    assert len(list(conns_out)) == 1
    assert len(list(conns_in)) == 1


def test_connection_exists(mock_synapse_ensemble):
    """
    syn1 = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn2 = SimpleSynapse(uid=2, pre="AVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn3 = SimpleSynapse(uid=3, pre="AVD", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn4 = SimpleSynapse(uid=4, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn5 = SimpleSynapse(uid=5, pre="AVC", post="PVC", g_max=1, tau=2, lambda_i=3)
    syn6 = SimpleSynapse(uid=6, pre="PVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn7 = SimpleSynapse(uid=7, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn8 = SimpleSynapse(uid=8, pre="AVB", post="AVA", g_max=1, tau=2, lambda_i=3)
    syn9 = SimpleSynapse(uid=9, pre="AVA", post="AVA", g_max=1, tau=2, lambda_i=3)

    """
    assert mock_synapse_ensemble.connection_exists("AVA", "AVA")
    assert mock_synapse_ensemble.connection_exists("AVA", "AVB")
    assert not mock_synapse_ensemble.connection_exists("AVA", "PVC")
    assert not mock_synapse_ensemble.connection_exists("AVA", "PVD")
    assert mock_synapse_ensemble.connection_exists("PVC", "AVB")


def test_connection_removals(mock_synapse_ensemble):
    """
    syn1 = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn2 = SimpleSynapse(uid=2, pre="AVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn3 = SimpleSynapse(uid=3, pre="AVD", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn4 = SimpleSynapse(uid=4, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn5 = SimpleSynapse(uid=5, pre="AVC", post="PVC", g_max=1, tau=2, lambda_i=3)
    syn6 = SimpleSynapse(uid=6, pre="PVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn7 = SimpleSynapse(uid=7, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    syn8 = SimpleSynapse(uid=8, pre="AVB", post="AVA", g_max=1, tau=2, lambda_i=3)
    syn9 = SimpleSynapse(uid=9, pre="AVA", post="AVA", g_max=1, tau=2, lambda_i=3)

    """
    initial_size = 9
    assert len(mock_synapse_ensemble) == initial_size

    mock_synapse_ensemble.remove_connection_between("PVC", "AVB")
    assert len(mock_synapse_ensemble) == initial_size - 1

    mock_synapse_ensemble.remove_recurrent_conn_for_neuron("AVA")
    assert len(mock_synapse_ensemble) == initial_size - 2

    mock_synapse_ensemble.remove_outgoing_conn_for_neuron("AVA")
    assert len(mock_synapse_ensemble) == initial_size - 5

    mock_synapse_ensemble.remove_incoming_conn_for_neuron("PVC")
    assert len(mock_synapse_ensemble) == initial_size - 6











