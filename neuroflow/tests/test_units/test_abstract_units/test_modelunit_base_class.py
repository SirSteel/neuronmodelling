import pytest
from neuroflow.units import ModelUnitABC
from neuroflow.units.parameters import PreparedConstraintsCollection, ParametersStore


def test_base_unit_bad_constraints():
    class MockUnit(ModelUnitABC):
        def __repr__(self):
            pass

        def __str__(self):
            pass

        def __eq__(self, other):
            pass

        @property
        def unpack(self) -> tuple:
            return 1, 2

        @classmethod
        def pack(cls, *loading_tuple):
            pass

        @classmethod
        def main_equation(cls, *args, **kwargs):
            pass

        @classmethod
        def parameter_constraints(cls):
            pass

    with pytest.raises(TypeError):
        _ = MockUnit("bad_constraints")


@pytest.fixture
def restrictions_full():
    rest = PreparedConstraintsCollection()
    with rest as r:
        r.register("p", ">0")
        r.register("q", ">3")
        r.register("a", ">3")
    return rest


@pytest.fixture
def restrictions_full2():
    rest = PreparedConstraintsCollection()
    with rest as r:
        r.register("p", ">-1")
        r.register("q", ">-1")
        r.register("a", ">-1")
    return rest


@pytest.fixture
def parameters_full():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           }
    return par


@pytest.fixture
def parameters_full_bad():
    par = {"p": -3,
           "q": -4.5,
           "a": -15,
           }
    return par


def test_base_unit_bad_params(restrictions_full, parameters_full_bad):
    class MockUnit(ModelUnitABC):
        def __repr__(self):
            pass

        def __str__(self):
            pass

        def __eq__(self, other):
            pass

        @property
        def unpack(self) -> tuple:
            return 1, 2

        @classmethod
        def pack(cls, *loading_tuple):
            pass

        @classmethod
        def main_equation(cls, *args, **kwargs):
            pass

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

    with pytest.raises(TypeError):
        _ = MockUnit("bad_params")

    with pytest.raises(ValueError):
        _ = MockUnit(parameters_full_bad)




def test_base_unit(parameters_full, restrictions_full, restrictions_full2):

    class MockUnit(ModelUnitABC):
        def __repr__(self):
            pass

        def __str__(self):
            pass

        def __eq__(self, other):
            pass

        @property
        def unpack(self) -> tuple:
            return 1, 2

        @classmethod
        def pack(cls, *loading_tuple):
            pass

        @classmethod
        def main_equation(cls, *args, **kwargs):
            pass

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

    x = MockUnit(parameters=parameters_full)

    with pytest.raises(TypeError):
        hash(x)

    n = x.parameters

    with pytest.raises(TypeError):
        x.parameters = "String"

    params_new_bad = ParametersStore(restrictions_full2, parameters=parameters_full)
    params_new_good = ParametersStore(restrictions_full, parameters=parameters_full)
    with pytest.raises(ValueError):
        x.parameters = params_new_bad

    x.parameters = params_new_good
    assert len(x.parameters) == 3

    # check expected parameters
    assert x.expected_parameters() == {'p', 'a', 'q'}










