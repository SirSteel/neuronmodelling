import pytest
from neuroflow.units import NeuronABC
from neuroflow.units.parameters import ParametersStore, PreparedConstraintsCollection


@pytest.fixture
def restrictions_full():
    rest = PreparedConstraintsCollection()
    rest.register("p", ">0")
    rest.register("q", ">3")
    rest.register("a", ">3")
    return rest


@pytest.fixture
def parameters_full():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           }
    return par


@pytest.fixture
def parameters_full1():
    par = {"p": 36,
           "q": 6.5,
           "a": 150,
           }
    return par


@pytest.fixture
def parameters_full2():
    par = {"p": 52,
           "q": 4452,
           "a": 5,
           }
    return par


@pytest.fixture
def sample_params(restrictions_full, parameters_full):
    return ParametersStore(restrictions_full, parameters_full)


def test_check_creation_of_neuron_subclass(restrictions_full, parameters_full):
    class MockNeuron(NeuronABC):

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    test = MockNeuron("test_name", **parameters_full)
    assert test.neuron_name == "test_name"
    # TODO: Compare this
    # assert test.__repr__() == "MockNeuron(neuron_name='test_name', a=15, q=4.5, p=3)"


@pytest.fixture
def mock_neuron_subclass(restrictions_full):
    class MockNeuron(NeuronABC):

        def __hash__(self):
            pass

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    return MockNeuron


@pytest.fixture
def mock_neuron_other_subclass(restrictions_full):
    class MockNeuronBest(NeuronABC):

        def __hash__(self):
            pass

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    return MockNeuronBest


@pytest.fixture
def mock_neuron_instance(mock_neuron_subclass, parameters_full):
    return mock_neuron_subclass("test_name", **parameters_full)


def test_getting_expected_params(mock_neuron_instance, sample_params):
    assert mock_neuron_instance.expected_parameters() == set(sample_params.parameter_names)


def test_unpacking_loading(mock_neuron_instance, restrictions_full):
    class MockNeuron(NeuronABC):

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    neuron_type, name, params = mock_neuron_instance.unpack
    assert isinstance(name, str)
    assert isinstance(neuron_type, str)
    assert name == "test_name"
    assert neuron_type == "MockNeuron"
    assert isinstance(params, dict)


def test_cannot_instantiate():
    with pytest.raises(NotImplementedError):
        x = NeuronABC("stubby")


def test_neuron_repr_str(mock_neuron_instance):
    assert 'MockNeuron(neuron_name="test_name", **parameters' in repr(mock_neuron_instance)
    assert "test_name<MockNeuron(" in str(mock_neuron_instance)


def test_equality_comparisons(mock_neuron_subclass,
                              mock_neuron_other_subclass,
                              parameters_full,
                              parameters_full1,
                              parameters_full2):
    neu1 = mock_neuron_subclass("AVA", **parameters_full)
    neu2 = mock_neuron_subclass("AVA", **parameters_full1)
    neu3 = mock_neuron_subclass("AVB", **parameters_full)
    neu4 = mock_neuron_subclass("AVA", **parameters_full)
    neu5 = mock_neuron_subclass("AVB", **parameters_full2)
    neu6 = mock_neuron_other_subclass("AVA", **parameters_full)

    assert neu1 == neu1
    assert neu4 == neu4
    assert neu1 == neu4
    assert neu1 != neu2
    assert neu1 != neu3
    assert neu3 != neu5
    assert not neu1 == neu6

    with pytest.raises(TypeError):
        assert neu1 == 3





