import pytest
from neuroflow.units import SynapseABC
from neuroflow.units.parameters import ParametersStore, PreparedConstraintsCollection


@pytest.fixture
def restrictions_full():
    with PreparedConstraintsCollection() as rest:
        rest.register("p", ">0")
        rest.register("q", ">3")
        rest.register("a", ">3")
        return rest


@pytest.fixture
def parameters_full():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           }
    return par


@pytest.fixture
def parameters_full1():
    par = {"p": 30,
           "q": 40.5,
           "a": 105,
           }
    return par


@pytest.fixture
def parameters_full2():
    par = {"p": 34,
           "q": 45.5,
           "a": 115,
           }
    return par


@pytest.fixture
def sample_params(restrictions_full, parameters_full):
    return ParametersStore(restrictions_full, parameters_full)


def test_check_creation_of_neuron_subclass(restrictions_full, parameters_full):
    class MockSynapse(SynapseABC):

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    test = MockSynapse(uid= 1, pre="PreNeuron", post="PostNeuron", **parameters_full)
    assert test.pre == "PreNeuron"
    assert test.post == "PostNeuron"
    # TODO: Compare this
    # assert test.__repr__() == "MockNeuron(neuron_name='test_name', a=15, q=4.5, p=3)"


@pytest.fixture
def mock_synapse_subclass(restrictions_full):
    class MockSynapse(SynapseABC):

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    return MockSynapse


@pytest.fixture
def mock_synapse_other_subclass(restrictions_full):
    class MockSynapseBest(SynapseABC):

        @classmethod
        def parameter_constraints(cls):
            return restrictions_full

        @staticmethod
        def main_equation(**kwargs):
            # fake for now
            return lambda x: str(x)

    return MockSynapseBest


@pytest.fixture
def mock_synapse_instance(mock_synapse_subclass, parameters_full):

    return mock_synapse_subclass(uid=1, pre="PreNeuron", post="PostNeuron", **parameters_full)


def test_getting_expected_params(mock_synapse_instance, sample_params):
    assert mock_synapse_instance.expected_parameters() == set(sample_params.parameter_names)


def test_unpacking(mock_synapse_instance):
    type, uid, pre, post, params = mock_synapse_instance.unpack
    assert isinstance(pre, str)
    assert isinstance(post, str)
    assert isinstance(type, str)
    assert isinstance(uid, int)
    assert pre == "PreNeuron"
    assert post == "PostNeuron"
    assert type == "MockSynapse"
    assert isinstance(params, dict)


def test_cannot_instantiate():
    with pytest.raises(TypeError):
        _ = SynapseABC("stubby")

    with pytest.raises(NotImplementedError):
        _ = SynapseABC(uid=1, post="stubby", pre="bla", parameters=dict())


def test_repr_str(mock_synapse_instance):
    assert 'MockSynapse(pre="PreNeuron", post="PostNeuron", **parameters' in repr(mock_synapse_instance)

    assert '(PreNeuron -> PostNeuron) <MockSynapse' in str(mock_synapse_instance)


def test_get_id(mock_synapse_instance):
    uid, pre, post = mock_synapse_instance.get_id
    assert isinstance(uid, int)
    assert pre == "PreNeuron"
    assert post == "PostNeuron"


def test_equality_comparisons(mock_synapse_subclass,
                              mock_synapse_other_subclass,
                              parameters_full,
                              parameters_full1,
                              parameters_full2):

    # prepare
    syn1 = mock_synapse_subclass(uid=1, pre="AVA", post="AVB", **parameters_full)
    syn2 = mock_synapse_subclass(uid=1, pre="AVA", post="AVB", **parameters_full)

    syn3 = mock_synapse_subclass(uid=1, pre="AVC", post="AVB", **parameters_full)
    syn4 = mock_synapse_subclass(uid=1, pre="AVA", post="AVD", **parameters_full)
    syn5 = mock_synapse_subclass(uid=2, pre="AVA", post="AVB", **parameters_full)

    syn6 = mock_synapse_subclass(uid=1, pre="AVA", post="AVB", **parameters_full1)

    syn7 = mock_synapse_subclass(uid=4, pre="AVD", post="AVC", **parameters_full2)

    syn8 = mock_synapse_other_subclass(uid=1, pre="AVA", post="AVB", **parameters_full)

    assert syn1 == syn1
    assert syn1 == syn2

    assert syn1 != syn3
    assert syn1 != syn4
    assert syn1 != syn5
    assert syn1 != syn6
    assert syn1 != syn7
    assert syn1 != syn8

    with pytest.raises(TypeError):
        assert syn1 == 2



