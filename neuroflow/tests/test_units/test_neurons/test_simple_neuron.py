import pytest
import numpy as np
from neuroflow.units.neurons.simple_neuron import SimpleNeuron


def test_neuron_creation():
    _ = SimpleNeuron("AVA", g_max=1, tau=2, lambda_i=3)
    _ = SimpleNeuron("best_neuron", g_max=1.8, tau=2.6, lambda_i=3.7)
    _ = SimpleNeuron("Mocky", g_max=1.2, tau=2.5, lambda_i=3)


def test_neuron_creation_invalid_name():
    with pytest.raises(TypeError):
        _ = SimpleNeuron(1, g_max=1, tau=2, lambda_i=3)


def test_neuron_creation_extra_arguments():
    with pytest.raises(ValueError):
        _ = SimpleNeuron("AVA", g_max=1, tau=2, lambda_i=3, extra=5)


def test_neuron_creation_missing_arguments():
    with pytest.raises(ValueError):
        _ = SimpleNeuron("AVA", g_max=1, tau=2)


def test_neuron_creation_invalid_type_arguments():
    with pytest.raises(ValueError):
        _ = SimpleNeuron("AVA", g_max=1, tau=2, lambda_i="a")


def test_neuron_simple_computation():
    n = SimpleNeuron("AVA", g_max=1, tau=2, lambda_i=3)

    arr1 = np.array([1, 2, 3])
    arr2 = np.array([1, 2, 3])
    arr3 = np.array([1, 2, 3])

    arr_res = SimpleNeuron.main_equation(g_max=arr1, tau=arr2, lambda_i=arr3)

