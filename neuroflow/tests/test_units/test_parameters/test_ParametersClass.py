import pytest
from neuroflow.units.parameters import ParametersStore, PreparedConstraintsCollection


@pytest.fixture
def restrictions_full():
    """
           {"p": lambda x: x > 0,
            "q": lambda x: x > 3,
            "a": lambda x: x > 3,
            "b": lambda x: x > 3,
            "c": lambda x: x > 3,
            "d": lambda x: x > 3,
            }

    """
    rest = PreparedConstraintsCollection()
    rest.register("p", ">0")
    rest.register("q", ">3")
    rest.register("a", ">3")
    rest.register("b", ">3")
    rest.register("c", ">3")
    rest.register("d", ">3")
    return rest


@pytest.fixture
def parameters_full():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           "b": 20,
           "c": 30,
           "d": 5,
           }
    return par


@pytest.fixture
def parameters_keys_missing():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           "b": 20,
           "c": 30,
           }
    return par


@pytest.fixture
def parameters_keys_extra():
    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           "b": 20,
           "c": 30,
           "d": 1.5,
           "e": 1.5}
    return par


@pytest.fixture
def parameters_keys_missingandextra():
    par = {"p": 3,
           "a": 15,
           "b": 20,
           "c": 30,
           "d": 1.5,
           "e": 1.5}
    return par


@pytest.fixture
def parameters_values_invalid():
    par = {"p": 0,
           "q": -1,
           "a": -1,
           "b": -1,
           "c": -1,
           "d": -1,
           }
    return par


def test_creation(restrictions_full, parameters_full):
    param = ParametersStore(restrictions_full, parameters_full)
    assert len(param) == 6
    assert "a" in param
    assert "b" in param
    assert "c" in param
    assert "d" in param
    assert "p" in param
    assert "q" in param

    with pytest.raises(TypeError):
        _ = ParametersStore(dict(), parameters_full)


def test_creation_parameter_mismatch(restrictions_full,
                                     parameters_keys_missing,
                                     parameters_keys_extra,
                                     parameters_keys_missingandextra):
    with pytest.raises(ValueError):
        ParametersStore(restrictions_full, parameters_keys_missing)

    with pytest.raises(ValueError):
        ParametersStore(restrictions_full, parameters_keys_extra)

    with pytest.raises(ValueError):
        ParametersStore(restrictions_full, parameters_keys_missingandextra)


def test_parameter_verification(restrictions_full,
                                parameters_full,
                                parameters_values_invalid):

    # rest = {"p": lambda x: x > 0,
    #         "q": lambda x: x > 3,
    #         "a": lambda x: x > 3,
    #         "b": lambda x: x > 3,
    #         "c": lambda x: x > 3,
    #         "d": lambda x: x > 3,
    #         }

    with pytest.raises(ValueError):
        ParametersStore(restrictions_full, parameters_values_invalid)

    # verifier = partial(n_base.ParametersStore._verify_parameter,
    #                    parameter_constraints=**restrictions_full)
    param = ParametersStore(restrictions_full, parameters_full)
    rest = param.constraints

    assert not rest.verify_parameter("p", -1)
    assert not rest.verify_parameter("a", 0)

    with pytest.raises(TypeError):
        assert not rest.verify_parameter("p", "hahaaha")

    with pytest.raises(TypeError):
        assert not rest.verify_parameter(1, 1)

    with pytest.raises(KeyError):
        assert not rest.verify_parameter("xyz", 1)


@pytest.fixture
def sample_params(restrictions_full, parameters_full):
    return ParametersStore(restrictions_full, parameters_full)


def test_deletion(sample_params):
    with pytest.raises(AttributeError):
        del sample_params["p"]


def test_iteration(sample_params):
    index = 0
    for key, value in sample_params.items():
        assert isinstance(key, str)
        assert isinstance(value, (int, float))
        index += 1
    assert index == len(sample_params)


def test_len(sample_params, parameters_full):
    assert len(sample_params) == len(parameters_full.keys())


def test_getting_true(sample_params, parameters_full):
    for param_name, param_value in parameters_full.items():
        assert sample_params[param_name] == param_value


def test_getting_false(sample_params):
    with pytest.raises(KeyError):
        x = sample_params["not_existing"]


def test_setting_true(sample_params):
    sample_params["q"] = 5
    assert sample_params["q"] == 5

    sample_params["a"] = 10
    assert sample_params["a"] == 10

    sample_params["b"] = 12
    assert sample_params["b"] == 12


def test_setting_non_existing(sample_params):
    with pytest.raises(KeyError):
        sample_params["xyz"] = 12


def test_setting_invalid(sample_params):
    with pytest.raises(ValueError):
        sample_params["p"] = -300

    with pytest.raises(ValueError):
        sample_params["d"] = 1


def test_restrictions_attr(sample_params):
    with pytest.raises(AttributeError):
        del sample_params.constraints

    with pytest.raises(AttributeError):
        sample_params.constraints = 1

    assert isinstance(sample_params.constraints, PreparedConstraintsCollection)


def test_compare_restrictions_class_f(parameters_full):
    rest1 = PreparedConstraintsCollection()
    rest1.register("p", ">0")
    rest1.register("q", ">3")
    rest1.register("a", ">3")
    rest1.register("b", ">3")
    rest1.register("c", ">3")
    rest1.register("d", ">3")

    rest2 = PreparedConstraintsCollection()
    rest2.register("p", ">0")
    rest2.register("q", ">3")
    rest2.register("a", ">3")
    rest2.register("b", ">3")
    rest2.register("c", ">3")
    rest2.register("d", ">3")

    rest3 = PreparedConstraintsCollection()
    rest3.register("p", ">0")
    rest3.register("q", ">0")
    rest3.register("a", ">0")
    rest3.register("b", ">0")
    rest3.register("c", ">0")
    rest3.register("d", ">0")

    p1 = ParametersStore(rest1, parameters_full)
    p2 = ParametersStore(rest2, parameters_full)
    p3 = ParametersStore(rest3, parameters_full)

    assert rest1 == rest2
    assert rest1 != rest3

    assert p1.compare_constraints(p2)
    assert p2.compare_constraints(p1)
    assert not p1.compare_constraints(p3)
    assert not p2.compare_constraints(p3)


def test_comparisons():
    rest1 = PreparedConstraintsCollection()
    rest1.register("p", ">0")
    rest1.register("q", ">3")
    rest1.register("a", ">3")
    rest1.register("b", ">3")
    rest1.register("c", ">3")
    rest1.register("d", ">3")

    rest2 = PreparedConstraintsCollection()
    rest2.register("p", ">0")
    rest2.register("q", ">3")
    rest2.register("a", ">3")
    rest2.register("b", ">3")
    rest2.register("c", ">3")
    rest2.register("d", ">3")

    rest3 = PreparedConstraintsCollection()
    rest3.register("p", ">0")
    rest3.register("q", ">0")
    rest3.register("a", ">0")
    rest3.register("b", ">0")
    rest3.register("c", ">0")
    rest3.register("d", ">0")

    par = {"p": 3,
           "q": 4.5,
           "a": 15,
           "b": 20,
           "c": 30,
           "d": 5,
           }

    par1 = {"p": 30,
           "q": 4.5,
           "a": 15,
           "b": 200,
           "c": 30,
           "d": 5,
           }

    # both same
    p1 = ParametersStore(rest1, par)
    p2 = ParametersStore(rest1, par)

    # what happens in multiple evaluations
    assert p1 == p1
    assert p1 == p1
    assert p1 == p1

    # exactly the same
    assert p1 == p2

    # constraints different, but same type
    p3 = ParametersStore(rest2, par)

    assert p1 == p3
    assert p2 == p3

    # different constraints
    p4 = ParametersStore(rest3, par)

    assert not p1 == p4
    assert not p2 == p4
    assert not p3 == p4

    # same constraints diff param values
    p5 = ParametersStore(rest1, par1)

    assert p1 != p5
    assert p2 != p5
    assert p3 != p5

    # diff constraints diff param values
    p6 = ParametersStore(rest3, par1)

    assert not p1 == p6
    assert not p2 == p6
    assert not p3 == p6
    assert p1 != p6
    assert p2 != p6
    assert p3 != p6

    with pytest.raises(TypeError):
        assert p1 == 1

    with pytest.raises(TypeError):
        assert p2 == "string"


def test_unpacking(sample_params, parameters_full):
    p_dict = sample_params.unpack
    assert isinstance(p_dict, dict)
    assert set(p_dict.keys()) == set(parameters_full)
    assert all(parameters_full[curr_key] == curr_val for curr_key, curr_val in p_dict.items())


def test_param_repr_str(sample_params):

    assert repr(sample_params) == 'ParametersStore(constraints=<PreparedConstraintsCollection>, **parameters)'
    assert len(str(sample_params)) == 33



