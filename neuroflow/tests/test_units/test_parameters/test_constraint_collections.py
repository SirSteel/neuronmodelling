import pytest
from neuroflow.units.parameters import PreparedConstraintsCollection, LambdaConstraintsCollection


def test_parameter_string_parser():
    string1 = " not 0 ; ex 5 ; + ; - ;"
    string2 = " <50 ; >-50 ; -50<x<50 ; - ;"
    string3 = " < 50 > ; >>5 ; blabla ;"
    string4 = " not 0 "

    fac = PreparedConstraintsCollection()

    list1 = fac.parse_constraints_string(string1)
    assert len(list1) == 4

    list2 = fac.parse_constraints_string(string2)
    assert len(list2) == 4

    list3 = fac.parse_constraints_string(string4)
    assert len(list3) == 1

    with pytest.raises(ValueError):
        list4 = fac.parse_constraints_string(string3)
        assert len(list4) == 1


def test_create_constraint_collection():
    rest1 = PreparedConstraintsCollection()
    rest1.register("p", ">0")
    rest1.register("q", ">3")
    rest1.register("a", ">3")
    rest1.register("b", ">3")
    rest1.register("c", ">3")
    rest1.register("d", ">3")

    rest2 = PreparedConstraintsCollection()
    rest2.register("p", ">0")
    rest2.register("q", ">3")
    rest2.register("a", ">3")
    rest2.register("b", ">3")
    rest2.register("c", ">3")
    rest2.register("d", ">3")

    rest3 = PreparedConstraintsCollection()
    rest3.register("p", ">0")
    rest3.register("q", ">0")
    rest3.register("a", ">0")
    rest3.register("b", ">0")
    rest3.register("c", ">0")
    rest3.register("d", ">0")

    assert rest1 == rest2
    assert rest1 != rest3


def test_res_locking_no_overrride():

    rest3 = PreparedConstraintsCollection()
    rest3.register("p", ">0")
    rest3.register("q", ">0")
    rest3.register("a", ">0")
    rest3.register("b", ">0")
    rest3.register("c", ">0")
    rest3.register("d", ">0")

    with pytest.raises(ValueError):
        rest3.register("d", ">0")

    rest3.locked = True

    with pytest.raises(ValueError):
        rest3.register("ef", ">6")

    with pytest.raises(ValueError):
        rest3.locked = False


def test_lambda_constraints_coll():
    rest3 = LambdaConstraintsCollection()
    rest3.register("p", lambda x: x > 0)
    rest3.register("q", lambda x: x > 0)

    with pytest.raises(TypeError):
        rest3.register("q", ">0")

    with pytest.raises(ValueError):
        rest3.register("p", lambda x: x > 50)

    rest3.locked = True

    with pytest.raises(RuntimeError):
        rest3.register("ef", lambda x: x > 0)

    with pytest.raises(ValueError):
        rest3.locked = False


@pytest.fixture()
def mock_prep_rest_coll():
    rest1 = PreparedConstraintsCollection()
    rest1.register("p", ">0")
    rest1.register("q", ">3")
    rest1.register("a", ">3")
    rest1.register("b", ">3")
    rest1.register("c", ">3")
    rest1.register("d", ">3")

    return rest1


@pytest.fixture()
def mock_lamb_rest_coll():
    rest3 = LambdaConstraintsCollection()
    rest3.register("p", lambda x: x > 0)
    rest3.register("q", lambda x: x > 0)

    return rest3


def test_lambda_verify(mock_lamb_rest_coll):
    rest3 = mock_lamb_rest_coll
    assert rest3.verify_parameter("p", 20)
    assert not rest3.verify_parameter("p", -1)

    with pytest.raises(KeyError):
        assert rest3.verify_parameter("no_exist", 20)

    with pytest.raises(TypeError):
        assert rest3.verify_parameter(4234, 20)

    with pytest.raises(TypeError):
        assert rest3.verify_parameter("no_exist", "adsasda")


def test_prep_verify(mock_prep_rest_coll):
    rest = mock_prep_rest_coll
    assert rest.verify_parameter("p", 20)
    assert not rest.verify_parameter("p", -1)

    with pytest.raises(KeyError):
        assert rest.verify_parameter("no_exist", 20)

    with pytest.raises(TypeError):
        assert rest.verify_parameter(4234, 20)

    with pytest.raises(TypeError):
        assert rest.verify_parameter("no_exist", "adsasda")


def test_repr_str_base(mock_prep_rest_coll, mock_lamb_rest_coll):
    assert str(mock_prep_rest_coll) == "Constraints Collection -> (p, q, a, b, c, d)"
    assert repr(mock_prep_rest_coll) == 'PreparedConstraintsCollection() -> (p, q, a, b, c, d)'

    assert str(mock_lamb_rest_coll) == "Constraints Collection -> (p, q)"
    assert repr(mock_lamb_rest_coll) == 'LambdaConstraintsCollection() -> (p, q)'


def test_context_manager_base():
    rest = LambdaConstraintsCollection()
    with rest as r:
        r.register("p", lambda x: x > 0)
        r.register("q", lambda x: x > 0)

    assert rest.verify_parameter("p", 20)
    assert not rest.verify_parameter("p", -1)

    with pytest.raises(RuntimeError):
        rest.register("p", lambda x: x > 0)

    with pytest.raises(RuntimeError):
        with rest as r:
            r.register("p", lambda x: x > 0)

    # could be any error
    with pytest.raises(ValueError):
        rest1 = LambdaConstraintsCollection()
        with rest1 as r1:
            r1.register("p", lambda x: x > 0)
            r1.register("q", lambda x: x > 0)
            raise ValueError



