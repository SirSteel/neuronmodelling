import pytest
from neuroflow.units.parameters.constraints_gallery import *


def test_exact_value_res():
    a5 = ExactValueConstraint(5)
    b20 = ExactValueConstraint(20)
    c0 = ExactValueConstraint(0)
    dmin1 = ExactValueConstraint(-1)
    efloat = ExactValueConstraint(0.5)

    assert a5.parameter == 5
    assert b20.parameter == 20
    assert c0.parameter == 0
    assert dmin1.parameter == -1
    assert efloat.parameter == 0.5

    # all correct
    assert a5.parse_parameter(5)
    assert b20.parse_parameter(20)
    assert c0.parse_parameter(0)
    assert dmin1.parse_parameter(-1)
    assert efloat.parse_parameter(0.5)

    # all wrong
    assert not a5.parse_parameter(0)
    assert not b20.parse_parameter(1)
    assert not c0.parse_parameter(1)
    assert not dmin1.parse_parameter(1)
    assert not efloat.parse_parameter(1)

    assert not a5.parse_parameter(13212)
    assert not b20.parse_parameter(41.324)
    assert not c0.parse_parameter(-3.41)
    assert not dmin1.parse_parameter(-234)
    assert not efloat.parse_parameter(23.14)


def test_exact_not_value_res():
    a5 = NotExactValueConstraint(5)
    b20 = NotExactValueConstraint(20)
    c0 = NotExactValueConstraint(0)
    dmin1 = NotExactValueConstraint(-1)
    efloat = NotExactValueConstraint(0.5)

    assert a5.parameter == 5
    assert b20.parameter == 20
    assert c0.parameter == 0
    assert dmin1.parameter == -1
    assert efloat.parameter == 0.5

    # all wrong
    assert not a5.parse_parameter(5)
    assert not b20.parse_parameter(20)
    assert not c0.parse_parameter(0)
    assert not dmin1.parse_parameter(-1)
    assert not efloat.parse_parameter(0.5)

    # all correct
    assert a5.parse_parameter(0)
    assert b20.parse_parameter(1)
    assert c0.parse_parameter(1)
    assert dmin1.parse_parameter(1)
    assert efloat.parse_parameter(1)

    assert a5.parse_parameter(13212)
    assert b20.parse_parameter(41.324)
    assert c0.parse_parameter(-3.41)
    assert dmin1.parse_parameter(-234)
    assert efloat.parse_parameter(23.14)


def test_greater():
    gminus100 = GreaterThanConstraint(-100)
    g1point5 = GreaterThanConstraint(1.5)

    assert gminus100.parse_parameter(0)
    assert gminus100.parse_parameter(10)
    assert not gminus100.parse_parameter(-110)

    assert g1point5.parse_parameter(200)
    assert g1point5.parse_parameter(1.6)
    assert not g1point5.parse_parameter(1)


def test_less():
    lminus100 = LessThanConstraint(-100)
    l1point5 = LessThanConstraint(1.5)

    assert lminus100.parse_parameter(-200)
    assert lminus100.parse_parameter(-101)
    assert not lminus100.parse_parameter(10)

    assert l1point5.parse_parameter(1)
    assert l1point5.parse_parameter(1.4)
    assert not l1point5.parse_parameter(2)


def test_positive_negative_nonzero():
    pos = PositiveConstraint()
    neg = NegativeConstraint()
    noz = NonZeroConstraint()

    assert pos.parameter == 0
    assert neg.parameter == 0
    assert noz.parameter == 0

    assert pos.parse_parameter(10)
    assert pos.parse_parameter(4354)
    assert not pos.parse_parameter(-23.34)

    assert neg.parse_parameter(-10)
    assert neg.parse_parameter(-1340)
    assert not neg.parse_parameter(10)

    assert noz.parse_parameter(10432)
    assert noz.parse_parameter(-10.5)
    assert not noz.parse_parameter(0)


def test_interval_restriction():
    between1and5 = IntervalConstraint(1, 50)
    bmin100and100 = IntervalConstraint(-100, 100)

    assert between1and5.parse_parameter(1.5)
    assert between1and5.parse_parameter(40.4563)
    assert not between1and5.parse_parameter(1)
    assert not between1and5.parse_parameter(50)
    assert not between1and5.parse_parameter(-5324)
    assert not between1and5.parse_parameter(3425)

    assert bmin100and100.parse_parameter(0)
    assert bmin100and100.parse_parameter(40.4563)
    assert bmin100and100.parse_parameter(-50)
    assert bmin100and100.parse_parameter(-90)
    assert not bmin100and100.parse_parameter(200)
    assert not bmin100and100.parse_parameter(120)
    assert not bmin100and100.parse_parameter(-5324)
    assert not bmin100and100.parse_parameter(-120)


def test_pure_comparisons():
    pos = PositiveConstraint()
    neg = NegativeConstraint()
    noz = NonZeroConstraint()
    pos1 = PositiveConstraint()
    neg1 = NegativeConstraint()
    noz1 = NonZeroConstraint()

    # all correct
    assert pos == pos1
    assert neg == neg1
    assert noz == noz1

    assert not pos == neg
    assert not neg == noz
    assert not noz == pos

    lminus100 = LessThanConstraint(-100)
    l1point5 = LessThanConstraint(1.5)
    lminus1001 = LessThanConstraint(-100)
    l1point51 = LessThanConstraint(1.5)

    gminus100 = GreaterThanConstraint(-100)
    g1point5 = GreaterThanConstraint(1.5)
    gminus1001 = GreaterThanConstraint(-100)
    g1point51 = GreaterThanConstraint(1.5)

    assert lminus100 == lminus1001
    assert l1point5 == l1point51
    assert gminus100 == gminus1001
    assert g1point5 == g1point51

    assert lminus100 != l1point5
    assert l1point5 != lminus1001
    assert gminus100 != g1point5
    assert g1point5 != gminus1001


def test_composite_comparisons():
    between1and5 = IntervalConstraint(1, 50)
    between1and51 = IntervalConstraint(1, 50)

    bmin100and100 = IntervalConstraint(-100, 100)
    bmin100and1001 = IntervalConstraint(-100, 100)

    assert between1and5 == between1and51
    assert bmin100and100 == bmin100and1001

    assert bmin100and100 != between1and5
    assert between1and5 != bmin100and100
    assert bmin100and100 != between1and51
    assert between1and5 != bmin100and1001


def test_invalid_comparisons():
    between1and5 = IntervalConstraint(1, 50)
    pos = PositiveConstraint()

    with pytest.raises(TypeError):
        assert between1and5 == 1

    with pytest.raises(TypeError):
        assert between1and5 == "best"

    with pytest.raises(TypeError):
        assert pos == 1

    with pytest.raises(TypeError):
        assert pos == "best"


def test_composite_constraints_decompose():
    between1and5 = IntervalConstraint(1, 50)
    pos = PositiveConstraint()
    g1point5 = GreaterThanConstraint(1.5)
    noz = NonZeroConstraint()
    a5 = ExactValueConstraint(5)

    comp1 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp11 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp2 = CustomCompositeConstraint([g1point5, between1and5, pos])
    comp3 = CustomCompositeConstraint([pos, between1and5, g1point5])

    comp_comp1 = CustomCompositeConstraint([noz, pos, comp1])

    comp_comp2 = CustomCompositeConstraint([comp_comp1, a5])

    deco_cons1 = CustomCompositeConstraint.decompose_constraints([comp_comp1, comp_comp2, comp3])
    assert len(deco_cons1) == 6

    deco_cons2 = CustomCompositeConstraint.decompose_constraints([comp1, ])
    assert len(deco_cons2) == 4

    with pytest.raises(TypeError):
        _ = CustomCompositeConstraint.decompose_constraints([12132, "fasdfasfd"])

    class MockConsInt(ConstraintInterfaceABC):
        def _hashing_fun(self):
            pass

        def __str__(self):
            pass

        def __repr__(self):
            pass

        @property
        def constraining_function(self):
            return None

    with pytest.raises(NotImplementedError):
        m = MockConsInt("mocky")
        _ = CustomCompositeConstraint.decompose_constraints([comp1, m])

    with pytest.raises(NotImplementedError):
        m = MockConsInt("mocky")
        _ = CustomCompositeConstraint.decompose_constraints([m, ])

    with pytest.raises(NotImplementedError):
        m = MockConsInt("mocky")
        _ = CustomCompositeConstraint.decompose_constraints([m, m])


def test_custom_composite_restriction():
    between1and5 = IntervalConstraint(1, 50)
    pos = PositiveConstraint()
    g1point5 = GreaterThanConstraint(1.5)
    noz = NonZeroConstraint()
    a5 = ExactValueConstraint(5)

    comp1 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp2 = CustomCompositeConstraint([g1point5, between1and5, pos])
    comp3 = CustomCompositeConstraint([pos, between1and5, g1point5])

    # region Order does not matter

    assert not comp1.parse_parameter(-1)
    assert not comp1.parse_parameter(51)
    assert not comp1.parse_parameter(1)

    assert comp1.parse_parameter(5)
    assert comp1.parse_parameter(30)
    assert comp1.parse_parameter(20)

    assert not comp2.parse_parameter(-1)
    assert not comp2.parse_parameter(51)
    assert not comp2.parse_parameter(1)

    assert comp2.parse_parameter(5)
    assert comp2.parse_parameter(30)
    assert comp2.parse_parameter(20)

    assert not comp3.parse_parameter(-1)
    assert not comp3.parse_parameter(51)
    assert not comp3.parse_parameter(1)

    assert comp3.parse_parameter(5)
    assert comp3.parse_parameter(30)
    assert comp3.parse_parameter(20)

    # endregion

    # region Composition of compositions

    comp_comp1 = CustomCompositeConstraint([noz, pos, comp1])

    assert not comp_comp1.parse_parameter(-1)
    assert not comp_comp1.parse_parameter(51)
    assert not comp_comp1.parse_parameter(1)

    assert comp_comp1.parse_parameter(5)
    assert comp_comp1.parse_parameter(30)
    assert comp_comp1.parse_parameter(20)

    comp_comp2 = CustomCompositeConstraint([comp_comp1, a5])

    assert not comp_comp2.parse_parameter(-1)
    assert not comp_comp2.parse_parameter(51)
    assert not comp_comp2.parse_parameter(1)

    assert comp_comp2.parse_parameter(5)
    assert not comp_comp2.parse_parameter(30)
    assert not comp_comp2.parse_parameter(20)

    # endregion


def test_composition_throw_error():

    with pytest.raises(TypeError):
        between1and5 = IntervalConstraint(1, 50)
        pos = PositiveConstraint()
        g1point5 = GreaterThanConstraint(1.5)

        _ = CustomCompositeConstraint([between1and5, pos, g1point5, 1, lambda x: x, "string"])


def test_custom_composite_comparisons():
    between1and5 = IntervalConstraint(1, 50)
    pos = PositiveConstraint()
    g1point5 = GreaterThanConstraint(1.5)
    noz = NonZeroConstraint()
    a5 = ExactValueConstraint(5)

    comp1 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp11 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp2 = CustomCompositeConstraint([g1point5, between1and5, pos])
    comp3 = CustomCompositeConstraint([pos, between1and5, g1point5])

    comp_comp1 = CustomCompositeConstraint([noz, pos, comp1])

    comp_comp2 = CustomCompositeConstraint([comp_comp1, a5])

    assert comp1 == comp11
    assert comp2 == comp1
    assert comp3 == comp1


def test_extract_error():

    between1and5 = IntervalConstraint(1, 50)
    pos = PositiveConstraint()
    g1point5 = GreaterThanConstraint(1.5)
    noz = NonZeroConstraint()
    a5 = ExactValueConstraint(5)

    comp1 = CustomCompositeConstraint([between1and5, pos, g1point5])

    comp_comp1 = CustomCompositeConstraint([noz, pos, comp1])

    comp_comp2 = CustomCompositeConstraint([comp_comp1, a5])

    multicomp = CustomCompositeConstraint([comp_comp2,
                                           comp_comp2,
                                           comp_comp1,
                                           between1and5,
                                           pos,
                                           g1point5])

    assert len(comp1.extract_constraint_error(-1)) == 3
    assert len(comp_comp1.extract_constraint_error(0)) == 4
    assert len(comp_comp2.extract_constraint_error(-1)) == 4
    assert len(multicomp.extract_constraint_error(-1)) == 4
    assert len(multicomp.extract_constraint_error(5)) == 0
    assert len(multicomp.extract_constraint_error(10)) == 1


def test_pure_constraints_repr_str():
    pos = PositiveConstraint()
    g1point5 = GreaterThanConstraint(1.5)
    noz = NonZeroConstraint()
    a5 = ExactValueConstraint(5)

    assert repr(pos) == 'PositiveConstraint(parameter=0, error_message=Value must be positive!)'
    assert repr(g1point5) == 'GreaterThanConstraint(parameter=1.5, error_message=Value must be greater than: 1.5)'
    assert repr(noz) == 'NonZeroConstraint(parameter=0, error_message=Value must not be: 0)'
    assert repr(a5) == 'ExactValueConstraint(parameter=5, error_message=Value must be: 5)'

    assert str(pos) == 'PositiveConstraint >> Value must be positive!'
    assert str(g1point5) == 'GreaterThanConstraint >> Value must be greater than: 1.5'
    assert str(noz) == 'NonZeroConstraint >> Value must not be: 0'
    assert str(a5) == 'ExactValueConstraint >> Value must be: 5'



def test_composite_constraints_repr_str():
    between1and5 = IntervalConstraint(1, 50)
    pos = PositiveConstraint()
    g1point5 = GreaterThanConstraint(1.5)
    noz = NonZeroConstraint()
    a5 = ExactValueConstraint(5)

    comp1 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp11 = CustomCompositeConstraint([between1and5, pos, g1point5])
    comp2 = CustomCompositeConstraint([g1point5, between1and5, pos])
    comp3 = CustomCompositeConstraint([pos, between1and5, g1point5])

    comp_comp1 = CustomCompositeConstraint([noz, pos, comp1])

    comp_comp2 = CustomCompositeConstraint([comp_comp1, a5])

    assert repr(comp1) == 'CustomCompositeConstraint(constraints_list=[<#4>], error_message=None)'
    assert repr(comp2) == 'CustomCompositeConstraint(constraints_list=[<#4>], error_message=None)'
    assert repr(comp3) == 'CustomCompositeConstraint(constraints_list=[<#4>], error_message=None)'
    assert repr(comp_comp1) == 'CustomCompositeConstraint(constraints_list=[<#5>], error_message=None)'
    assert repr(comp_comp2) == 'CustomCompositeConstraint(constraints_list=[<#6>], error_message=None)'

    assert str(comp1) == 'CustomCompositeConstraint<4>> err: None'
    assert str(comp2) == 'CustomCompositeConstraint<4>> err: None'
    assert str(comp3) == 'CustomCompositeConstraint<4>> err: None'
    assert str(comp_comp1) == 'CustomCompositeConstraint<5>> err: None'
    assert str(comp_comp2) == 'CustomCompositeConstraint<6>> err: None'









