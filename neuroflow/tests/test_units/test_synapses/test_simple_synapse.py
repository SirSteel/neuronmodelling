import pytest
import numpy as np
from neuroflow.units.synapses.simple_synapse import SimpleSynapse


def test_synapse_creation():
    _ = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    _ = SimpleSynapse(uid=2, pre="best_neuron", post="second_best", g_max=1.8, tau=2.6, lambda_i=3.7)
    _ = SimpleSynapse(uid=3, pre="Mocky", post="Blaaaa", g_max=1.2, tau=2.5, lambda_i=3)


def test_synapse_invalid_creation():
    with pytest.raises(TypeError):
        _ = SimpleSynapse(1, g_max=1, tau=2, lambda_i=3)

    with pytest.raises(TypeError):
        _ = SimpleSynapse("AVA", g_max=1, tau=2, lambda_i=3)

    with pytest.raises(ValueError):
        _ = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3, extra=5)

    with pytest.raises(ValueError):
        _ = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2)

    with pytest.raises(ValueError):
        _ = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i="a")


def test_synapse_simple_computation():
    _ = SimpleSynapse(uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)

    arr1 = np.array([1, 2, 3])
    arr2 = np.array([1, 2, 3])
    arr3 = np.array([1, 2, 3])

    _ = SimpleSynapse.main_equation(g_max=arr1, tau=arr2, lambda_i=arr3)