import pytest
import inspect
from neuroflow.units import unit_factories as uf
from neuroflow.units import NeuronABC, ModelUnitABC, SynapseABC
from neuroflow.utilities import ModuleImporter, ModuleImporterInterface


def test_base_factory_class():
    class MockSuperclass(ModelUnitABC):
        def __repr__(self):
            pass

        def __str__(self):
            pass

        def __eq__(self, other):
            pass

        @property
        def unpack(self) -> tuple:
            pass

        @classmethod
        def pack(cls, *loading_tuple):
            pass

        @classmethod
        def main_equation(cls, *args, **kwargs):
            pass

        @classmethod
        def parameter_constraints(cls):
            pass

    class MockImporterClass(ModuleImporterInterface):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def get_class(self, class_name):
            pass

    class FakeFactory(uf.FactoryABC):

        def __new__(cls):
            return super().__new__(cls, factory_superclass=MockSuperclass, module_importer=MockImporterClass)

        def create_unit(self, neuron_type: str, neuron_name: str, **neuron_parameters):
            pass

    x = FakeFactory()
    assert isinstance(x, uf.FactoryABC)


def test_base_factory_class_operationality():
    class MockSuperclass(ModelUnitABC):
        def __repr__(self):
            pass

        def __str__(self):
            pass

        def __eq__(self, other):
            pass

        @property
        def unpack(self) -> tuple:
            pass

        @classmethod
        def pack(cls, *loading_tuple):
            pass

        @classmethod
        def main_equation(cls, *args, **kwargs):
            pass

        @classmethod
        def parameter_constraints(cls):
            pass

    class MockImporterClass(ModuleImporterInterface):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def get_class(self, class_name):
            if class_name == "mock_key_error":
                raise KeyError()
            elif class_name == "ok":
                return None

    class FakeFactory(uf.FactoryABC):

        def __new__(cls,
                    factory_superclass=MockSuperclass,
                    module_importer=MockImporterClass):
            return super().__new__(cls, factory_superclass=factory_superclass, module_importer=module_importer)

        def create_unit(self, neuron_type: str, neuron_name: str, **neuron_parameters):
            pass

    x = FakeFactory()
    assert isinstance(x, uf.FactoryABC)
    assert isinstance(x.importer, MockImporterClass)
    assert inspect.isclass(x.factory_superclass)

    with pytest.raises(ValueError):
        _ = x.get_unit("mock_key_error")

    n = x.get_unit("ok")
    assert n is None

    with pytest.raises(TypeError):
        _ = FakeFactory(factory_superclass="test", module_importer=MockImporterClass)

    with pytest.raises(TypeError):
        _ = FakeFactory(factory_superclass=MockSuperclass, module_importer="test")


def test_neuron_factory():
    n_factory = uf.NeuronFactory()

    n1 = n_factory.create_unit("SimpleNeuron", "AVA", g_max=1, tau=2, lambda_i=3)
    n2 = n_factory.create_unit("SimpleNeuron", "AVB", g_max=1, tau=2, lambda_i=3)
    n3 = n_factory.create_unit("SimpleNeuron", "AVC", g_max=1, tau=2, lambda_i=3)

    for neuron_obj in [n1, n2, n3]:
        assert isinstance(neuron_obj, NeuronABC)
        neuron_type, neuron_name, params = neuron_obj.unpack
        assert neuron_type == "SimpleNeuron"
        assert neuron_obj.neuron_name == neuron_name


def test_synapse_factory_random_uid():
    n_factory = uf.SynapseFactory()

    s1 = n_factory.create_unit("SimpleSynapse", pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    s2 = n_factory.create_unit("SimpleSynapse", pre="AVB", post="PVC", g_max=1, tau=2, lambda_i=3)
    s3 = n_factory.create_unit("SimpleSynapse", pre="AVC", post="AVB", g_max=1, tau=2, lambda_i=3)
    s4 = n_factory.create_unit("SimpleSynapse", pre="AVD", post="AVA", g_max=1, tau=2, lambda_i=3)

    for synapse_obj in [s1, s2, s3, s4]:
        assert isinstance(synapse_obj, SynapseABC)
        syn_type, uid, pre, post, params = synapse_obj.unpack
        assert syn_type == "SimpleSynapse"
        assert synapse_obj.pre == pre
        assert synapse_obj.post == post


def test_synapse_factory_assigned_uid():
    n_factory = uf.SynapseFactory()

    s1 = n_factory.create_unit("SimpleSynapse", uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)
    s2 = n_factory.create_unit("SimpleSynapse", uid=2, pre="AVB", post="PVC", g_max=1, tau=2, lambda_i=3)

    with pytest.raises(TypeError):
        _ = n_factory.create_unit("SimpleSynapse", uid="string", pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)

    with pytest.raises(ValueError):
        _ = n_factory.create_unit("SimpleSynapse", uid=1, pre="AVA", post="AVB", g_max=1, tau=2, lambda_i=3)




