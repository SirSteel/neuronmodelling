import pytest
from neuroflow.utilities import verify_type, lambda_code_fingerprint


def test_verifier():
    flvar = 5.45
    strvar = "str"
    intvar = 23

    verify_type("test", intvar, int)
    verify_type("test", flvar, float)
    verify_type("test", strvar, str)

    with pytest.raises(TypeError) as err:
        verify_type("test", intvar, float)
    assert str(err.value) == "The variable <test> must be of type <float>, got <int> instead!"

    with pytest.raises(TypeError) as err:
        verify_type("test", flvar, str)
    assert str(err.value) == "The variable <test> must be of type <str>, got <float> instead!"

    with pytest.raises(TypeError) as err:
        verify_type("test", strvar, int)
    assert str(err.value) == "The variable <test> must be of type <int>, got <str> instead!"

    verify_type("test", intvar, [int, float])
    verify_type("test", strvar, [str, int])
    verify_type("test", flvar, [int, float])

    with pytest.raises(TypeError) as err:
        verify_type("test", strvar, [int, float])
    assert str(err.value) == "The variable <test> must be of type <int, float>, got <str> instead!"

    with pytest.raises(TypeError) as err:
        verify_type("test", flvar, [int, str])
    assert str(err.value) == "The variable <test> must be of type <int, str>, got <float> instead!"

    """
        if isinstance(variable_name, str):
        raise TypeError("The argument variable_name, must be string!")

    # first check if it is either one class type, or if it is a list
    elif not(isinstance(expected_type, list) or inspect.isclass(expected_type)):

        raise TypeError("The argument expected_type must be either a list, or a class!")
        
        # if it is a list, check if all the elements are actually classes
    elif isinstance(expected_type, list) and not all(inspect.isclass(type_el) for type_el in expected_type):

        raise TypeError("If the argument expected_type is a list, it must only contain class elements")
        
        
    """

    # check errors if called wrong!
    with pytest.raises(TypeError) as err:
        verify_type(123, flvar, [int, str])
    assert str(err.value) == "The argument variable_name, must be string!"

    with pytest.raises(TypeError) as err:
        verify_type("test", flvar, 123)
    assert str(err.value) == "The argument expected_type must be either a list, or a class!"

    with pytest.raises(TypeError) as err:
        verify_type("test", flvar, [int, str, 1234])
    assert str(err.value) == "If the argument expected_type is a list, it must only contain class elements"


def test_lambda_fingerprinting():
    x = lambda x: x ** 2
    y = lambda x: x ** 2
    z = lambda y: y ** 2
    k = lambda x: x ** 3
    n = lambda m: m ** 3

    assert lambda_code_fingerprint(x) == lambda_code_fingerprint(y)

    assert lambda_code_fingerprint(z) == lambda_code_fingerprint(x)
    assert lambda_code_fingerprint(z) == lambda_code_fingerprint(y)

    assert lambda_code_fingerprint(k) != lambda_code_fingerprint(x)
    assert lambda_code_fingerprint(k) != lambda_code_fingerprint(y)
    assert lambda_code_fingerprint(k) != lambda_code_fingerprint(z)

    assert lambda_code_fingerprint(k) == lambda_code_fingerprint(n)
    assert lambda_code_fingerprint(n) == lambda_code_fingerprint(k)




