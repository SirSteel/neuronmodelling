import pytest
import os

from neuroflow.utilities.module_importer import ModuleImporter
from neuroflow.utilities.module_importer import _is_valid_module
from neuroflow.utilities.module_importer import _is_valid_path
from neuroflow.utilities.module_importer import get_modules_in_folder
from neuroflow.utilities.module_importer import get_modules_in_subfolders
from neuroflow.utilities.module_importer import get_module_classes


def test_module_validity():
    assert _is_valid_module("python.py")
    assert _is_valid_module("python.pyc")
    assert _is_valid_module("python.pyo")

    assert not _is_valid_module("python")
    assert not _is_valid_module("python.png")
    assert not _is_valid_module("python.bin")

    assert not _is_valid_module("__init__.py")


def test_path_filter():
    assert _is_valid_path("/home/lzelezni/PycharmProjects/neuroflow/neuroflow/units")
    assert _is_valid_path("/home/lzelezni/PycharmProjects/neuroflow/neuroflow/units/")
    assert _is_valid_path("/home/lzelezni/PycharmProjects/neuroflow/")
    assert _is_valid_path("/home/lzelezni/PycharmProjects")

    assert not _is_valid_path("/home/lzelezni/PycharmProjects/neuroflow/neuroflow/__pycache__")
    assert not _is_valid_path("/home/lzelezni/PycharmProjects/neuroflow/neuroflow/__pycache__/")
    assert not _is_valid_path("/home/__pycache__/PycharmProjects/neuroflow/")
    assert not _is_valid_path("/home/__pycache__/PycharmProjects")

    assert not _is_valid_path("/home/__pycache__/__pycache__")
    assert not _is_valid_path("/home/__pycache__/__pycache__/")

    assert not _is_valid_path("/home/lzelezni/PycharmProjects/venv")
    assert not _is_valid_path("/home/lzelezni/PycharmProjects/venv/")
    assert not _is_valid_path("/home/lzelezni/PycharmProjects/.cache")
    assert not _is_valid_path("/home/lzelezni/PycharmProjects/.cache/")


def create_fake_class(class_name):
    class_def = "import numpy as np \n" \
                "from neuroflow.units import NeuronABC \n" \
                "from neuroflow.parameters import PreparedConstraintsCollection \n\n" \
                "class {}(NeuronABC): \n" \
                "    @staticmethod\n" \
                "    def main_equation(g_max):\n" \
                "        pass\n\n" \
                "    @classmethod\n" \
                "    def parameter_constraints(cls) -> dict:\n" \
                "        with PreparedConstraintsCollection() as rest:\n" \
                "            rest.register('g_max', 'not0')\n" \
                "        return rest\n" \
                "\n" \
                "\n".format(class_name)
    return class_def


def create_mock_fs(fs):
    fs.CreateFile('/tmp/home/pymod1.py', contents=create_fake_class("PyClass1"))
    fs.CreateFile('/tmp/home/pymod2.py', contents=create_fake_class("PyClass2"))
    fs.CreateFile('/tmp/home/__init__.py')

    fs.CreateFile('/tmp/home/fol1/fol11/__init__.py')
    fs.CreateFile('/tmp/home/fol1/fol11/pymod3.py', contents=create_fake_class("PyClass3"))
    fs.CreateFile('/tmp/home/fol1/fol11/pymod4.py', contents=create_fake_class("PyClass4"))
    fs.CreateFile('/tmp/home/fol1/fol11/pymod5.py', contents=create_fake_class("PyClass5"))

    fs.CreateFile('/tmp/home/fol1/fol12/pymod6.py')
    fs.CreateFile('/tmp/home/fol1/fol12/pymod7.py')
    fs.CreateFile('/tmp/home/fol1/fol12/pymod8.py')

    fs.CreateFile('/tmp/home/fol2/test.img')
    fs.CreateFile('/tmp/home/fol2/pymod9.py')
    fs.CreateFile('/tmp/home/fol2/pymod10.py')
    fs.CreateFile('/tmp/home/fol2/pymod11.py')
    fs.CreateFile('/tmp/home/fol2/pymod12.py')
    fs.CreateFile('/tmp/home/fol2/pymod13.py')
    fs.CreateFile('/tmp/home/fol2/pymod14.py')
    fs.CreateFile('/tmp/home/fol2/pymod15.py')

    fs.CreateFile('/tmp/home/fol3/mocky.png')

    fs.CreateFile('/tmp/home/fol4/mocky4.png')

    fs.CreateFile('/tmp/home/fol4/fol41/pymod19.py')

    fs.CreateFile('/tmp/home/fol4/fol41/fol411/pymod16.py')
    fs.CreateFile('/tmp/home/fol4/fol41/fol411/pymod17.py')

    fs.CreateFile('/tmp/home/fol5/pymod18.py')
    fs.CreateFile('/tmp/home/fol5/pymod18.png')
    fs.CreateFile('/tmp/home/fol5/fol51/mockfake.png')


def test_get_modules_in_folder(fs):

    create_mock_fs(fs)

    mod_list = get_modules_in_folder("/tmp/home/fol2/", with_paths=False)
    assert len(mod_list) == 7
    assert "pymod12.py" in mod_list

    mod1_list = get_modules_in_folder("/tmp/home/fol2/")

    for filename, path in mod1_list:
        assert isinstance(filename, str)
        root, fn = os.path.split(path)
        assert filename == fn


def test_get_modules_in_subfolders(fs):

    create_mock_fs(fs)

    mod_list = get_modules_in_subfolders("/tmp/home/", with_paths=False)
    assert len(mod_list) == 19

    for curr_num in range(1, 20):
        name = "pymod{}.py".format(curr_num)
        assert name in mod_list

    mod1_list = get_modules_in_subfolders("/tmp/home/")

    for filename, path in mod1_list:
        assert isinstance(filename, str)
        root, fn = os.path.split(path)
        assert filename == fn
