from abc import abstractmethod, ABCMeta
from collections import namedtuple

from neuroflow.units import unit_abc

from neuroflow.utilities import verify_type


class NeuronABC(unit_abc.ModelUnitABC, metaclass=ABCMeta):
    """ The neuron base class, implements the naming conventions,
    repr and strings, comparisons , unpacking,loading and properties """

    NeuronUnitForm = namedtuple('NeuronUnitForm', ['type', 'name', 'parameters'])

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __new__(cls, neuron_name, **kwargs):
        """ The instance creation method

        It uses its base class ModelUnitABC, to initialize the parameters, and only sets the neuron name to the object

        :param neuron_name: A string, as a name of the neuron
        :param kwargs: The parameters of the neuron
        :return: A new neuron unit instance
        """

        # check if neuron_name is a string
        verify_type("neuron_name", neuron_name, str)

        instance = super().__new__(cls, parameters=kwargs)
        setattr(instance, "_neuron_name", neuron_name)

        return instance

    def __repr__(self):
        """ A representation of the neuron, used mostly of developer purposes
        Basically a representation of how the neuron was instantiated

        :return: something in the lines of NeuronType(neuron_name='AVA', g_max=5, m_C=1.4)
        """

        return '{n_type}(neuron_name="{n_name}", ' \
               '**parameters <{params}>)'.format(n_type=self.__class__.__name__,
                                                 n_name=self.neuron_name,
                                                 params=str(self.parameters))

    def __str__(self):
        """ A string representation of the neuron"""
        return "{n_name}<{n_type}({params})>".format(n_type=self.__class__.__name__,
                                                     n_name=self.neuron_name,
                                                     params=", ".join(self.expected_parameters()))

    def __eq__(self, other):
        """ The comparison function to compare neuron classes
        - Must be of NeuronABC class first
        - Must be same class Type
        - Must have the same name
        - Must have the same ParametersStore (this falls back to parameters comparison function)

        :param other: The other NeuronABC subclass
        :return: True if the objects match
        """

        # not NeuronABC
        if not issubclass(other.__class__, NeuronABC):
            raise TypeError("Cannot compare types which are not NeuronABC subclasses")
        #
        # not the same type
        elif self.__class__.__name__ != other.__class__.__name__:
            return False
        #
        # not the same name
        elif self.neuron_name != other.neuron_name:
            return False
        #
        # not the same parameter_names
        elif self.parameters != other.parameters:
            return False

        # if it all passes it is the same neuron
        else:
            return True

    # endregion

    #################
    #  Properties   #
    #################

    # region Properties

    @property
    def neuron_name(self):
        """ The neurons name is one of its main properties """
        return getattr(self, "_neuron_name")

    @property
    def unpack(self) -> tuple:
        """ Unpacking function to extract neuron name plus parameter_names

        :return: A tuple with synapses_type , neuron name and parameter_names
        """
        return self.NeuronUnitForm(type=self.__class__.__name__,
                                   name=self.neuron_name,
                                   parameters=self.parameters.unpack)

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region AbstractMethods

    @classmethod
    @abstractmethod
    def parameter_constraints(cls):
        """
        This function should basically return a parameter constraints object, which will be used at instantiation
        each class type thus defines its own constraints on the parameters shared across all instances
        :return: RestrictionsABC object
        """
        raise NotImplementedError  # pragma: nocover

    @classmethod
    @abstractmethod
    def main_equation(cls, *args, **kwargs):
        """ The main equation of the ModelUnit

        It must be implemented as a class method since its class will have one such function and might reference other
        functions defined in itself, also the function can then be implemented to be vectorized!
        """
        raise NotImplementedError   # pragma: nocover

    # endregion


if __name__ == "__main__":
    raise RuntimeError("This module should not be called directly!")  # pragma: nocover
