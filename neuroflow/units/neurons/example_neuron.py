import numpy as np
import scipy as sp
import pylab as plt
import random

import scipy.interpolate as scp_interpolate
from scipy.integrate import odeint
from functools import partial

from neuroflow import units
from neuroflow.units import parameters as param_pkg


class ExampleNeuron(units.NeuronABC):

    @classmethod
    def parameter_constraints(cls):
        """
        cell_capacitance = 1
        g_leak = 0.3
        v_leak = 54.387
        :return:
        """
        with param_pkg.PreparedConstraintsCollection() as rest:
            rest.register("cell_capacitance", "not0")
            rest.register("g_leak", "not0")
            rest.register("v_leak", "not0")
            return rest

    @classmethod
    def input_voltage1(cls, t):
        # high = 50
        # low = 0
        # high_or_low = False
        #
        # if t != 0 and t > 10 and t % 40 < 2 and high_or_low == False:
        #     high_or_low = True
        # elif t != 0 and t % 40 == 0 and high_or_low == True:
        #     high_or_low = False
        #
        # if high_or_low is True:
        #     return high
        # else:
        #     return low
        # return 10 * (t > 100) - 10 * (t > 200) + 35 * (t > 300) - 35 * (t > 400)
        # return np.sin(t/32)

        min_val = 0
        max_val = 30
        # high_or_low = False

        t_divisible = (t // 100) + 15

        if t_divisible % 2 == 0:
            high_or_low = False
        else:
            high_or_low = True

        noise = lambda val: val
        if np.random.choice(2, 1, p=[0.015, 0.985])[0] == 0:
            noise = lambda val: val + val * (random.randint(-500, 500) / 10000)

        if high_or_low is True:
            return noise(max_val)
        else:
            return noise(min_val)

    @classmethod
    def input_voltage(cls, t):
        pass

    @classmethod
    def main_equation(cls, v, timestep, params):

        cell_capacitance = params["cell_capacitance"]
        g_leak = params["g_leak"]
        v_leak = params["v_leak"]
        # inter = params["input_inter"]

        # current_input = cls.input_voltage(timestep)
        current_input = 0
        # current_input = inputs(timestep)
        curr_voltage = v

        dvdt = (g_leak/cell_capacitance) * (v_leak - curr_voltage) + current_input/cell_capacitance

        return dvdt


class VirtualNeuron(units.NeuronABC):

    @classmethod
    def parameter_constraints(cls):
        """
        cell_capacitance = 1
        g_leak = 0.3
        v_leak = 54.387
        :return:
        """
        with param_pkg.PreparedConstraintsCollection() as rest:
            return rest

    @classmethod
    def main_equation(cls, v, timestep, params):

        cell_capacitance = params["cell_capacitance"]
        g_leak = params["g_leak"]
        v_leak = params["v_leak"]
        # inter = params["input_inter"]

        # current_input = cls.input_voltage(timestep)
        current_input = 0
        # current_input = inputs(timestep)
        curr_voltage = v

        dvdt = (g_leak/cell_capacitance) * (v_leak - curr_voltage) + current_input/cell_capacitance

        return dvdt


def f(x):
    y = 0
    result = []
    for _ in x:
        result.append(y)
        y += np.random.normal(scale=1)
    return np.array(result)


def create_interpolator(vector, kind="cubic"):
    vector_len = len(vector)
    sample_points = np.linspace(0, vector_len, num=vector_len, endpoint=True)
    return scp_interpolate.interp1d(sample_points, vector, kind=kind)


def solve_system_equation(input_values, timestep, neuron_params, neuron_equation, synapse_params, synapse_equation):
    nrn_eqs = partial(neuron_equation, params=neuron_params)
    syn_eqs = partial(synapse_equation, params=synapse_params)

    def eqs(input, timestep):

        dvdt = nrn_eqs(input[0], timestep)
        didt = syn_eqs((dvdt, input[1]), timestep)

        return dvdt, didt


def main():

    input_values = f(np.linspace(0, 1000, 1000))

    params = {"cell_capacitance"    :   2,
              "g_leak"              : 0.1,
              "v_leak"              :  -50,}

    my_neuron = ExampleNeuron("Spikey", **params)

    t = sp.arange(0.0, 450.0, 0.01)
    """ The time to integrate over """

    # diff_eq = partial(my_neuron.main_equation, params=params, inputs=)

    v = odeint(my_neuron.main_equation, [-50], t, args=(params,))
    v1 = v[:, 0]
    # v2 = v[:, 1]

    v3 = np.vectorize(my_neuron.input_voltage)(t)

    plt.figure()
    plt.title('Example Neuron')
    plt.plot(t, v1)
    # plt.plot(t, v2)
    plt.plot(t, v3)
    plt.ylabel('V (mV)')
    plt.show()


if __name__ == "__main__":
    main()
