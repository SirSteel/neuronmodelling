import scipy as sp
import pylab as plt
from scipy.integrate import odeint
from functools import partial

from neuroflow import units
from neuroflow.units import parameters as param_pkg


class HodginHuxley(units.NeuronABC):

    @classmethod
    def alpha_m(cls, V):
        """Channel gating kinetics. Functions of membrane voltage"""
        return 0.1*(V+40.0)/(1.0 - sp.exp(-(V+40.0) / 10.0))

    @classmethod
    def beta_m(cls, V):
        """Channel gating kinetics. Functions of membrane voltage"""
        return 4.0*sp.exp(-(V+65.0) / 18.0)

    @classmethod
    def alpha_h(cls, V):
        """Channel gating kinetics. Functions of membrane voltage"""
        return 0.07*sp.exp(-(V+65.0) / 20.0)

    @classmethod
    def beta_h(cls, V):
        """Channel gating kinetics. Functions of membrane voltage"""
        return 1.0/(1.0 + sp.exp(-(V+35.0) / 10.0))

    @classmethod
    def alpha_n(cls, V):
        """Channel gating kinetics. Functions of membrane voltage"""
        return 0.01*(V+55.0)/(1.0 - sp.exp(-(V+55.0) / 10.0))

    @classmethod
    def beta_n(cls, V):
        """Channel gating kinetics. Functions of membrane voltage"""
        return 0.125*sp.exp(-(V+65) / 80.0)

    @classmethod
    def I_Na(cls, V, m, h, g_Na, E_Na):
        """
        Membrane current (in uA/cm^2)
        Sodium (Na = element name)

        |  :param V:
        |  :param m:
        |  :param h:
        |  :return:
        """
        return g_Na * m**3 * h * (V - E_Na)

    @classmethod
    def I_K(self, V, n, g_K, E_K):
        """
        Membrane current (in uA/cm^2)
        Potassium (K = element name)

        |  :param V:
        |  :param h:
        |  :return:
        """
        return g_K * n**4 * (V - E_K)

    @classmethod
    def I_L(cls, V, g_L, E_L):
        """
        Membrane current (in uA/cm^2)
        Leak

        |  :param V:
        |  :param h:
        |  :return:
        """
        return g_L * (V - E_L)

    @classmethod
    def I_inj(cls, t):
        """
        External Current

        |  :param t: time
        |  :return: step up to 10 uA/cm^2 at t>100
        |           step down to 0 uA/cm^2 at t>200
        |           step up to 35 uA/cm^2 at t>300
        |           step down to 0 uA/cm^2 at t>400
        """
        return 10*(t > 100) - 10*(t > 200) + 35*(t > 300) - 35*(t > 400)

    @classmethod
    def main_equation(cls, X, t, params, **kwargs):
        """
        Integrate

        |  :param X:
        |  :param t:
        |  :return: calculate membrane potential & activation variables
        """
        E_Na = params["E_Na"]
        g_Na = params["g_Na"]
        E_K = params["E_K"]
        g_K = params["g_K"]
        E_L = params["E_L"]
        g_L = params["g_L"]
        C_m = params["C_m"]

        V, m, h, n = X

        dVdt = (cls.I_inj(t) -
                cls.I_Na(V, m, h, g_Na=g_Na, E_Na=E_Na) -
                cls.I_K(V, n, E_K=E_K, g_K=g_K) -
                cls.I_L(V, E_L=0, g_L=g_L)) / C_m

        dmdt = cls.alpha_m(V)*(1.0-m) - cls.beta_m(V)*m

        dhdt = cls.alpha_h(V)*(1.0-h) - cls.beta_h(V)*h

        dndt = cls.alpha_n(V)*(1.0-n) - cls.beta_n(V)*n

        return dVdt, dmdt, dhdt, dndt

    @classmethod
    def parameter_constraints(cls):
        with param_pkg.PreparedConstraintsCollection() as rest:
            rest.register("C_m", "not0")
            rest.register("g_Na", "not0")
            rest.register("g_K", "not0")
            rest.register("g_L", "not0")
            rest.register("E_Na", "not0")
            rest.register("E_K", "not0")
            rest.register("E_L", "not0")
            return rest


def main():

    # C_m  =   1.0
    # """membrane capacitance, in uF/cm^2"""
    #
    # g_Na = 120.0
    # """Sodium (Na) maximum conductances, in mS/cm^2"""
    #
    # g_K  =  36.0
    # """Postassium (K) maximum conductances, in mS/cm^2"""
    #
    # g_L  =   0.3
    # """Leak maximum conductances, in mS/cm^2"""
    #
    # E_Na =  50.0
    # """Sodium (Na) Nernst reversal potentials, in mV"""
    #
    # E_K  = -77.0
    # """Postassium (K) Nernst reversal potentials, in mV"""
    #
    # E_L  = -54.387
    # """Leak Nernst reversal potentials, in mV"""

    params = {"C_m"  :   1.0,
              "g_Na" : 120.0,
              "g_K"  :  36.0,
              "g_L"  :   0.3,
              "E_Na" :  50.0,
              "E_K"  : -77.0,
              "E_L"  : -54.387}

    my_neuron = HodginHuxley("Spikey", **params)

    t = sp.arange(0.0, 450.0, 0.01)
    """ The time to integrate over """

    diff_eq = partial(my_neuron.main_equation, params=params)

    X = odeint(diff_eq, [-65, 0.05, 0.6, 0.32], t)
    V = X[:, 0]

    plt.figure()
    plt.title('Hodgkin-Huxley Neuron')
    plt.plot(t, V, 'k')
    plt.ylabel('V (mV)')
    plt.show()


if __name__ == "__main__":
    main()