import re
from abc import abstractmethod, ABCMeta
from collections import OrderedDict
from neuroflow.units.parameters import constraints_gallery as res_gallery
from neuroflow.utilities import verify_type


class ConstraintsCollectionsInterface(metaclass=ABCMeta):
    @abstractmethod
    def verify_parameter(self, parameter_name, parameter_value):
        """ an abstract interface that runs the verification on a parameter based on its name and value """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def check_parameter(self, parameter_name, parameter_value):
        """ an abstract interface that runs the verification on a parameter based on its name and value """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def register(self, *args, **kwargs):
        """ the method that allows adding new parameter constraints """
        raise NotImplementedError  # pragma: no cover


class ConstraintsCollectionABC(ConstraintsCollectionsInterface, metaclass=ABCMeta):
    """ The ConstraintsCollectionABC defines an abstract base class as an interface

        the interface requires a locked parameter to prevent modification
        and a parameter restriction which is an OrderedDict, with parameter names as keys and the
        values are the callables, which are called when a parameter is verified
    """
    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __init__(self):
        """
        Initialize the collection, the interface requires a locked parameter to prevent modification
        and a parameter restriction which is an OrderedDict, with parameter names as keys and the
        values are the callables

        """
        self._parameter_constraints = OrderedDict()
        self._locked = False

    def __eq__(self, other):
        """
        Enables the comparison between the parameter restriction collection, which is a fallback
        on the comparisons between RestrictionsABC

        :param other: The other RestrictionCollectionABC to be compared to
        :return:
        """
        verify_type("other", other, ConstraintsCollectionABC)
        return self.parameter_constraints == other.parameter_constraints

    def __ne__(self, other):
        """ inequality is defined as the inverse of equality """
        return not self.__eq__(other)

    def __contains__(self, parameter_name):
        """ checks if the argument is present in the constraints """
        return parameter_name in self.parameter_names

    def __enter__(self):
        """ ContextManager protocol! __enter__, returns itself as the object if it is not locked
        if it is, then it throws an error
        """
        if self.locked:
            raise RuntimeError("The object is locked and can therefore no longer be modified!")
        else:
            return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ ContextManager protocol! __exit__ handles the exit out of the with statement block
            if there is no exception lock the object, if there is an expection do not lock the object
        """
        if exc_type is None:
            self.locked = True
        else:
            return False

    def __str__(self):
        return "Constraints Collection -> ({})".format(", ".join(self.parameter_names))

    def __repr__(self):
        return "{me}() -> ({res})".format(me=self.__class__.__name__,
                                        res=", ".join(self.parameter_names))

    # endregion

    ###########################
    # Public Concrete Methods #
    ###########################

    # region Concrete methods

    @property
    def locked(self):
        """ the locked property defines if the constraints can be modified """
        return self._locked

    @locked.setter
    def locked(self, lock: bool):
        """ The locked setter, only allows the locked flag to be set once to true
            then the object is considered locked

        :param lock: boolean
        :return:
        """
        if not self.locked and lock is True:
            self._locked = lock
        elif self.locked and lock is False:
            raise ValueError("Cannot unlock parameter constraints once locked!")
        else:
            pass

    @property
    def parameter_constraints(self):
        """ the property of parameter constraints containing the object holding the constraints """
        return self._parameter_constraints

    @property
    def parameter_names(self):
        """ get the names of the parameters that are set"""
        return self._parameter_constraints.keys()

    def check_parameter(self, parameter_name, parameter_value):
        """ The concrete method to verify the parameters using the concrete constraints

        :param parameter_name: a string as the parameter name
        :param parameter_value: the value of the parameter (int, float)
        :return: True if parameter passes
        """

        verify_type("parameter_name", parameter_name, str)
        verify_type("parameter_value", parameter_value, [int, float])

        if parameter_name not in self.parameter_constraints:
            raise KeyError("Unexpected parameter <{}>".format(parameter_name))

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region Abstractions

    @abstractmethod
    def register(self, *args, **kwargs):
        """ the method that allows adding new parameter constraints """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def verify_parameter(self, parameter_name, parameter_value):
        """ an abstract interface that runs the verification on a parameter based on its name and value """
        raise NotImplementedError  # pragma: no cover

    # endregion


class PreparedConstraintsCollection(ConstraintsCollectionABC, ConstraintsCollectionsInterface):

    positive_pattern = re.compile(r"^[+]$")
    negative_pattern = re.compile(r"^[-]$")
    not_pattern = re.compile(r"^not([-+]*[0-9]+)$")
    ex_pattern = re.compile(r"^ex([-+]*[0-9]+)$")
    more_pattern = re.compile(r"^>([-+]*[0-9]+)$")
    less_pattern = re.compile(r"^<([-+]*[0-9]+)$")
    interval_pattern = re.compile(r"^([-+]*[0-9]+)<x<([-+]*[0-9]+)$")

    # constraints_control_tokens = {"positive":   (res_gallery.PositiveConstraint,        positive_pattern),
    #                               "negative":   (res_gallery.NegativeConstraint,        negative_pattern),
    #                               "not":        (res_gallery.NotExactValueConstraint,   not_pattern),
    #                               "ex":         (res_gallery.ExactValueConstraint,      ex_pattern),
    #                               "more":       (res_gallery.GreaterThanConstraint,     more_pattern),
    #                               "less":       (res_gallery.LessThanConstraint,        less_pattern),
    #                               "interval":   (res_gallery.IntervalConstraint,        interval_pattern)}

    def parse_constraints_string(self, restriction_string: str) -> set:
        """
        The overly-complex function to parse the restriction control sequence strings

        Control Sequence        Meaning                     Explanation
        ---------------------------------------------------------------------------------------------
        +                       Positive only               Allow only positive values
        -                       Negative only               Allow only negative values
        notX                    Not X value                 Do not allow values X
        exX                     Must be X                   Only allow values X
        >X                      More then X                 Values must be more then X
        <X                      Less then X                 Values must be less then X
        M<x<N                   Interval M, N               Must be more then M but less then N

        :param restriction_string: a string with control sequences
        :return: return the gathered restriction instances, conserve only unique
        """
        gathered_constraints = set()

        # token = namedtuple('token', ['type', 'value'])
        #
        # # grammar = r'\+|\-|\bnot\b|\bex\b|\>|\<|[a-zA-Z0-9_]+'
        # grammar = re.compile(r'|'.join(value[1] for value in self.constraints_control_tokens.values()))
        #
        # tokens = {'plus': '\+', 'minus': '\-', 'not': r'\bnot\b', 'ex': r'\bex\b', 'lt': '\<', 'gt': '\>',
        #           'var': '[a-zA-Z0-9_]+'}
        #
        # sample_input = 'val1+val23; val1 < val3 < new_variable; ex val3;not secondvar;'
        #
        # caught_tokens = re.findall(grammar, restriction_string)
        #
        # tokenized_grammar = [token([a for a, b in tokens.items() if re.findall(b, i)][0], i) for i in
        #                      re.findall(grammar, restriction_string)]
        #
        # for t in tokenized_grammar:
        #
        #     if t.type in self.constraints_control_tokens:
        #
        #         if t.value is not None:
        #             t_obj = self.constraints_control_tokens[t.type][0](*t.value)
        #
        #         else:
        #             t_obj = self.constraints_control_tokens[t.type][0]()
        #
        #         gathered_constraints.add(t_obj)
        #     else:
        #         raise ValueError("The constraints string got unknown control sequences")

        for control_seq in restriction_string.split(";"):
            stripped = control_seq.strip().replace(" ", "")

            if stripped == "":
                continue

            elif stripped == "+":
                gathered_constraints.add(res_gallery.PositiveConstraint())

            elif stripped == "-":
                gathered_constraints.add(res_gallery.NegativeConstraint())

            elif self.not_pattern.match(stripped):

                searched = re.search(self.not_pattern, stripped)
                param = float(searched.group(1))
                gathered_constraints.add(res_gallery.NotExactValueConstraint(param))

            elif self.ex_pattern.match(stripped):

                searched = re.search(self.ex_pattern, stripped)
                param = float(searched.group(1))
                gathered_constraints.add(res_gallery.ExactValueConstraint(param))

            elif self.more_pattern.match(stripped):

                searched = re.search(self.more_pattern, stripped)
                param = float(searched.group(1))
                gathered_constraints.add(res_gallery.GreaterThanConstraint(param))

            elif self.less_pattern.match(stripped):

                searched = re.search(self.less_pattern, stripped)
                param = float(searched.group(1))
                gathered_constraints.add(res_gallery.LessThanConstraint(param))

            elif self.interval_pattern.match(stripped):

                searched = re.search(self.interval_pattern, stripped)
                param1, param2 = float(searched.group(1)), float(searched.group(2))
                gathered_constraints.add(res_gallery.IntervalConstraint(param1, param2))

            else:
                raise ValueError("Restriction string could not be parsed!")

        return gathered_constraints

    def register(self, parameter_name, constraints_string):
        """ The concrete registration method basically using the CustomCompositeConstraint

        :param parameter_name: string, the name of the parameter
        :param constraints_string: string, a string to be parsed for control sequences

        """

        if self.locked:
            raise ValueError("Cannot add any new parameter_names, this object is locked!")

        if parameter_name not in self:
            constraints_set = self.parse_constraints_string(constraints_string)
            parameter_composite_constraint = res_gallery.CustomCompositeConstraint(constraints_set)
            self._parameter_constraints[parameter_name] = parameter_composite_constraint
        else:
            raise ValueError("Attempting to override parameter constraints, best stick to one!")

    def verify_parameter(self, parameter_name, parameter_value):
        """ The concrete method to verify the parameters using the concrete constraints

        :param parameter_name: a string as the parameter name
        :param parameter_value: the value of the parameter (int, float)
        :return: True if parameter passes
        """
        super().check_parameter(parameter_name, parameter_value)
        return self._parameter_constraints[parameter_name].parse_parameter(parameter_value=parameter_value)


class LambdaConstraintsCollection(ConstraintsCollectionABC, ConstraintsCollectionsInterface):

    def register(self, parameter_name, lambda_expression):

        if self.locked:
            raise RuntimeError("Cannot add any new parameter_names, this object is locked!")

        if not callable(lambda_expression):
            raise TypeError("Value of the restriction must be a lambda expression!")

        if parameter_name not in self:
            self._parameter_constraints[parameter_name] = lambda_expression
        else:
            raise ValueError("Attempting to override parameter constraints, best stick to one!")

    def verify_parameter(self, parameter_name, parameter_value):
        """ The concrete method to verify the parameters using the concrete constraints

        :param parameter_name: a string as the parameter name
        :param parameter_value: the value of the parameter (int, float)
        :return: True if parameter passes
        """
        super().check_parameter(parameter_name, parameter_value)
        return self._parameter_constraints[parameter_name](parameter_value)


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
