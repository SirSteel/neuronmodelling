from abc import abstractmethod, ABCMeta
from neuroflow.utilities import verify_type, lambda_code_fingerprint


# region Abstract Base Classes

class ConstraintInterfaceABC(metaclass=ABCMeta):
    """ An interface definition for the constraints, it has to implement methods for comparisons,
    parsing of the parameters, which in turn used the restriction function which is the core of the class itself

    """

    def __init__(self, error_message):
        """
        Each restriction must have an error message to make it useful for the user to see where it failed
        it also hast to be hashable but this is done trought an abstract interface
        """

        verify_type("error_massage", error_message, str)

        self._error_message = error_message

        self._hash = self._hashing_fun()

    def __ne__(self, other):
        """ inequality is defined as the inverse of equality """
        return not self.__eq__(other)

    def __hash__(self):
        """ hash is completely static and assigned at initiation trough an abstract interface """
        return self._hash

    def __eq__(self, other):
        """ equality is based on being subclass of ConstraintInterfaceABC
        and compares the hashes, however they are defined
        """
        if not isinstance(other, ConstraintInterfaceABC):
            raise TypeError("Can only compare constraints with classes "
                            "that are subtype of PureConstraintABC")

        return hash(self) == hash(other)

    def parse_parameter(self, parameter_value) -> bool:
        """
        The concretion of the parsing which basically passes the parameter to the restriction function
        and forwards its return value to the caller

        :param parameter_value: a value to be passed to the parser
        :return: bool if value is within the parameters set by the restriction function
        """
        return self.constraining_function(parameter_value)

    @property
    def error_message(self):
        """ Error message is one of the core properties, if it is not defined return None """
        err_msg = getattr(self, "_error_message", None)
        if err_msg == "":
            return None
        else:
            return err_msg

    @abstractmethod
    def _hashing_fun(self):
        """ should be implemented in subclasses """
        raise NotImplementedError  # pragma: nocover

    @abstractmethod
    def __str__(self):
        """ should be implemented in subclasses """
        raise NotImplementedError  # pragma: nocover

    @abstractmethod
    def __repr__(self):
        """ should be implemented in subclasses """
        raise NotImplementedError  # pragma: nocover

    @property
    @abstractmethod
    def constraining_function(self):
        """ should be implemented in subclasses
        the restriction function must return a CALLABLE!

        """
        raise NotImplementedError  # pragma: nocover


class PureConstraintABC(ConstraintInterfaceABC, metaclass=ABCMeta):
    """ Pure constraints take only one parameter and thus define a
        single function that can take that parameter as a value to compare to
        they already concretize hashing functions but forward the
        restriction function generation to its subclasses

    """

    def __init__(self, parameter, error_message):
        """
        Do initialization checks

        :param parameter: must be an numeric type
        :param error_message: must be a string but is passed to the super() constructor
        """

        verify_type("parameter", parameter, [int, float])
        self._parameter = parameter
        super().__init__(error_message=error_message)

    @property
    def parameter(self):
        """ the parameter property, which is read-only"""
        return self._parameter

    def _hashing_fun(self):
        """
        The concrete hashing function for the PureRestrictions
        basically hashes:
        - name
        - error_message
        - parameter
        - constraining_function fingerprint

        All these combined turn every restriction into a unique enough

        :return: a hash integer value
        """
        name_hash = hash(self.__class__.__name__)
        error_hash = hash(self.error_message)
        param_hash = hash(self.parameter)
        res_hash = hash(lambda_code_fingerprint(self.constraining_function))
        return hash((name_hash, error_hash, param_hash, res_hash))

    def __str__(self):
        """
        A string representation of the restriction

        :return: Returns the name, parameter and error message which already helps define the restriction
        """
        return "{me} >> {err_msg}".format(me=self.__class__.__name__,
                                        param=str(self.parameter),
                                        err_msg=self.error_message)

    def __repr__(self):
        """ __repr__ function for debugging purposes

        it should display the class, its parameter, the error message and the function fingerprint

        """
        return "{me}(parameter={param}, " \
               "error_message={err_msg})".format(me=self.__class__.__name__,
                                                 param=str(self.parameter),
                                                 err_msg=self.error_message,)

    @property
    @abstractmethod
    def constraining_function(self):
        """ should be implemented in subclasses
        the restriction function must return a CALLABLE!

        """
        raise NotImplementedError  # pragma: nocover


class CompositeConstraintABC(ConstraintInterfaceABC, metaclass=ABCMeta):
    """ A composite restriction is defined as such that it is composed of a bunch of PureRestrictions
    """

    def __init__(self, constraints_list, error_message):
        """
        Initializes the Composite Restriction,
        which takes an error message, and a list or set of ConstraintInterfaceABC objects

        TODO: Should implement a decomposition function to turn it into PureRestriction objects
        TODO: and then use set to keep uniques


        :param constraints_list: list or set of ConstraintInterfaceABC objects
        :param error_message: a string of the error describing why it failed
        """

        verify_type("constraints_list", constraints_list, [list, set])

        # make sure all constraints are of correct type
        # for res in constraints_list:
        #     if not isinstance(res, ConstraintInterfaceABC):
        #         raise TypeError("Composed constraints must be of RestrictionsABC class")

        self._composed_constraints = frozenset(self.decompose_constraints(constraints_list))

        super().__init__(error_message=error_message)

    def __repr__(self):
        return "{me}(constraints_list=[<#{constr}>], " \
               "error_message={err_msg})".format(me=self.__class__.__name__,
                                                 constr=len(self.composed_constraints),
                                                 err_msg=str(self.error_message))

    def __str__(self):
        return "{me}<{constr}>> err: {err_msg}".format(me=self.__class__.__name__,
                                                      constr=len(self.composed_constraints),
                                                      err_msg=str(self.error_message))

    @property
    def composed_constraints(self):
        return self._composed_constraints

    def _hashing_fun(self):
        return hash(self.composed_constraints)

    @property
    def constraining_function(self):
        def map_constraints(param_val):
            return all(res.parse_parameter(param_val) for res in self._composed_constraints)
        return map_constraints

    @classmethod
    def decompose_constraints(cls, constraints_list) -> set:
        """ This method basically decomposes the constraints into its PureRestriction components

        It basically adds to a set to ensure uniquness, which is ensured by equality comparisons, and hashes of
        pure constraints. Any further composed constraints are decomposed recursivelly.

        This makes things more efficient

        """
        decomposed_restrictions = set()
        for res_obj in constraints_list:
            if not isinstance(res_obj, ConstraintInterfaceABC):
                raise TypeError("The list should only contain ConstraintInterfaceABC subclass objects")

            if isinstance(res_obj, PureConstraintABC):

                decomposed_restrictions.add(res_obj)

            elif isinstance(res_obj, CompositeConstraintABC):
                decomposed_restrictions.update(res_obj.decompose_constraints(res_obj.composed_constraints))

            else:
                raise NotImplementedError("Was not able to decompose type: <{}>".format(str(type(res_obj))))

        return decomposed_restrictions

# endregion

# region Pure Constraints


class ExactValueConstraint(PureConstraintABC):

    def __init__(self, parameter):
        error_message = "Value must be: {}".format(parameter)
        super().__init__(parameter=parameter, error_message=error_message)

    @property
    def constraining_function(self):
        def func(param_val):
            return param_val == self.parameter
        return func


class NotExactValueConstraint(PureConstraintABC):

    def __init__(self, parameter):
        error_message = "Value must not be: {}".format(parameter)
        super().__init__(parameter=parameter, error_message=error_message)

    @property
    def constraining_function(self):
        def func(param_val):
            return param_val != self.parameter
        return func


class GreaterThanConstraint(PureConstraintABC):
    def __init__(self, parameter):
        error_message = "Value must be greater than: {}".format(parameter)
        super().__init__(parameter=parameter, error_message=error_message)

    @property
    def constraining_function(self):
        def func(param_val):
            return param_val > self.parameter
        return func


class LessThanConstraint(PureConstraintABC):
    def __init__(self, parameter):
        error_message = "Value must be less than: {}".format(parameter)
        super().__init__(parameter=parameter, error_message=error_message)

    @property
    def constraining_function(self):
        def func(param_val):
            return param_val < self.parameter
        return func


class PositiveConstraint(GreaterThanConstraint):
    def __init__(self):
        error_message = "Value must be positive!"
        super(GreaterThanConstraint, self).__init__(parameter=0, error_message=error_message)


class NegativeConstraint(LessThanConstraint):
    def __init__(self):
        error_message = "Value must be negative!"
        super(LessThanConstraint, self).__init__(parameter=0, error_message=error_message)
        

class NonZeroConstraint(NotExactValueConstraint):
    def __init__(self):
        super().__init__(parameter=0)
        
# endregion
    
# region Composite Constraints


class IntervalConstraint(CompositeConstraintABC):
    def __init__(self, lower_bound, upper_bound):
        
        greater_than_res = GreaterThanConstraint(lower_bound)
        
        less_than_res = LessThanConstraint(upper_bound)
        
        error_message = "Value must be between " \
                        "<{lower}> and <{upper}>".format(lower=lower_bound,
                                                         upper=upper_bound)
        
        super().__init__(constraints_list=[greater_than_res, less_than_res],
                         error_message=error_message)


class CustomCompositeConstraint(CompositeConstraintABC):
    def __init__(self, restrictions_list, error_message=""):
        super().__init__(constraints_list=restrictions_list, error_message=error_message)

    def extract_constraint_error(self, parameter_value):
        """
        This function extracts the errors where the parameters failed to provide information to the user

        :param parameter_value: some numeric value of the parameters
        :return: a list of error strings
        """
        accum_errors = []
        for constraint in self.composed_constraints:
            if not constraint.parse_parameter(parameter_value):
                accum_errors.append(constraint.error_message)
        return list(set(accum_errors))
        
# endregion


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
