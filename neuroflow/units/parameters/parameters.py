from abc import ABC, abstractmethod, ABCMeta
from collections import OrderedDict
from collections.abc import MutableMapping
from neuroflow.units.parameters.constraints_collections import ConstraintsCollectionABC
from neuroflow.utilities import verify_type


class ParametersStoreABC(MutableMapping, ABC):

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    @abstractmethod
    def __new__(cls, *args, **kwargs):
        raise NotImplementedError  # pragma: no cover

    # region Getter,Setter,Deleter

    def __getitem__(self, parameter_name):
        """
        The getter function checks existance, returns parameter if it exists
        otherwise throws error

        :param parameter_name: The name of the parameter
        :return:
        """
        verify_type("parameter_name", parameter_name, str)

        if parameter_name in self:
            return getattr(self, "store")[parameter_name]
        else:
            raise KeyError("No such parameter exists!")

    def __setitem__(self, parameter_name, parameter_value):
        """
        The setter function, first check if parameter exists,
        we only allow setting of existing parameter_names
        second verify parameter using the abstract function "verify_parameter"
        if verification fails, handle it using the appropriate function "handle_param_set_errors"

        :param parameter_name: The name of the parameter to be set
        :param parameter_value: The value of the parameter to be set
        :return: None
        """

        verify_type("parameter_name", parameter_name, str)
        verify_type("parameter_value", parameter_value, [int, float])

        # check existence
        if parameter_name in self:

            # verify the parameter
            if self.verify_parameter(parameter_name, parameter_value):

                # set the parameter
                getattr(self, "store")[parameter_name] = parameter_value
            else:
                # handle the errors
                self._handle_param_set_errors(parameter_name, parameter_value)
        else:
            # handle non-existing parameter_names
            raise KeyError("Parameter does not exist, adding new parameter_names is not accepted!")

    def __delitem__(self, parameter_name):
        """ Should not delete parameters """
        raise AttributeError("Cannot delete parameter_names")

    # endregion Getter,Setter,Deleter

    # region Collection, Iterator protocols

    def __len__(self):
        """ Get the number of parameters in the store! """
        return len(getattr(self, "store"))

    def __contains__(self, item):
        """ Check if store contains parameter """
        return item in getattr(self, "store")

    def __iter__(self):
        """ Return an iterable from the parameters """
        return iter(self.store)

    # endregion

    def __eq__(self, other):
        """ Comparison between two parameter sets:
         - They both have to be subclases of ParametersStoreABC
         - The have to have the same constraints
         - They have to match the values

        :param other: The other ParametersStoreABC subclass
        :return: True if all matches, otherwise false
        """

        # not of the parameter_names type, cannot compare such
        if not isinstance(other, ParametersStoreABC):
            raise TypeError("Cannot compare ParametersStore type with {}".format(type(other)))

        # the constraints are different
        elif not self.compare_constraints(other):
            return False

        # the values are different
        elif not self.compare_parameter_values(other):
            return False

        # they are equal
        else:
            return True

    def __ne__(self, other):
        """ inequality is defined as an inverse to equality """
        return not self.__eq__(other)

    # endregion MagicMethods

    ###########################
    # Public Concrete Methods #
    ###########################

    # region Concrete methods

    @property
    def unpack(self) -> dict:
        """ The unpacking function, to store the parameters
        TODO: Maybe should implement the option to pack-unload meaning constraints need to be kept as well
        """
        return self.store.copy()

    @property
    def parameter_names(self):
        """ Get all the names of the parameters in the store! """
        return self.store.keys()  # pragma: nocover

    def compare_parameter_values(self, other):
        return all(self.store[other_key] == other_val for other_key, other_val in other.store.items())

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region Abstractions

    @abstractmethod
    def __repr__(self):
        """ must implement a repr function """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def __str__(self):
        """ Must implement a str method """
        raise NotImplementedError   # pragma: no cover

    @property
    @abstractmethod
    def store(self) -> dict:
        """ Store is implemented in any way, however it has to be a dict type"""
        raise NotImplementedError  # pragma: no cover

    @property
    @abstractmethod
    def constraints(self):
        """ Restrictions can have their own implementation but must be subtype of RestrictionsABC"""
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def compare_constraints(self, other):
        """ Function that enables the comparison between parameter values """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def verify_parameter(self, parameter_name, parameter_value):
        """ The interface to the parameter verification algorithm """
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def _handle_param_set_errors(self, parameter_name, parameter_value):
        """ The way to handle how setting errors are handled """
        raise NotImplementedError  # pragma: no cover

    # endregion


class ParametersStore(ParametersStoreABC):

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __new__(cls, constraints, parameters: dict, *args, **kwargs):

        instance = super(ParametersStoreABC, cls).__new__(cls)

        # region Check Restrictions

        # make sure parameter constraints are correctly structured
        if not isinstance(constraints, ConstraintsCollectionABC):
            raise TypeError("constraints must be ConstraintsCollectionABC subclass!")

        elif not isinstance(parameters, dict):
            raise TypeError("arameters must be a dict")

        else:
            setattr(instance, "_constraints", constraints)
            instance.constraints.locked = True

        # endregion

        # region Check Passed ParametersStore

        try:
            parameters = instance._check_passed_params(parameters)

        except ValueError as error:
            raise ValueError("ParametersStore had invalid values!") from error

        # endregion

        # region Verify ParametersStore and populate

        # create the parameter_names dict, to later populate
        setattr(instance, "_store", OrderedDict())

        # populate the parameter_names dictionary checking all the parameter_names
        for parameter_name, parameter_value in parameters.items():

            try:
                parameter_ok = instance.constraints.verify_parameter(parameter_name, parameter_value)

                if not parameter_ok:
                    curr_param_res = instance.constraints.parameter_constraints[parameter_name]
                    err_mes = curr_param_res.error_message
                    if err_mes is not None:
                        raise ValueError(err_mes)  # pragma: no cover / really not needed now
                    else:
                        raise ValueError(" \n ".join(curr_param_res.extract_constraint_error(parameter_value)))

            except (TypeError, ValueError, KeyError) as error:

                error_massage = "Invalid parameter_names passed to parameter constructor! " \
                                "Parameter <{param_name}> " \
                                "of type <{param_type}> " \
                                "with the value <{param_val}> failed".format(param_name=parameter_name,
                                                                             param_type=type(parameter_value),
                                                                             param_val=parameter_value)

                raise ValueError(error_massage) from error

            else:

                getattr(instance, "_store")[parameter_name] = parameter_value

        # endregion

        return instance

    def __repr__(self):
        """ the representation of the constraints how they would have been instantiated """
        return "ParametersStore(constraints=<{res_obj_type}>, " \
               "**parameters)".format(res_obj_type=self.constraints.__class__.__name__)

    def __str__(self):
        """
        The string representation of the parameters, displaying parameter values

        :return: "param_name=param_value, ..."
        """
        return ", ".join("{}={}".format(parameter_name, str(parameter_value))
                         for parameter_name, parameter_value in self.store.items())

    # endregion

    ##################
    # Public Methods #
    ##################

    # region PublicMethods

    @property
    def constraints(self):
        return getattr(self, "_constraints")

    def compare_constraints(self, other):
        return self.constraints == other.constraints

    @property
    def store(self) -> dict:
        return getattr(self, "_store")

    def verify_parameter(self, parameter_name, parameter_value):
        return self.constraints.verify_parameter(parameter_name, parameter_value)

    # endregion

    #########################
    # Private Class Methods #
    #########################

    # region PrivateClassMethods

    def _check_passed_params(self, parameters):

        expected_args = set(self.constraints.parameter_names)
        passed_args = set(parameters.keys())

        # get the symmetric difference
        set_difference = len(passed_args.symmetric_difference(expected_args))

        # if sets are same, arguments are consistent pass the function forward
        if set_difference == 0:

            return parameters

        # if sets are different be useful, and find out the difference, to assist debugging
        else:
            missing_args = expected_args - passed_args
            excess_args = passed_args - expected_args

            if len(missing_args) > 0 and len(excess_args) > 0:
                raise ValueError("Missing parameter_names: < {miss} >, "
                                 "Unexpected parameter_names: < {unexp} >".format(miss="|".join(missing_args),
                                                                             unexp="|".join(excess_args)))

            elif len(missing_args) > 0:
                raise ValueError("Missing parameter_names: < {miss} >".format(miss="|".join(missing_args)))

            elif len(excess_args) > 0:
                raise ValueError("Unexpected parameter_names: < {unexp} >".format(unexp="|".join(excess_args)))

    def _handle_param_set_errors(self, parameter_name, parameter_value):
        curr_param_res = self.constraints.parameter_constraints[parameter_name]
        err_mes = curr_param_res.error_message
        if err_mes is not None:
            raise ValueError(err_mes)  # pragma: no cover / really not needed now
        else:
            raise ValueError(" \n ".join(curr_param_res.extract_constraint_error(parameter_value)))

    # endregion


class Subject(metaclass=ABCMeta):

    @abstractmethod
    def attach(self, observer):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def detach(self, observer):
        raise NotImplementedError  # pragma: no cover

    @abstractmethod
    def notify(self):
        raise NotImplementedError  # pragma: no cover


class ParametersStoreObserved(ParametersStore, Subject):
    def __new__(cls, constraints, parameters: dict, *args, **kwargs):
        pass   # pragma: no cover

    def attach(self, observer):
        pass   # pragma: no cover

    def detach(self, observer):
        pass   # pragma: no cover

    def notify(self):
        pass   # pragma: no cover


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover
