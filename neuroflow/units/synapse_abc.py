from abc import abstractmethod, ABCMeta
from collections import namedtuple
from neuroflow.utilities import verify_type
from neuroflow.units import unit_abc


class SynapseABC(unit_abc.ModelUnitABC, metaclass=ABCMeta):
    """ The synapse base class, implements the naming conventions (post, pre), uid creation
       repr and strings, comparisons , unpacking,loading and properties """

    SynapseUnitForm = namedtuple('SynapseUnitForm', ['type', 'uid', 'pre', 'post', 'parameters'])
    SynapseUnitID = namedtuple('SynapseUnitID', ['uid', 'pre', 'post'])

    #################
    # Magic Methods #
    #################

    # region MagicMethods

    def __new__(cls, uid, pre, post, **kwargs):
        """ The instance creation method

        It uses its base class ModelUnitABC, to initialize the parameters,
        and only sets the pre synaptic neuron name and post synaptic neuron name to the object

        :param pre: A string, as a name of the pre synaptic neuron
        :param post: A string, as a name of the post synaptic neuron
        :param kwargs: The parameters of the synapse
        :return: A new synapse unit instance
        """

        # check that UID is an int
        verify_type("uid", uid, int)

        # check if pre is a string
        verify_type("pre", pre, str)

        # check if post is a string
        verify_type("post", post, str)

        instance = super().__new__(cls, parameters=kwargs)
        setattr(instance, "_pre", pre)
        setattr(instance, "_post", post)
        setattr(instance, "_uid", uid)
        return instance

    def __repr__(self):
        """ A representation of the synapse, used mostly of developer purposes
        Basically a representation of how the synapse was instantiated

        :return: something in the lines of SynapseType(pre='AVA', post='AVB', I_max=0.5, E=2)
        """

        repr_string = '{synapse_type}(pre="{pre}", post="{post}", ' \
                      '**parameters <{params}>)'.format(synapse_type=self.__class__.__name__,
                                                        pre=self.pre,
                                                        post=self.post,
                                                        params=str(self.parameters))
        return repr_string

    def __str__(self):
        """ A string representation of the synapse"""
        repr_string = " ({pre} -> {post}) " \
                      "<{synapse_type}({params})>".format(synapse_type=self.__class__.__name__,
                                                          pre=self.__getattribute__("pre"),
                                                          post=self.__getattribute__("post"),
                                                          params=", ".join(self.expected_parameters()))

        return repr_string

    def __eq__(self, other):
        """ The comparison function to compare synapse classes
        - Must be of SynapseABC class first
        - Must be same class Type
        - Must have the post and pre synaptic neurons
        - Must have the same UID
        - Must have the same ParametersStore (this falls back to parameters comparison function)

        :param other: The other NeuronABC subclass
        :return: True if the objects match
        """

        # not SynapseABC
        if not issubclass(other.__class__, SynapseABC):
            raise TypeError("Cannot compare types which are not NeuronABC subclasses")
        #
        # not the same type
        elif self.__class__.__name__ != other.__class__.__name__:
            return False
        #
        # not the same post, pre or uid
        elif self.post != other.post or \
                self.pre != other.pre or \
                self.uid != other.uid:

            return False
        #
        # not the same parameters
        elif self.parameters != other.parameters:
            return False

        # if it all passes it is the same neuron
        else:
            return True

    # endregion

    #################
    #  Properties   #
    #################

    # region Properties

    @property
    def post(self):
        """ Post synaptic neuron name is the main property of the synapse """
        return getattr(self, "_post")

    @property
    def pre(self):
        """ pre synaptic neuron name is the main property of the synapse """
        return getattr(self, "_pre")

    @property
    def uid(self):
        """ since synapses might have many similar connection, UIDs are assigned to each"""
        return getattr(self, "_uid")

    @property
    def unpack(self) -> tuple:
        """ Unpacking function to extract pre and post neurons plus parameter_names

        :return: A tuple with pre-synaptic-neuron name, post-synaptic-neuron name, and parameter_names
        """
        return self.SynapseUnitForm(type=self.__class__.__name__,
                                    uid=self.uid,
                                    pre=self.pre,
                                    post=self.post,
                                    parameters=self.parameters.unpack)

    @property
    def get_id(self) -> tuple:
        return self.SynapseUnitID(uid=self.uid, pre=self.pre, post=self.post)

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region AbstractMethods

    @classmethod
    @abstractmethod
    def parameter_constraints(cls):
        """
        This function should basically return a parameter constraints object, which will be used at instantiation
        each class type thus defines its own constraints on the parameters shared across all instances

        :return: RestrictionsABC object
        """
        raise NotImplementedError  # pragma: nocover

    @staticmethod
    @abstractmethod
    def main_equation(**kwargs):
        """ The main equation of the ModelUnit

        It must be implemented as a class method since its class will have one such function and might reference other
        functions defined in itself, also the function can then be implemented to be vectorized!
        """
        raise NotImplementedError  # pragma: nocover

    # endregion


if __name__ == "__main__":
    raise RuntimeError("This module should not be called directly!")  # pragma: nocover
