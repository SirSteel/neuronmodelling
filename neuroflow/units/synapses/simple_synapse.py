import numpy as np
from neuroflow import units
from neuroflow.units import parameters as param_pkg


class SimpleSynapse(units.SynapseABC):

    @staticmethod
    def main_equation(g_max, tau, lambda_i):
        np.sum([g_max, tau, lambda_i])

    @classmethod
    def parameter_constraints(cls) -> dict:
        with param_pkg.PreparedConstraintsCollection() as rest:
            rest.register("g_max", "not0")
            rest.register("tau", "not0")
            rest.register("lambda_i", "not0")
            return rest


if __name__ == '__main__':
    raise RuntimeError("This should not be run directly!")  # pragma: nocover
