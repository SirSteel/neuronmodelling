from abc import abstractmethod, ABCMeta
from neuroflow.units import parameters as param_pkg


class ModelUnitABC(metaclass=ABCMeta):
    """ The Abstract Base Class for architect units
    This is the abstract class for all architect units (neurons, synapses), it implements the common methods
    such as parameter instantiation and related methods. It defines the basic interface and the methods that
    the subclasses (neuronABC and synapseABC) need to implement or pass implementation to their subclasses

    """

    #################
    # Magic Methods #
    #################

    # region MagicMethods
    def __new__(cls, parameters, *args, **kwargs):
        """
        This method implements the creation of a ModelUnit, which mostly involves the initialization of the parameters

        :param parameters: A dictionary holding parameter names and their values
        :param args: Optional positional arguments
        :param kwargs: Optional keyword arguments
        :return: A new instance of ModelUnit
        """
        try:
            # create the parameter_names object
            parameters = param_pkg.ParametersStore(cls.parameter_constraints(), parameters)

        except TypeError as error:
            raise TypeError("Parameter initialization failed!") from error

        except ValueError as error:
            raise ValueError("Parameter initialization failed!") from error

        else:
            instance = super().__new__(cls)
            setattr(instance, "_parameters", parameters)
            return instance

    def __hash__(self):
        """
        Since ModelUnit-s change over time, hashing is not permitted!

        :return:
        """
        raise TypeError("<{me}> is a mutable datatype!".format(me=self.__name__))  # pragma: no cover

    def __ne__(self, other):
        """ Inequality is defined as an inverse of equality, which is abstracted """
        return not self.__eq__(other)  # pragma: no cover

    # endregion

    #################
    #  Properties   #
    #################

    # region Properties

    @property
    def parameters(self):
        """ ParametersStore are the main property of all ModelUnits """
        return getattr(self, "_parameters", None)

    @parameters.setter
    def parameters(self, new_parameters):
        """
        This is the main setter function for parameters, which checks for correct new type
        and whether the values have changed. The constraints must not change, since they are inherent to the unit

        :param new_parameters: The new parameters object
        :return: None
        """

        # check if the instance is of correct type
        if not isinstance(new_parameters, param_pkg.ParametersStoreABC):
            raise TypeError("This can only set objects of type Parameter!")

        # use shortcutting of bool operations, it will be false if it does not exist
        # so the second test only happens if it does exist
        elif not self.parameters.compare_constraints(new_parameters):
                raise ValueError("The constraints of the parameter_names do not match!")

        # if it is either the same object, or completely new set it
        else:
            setattr(self, "_parameters", new_parameters)

    @parameters.deleter
    def parameters(self):
        """ ParametersStore cannot be deleted! """
        raise AttributeError("Cannot delete the parameter_names property")  # pragma: no cover

    # endregion

    ###########################
    # Concrete Public Methods #
    ###########################

    # region ConcretePublicMethods

    @classmethod
    def expected_parameters(cls) -> set:
        """ Get the names of the expected parameters! """
        return set(getattr(cls.parameter_constraints(), "parameter_names"))

    # endregion

    ####################
    # Abstract Methods #
    ####################

    # region AbstractMethods

    @abstractmethod
    def __repr__(self):
        """ A representation function must be implemented for all units! """
        raise NotImplementedError   # pragma: nocover

    @abstractmethod
    def __str__(self):
        """ A string representation should be defined, but can fallback on __repr__"""
        raise NotImplementedError   # pragma: nocover

    @abstractmethod
    def __eq__(self, other):
        """ Equality between objects must be defined since it is important to compare ModelUnits """
        raise NotImplementedError   # pragma: nocover

    @property
    @abstractmethod
    def unpack(self) -> tuple:
        """ Unpacking must be defined for ModelUnits to assist in their storage! """
        raise NotImplementedError    # pragma: nocover

    @classmethod
    @abstractmethod
    def main_equation(cls, *args, **kwargs):
        """ The main equation of the ModelUnit

        It must be implemented as a class method since its class will have one such function and might reference other
        functions defined in itself, also the function can then be implemented to be vectorized!

        """
        raise NotImplementedError   # pragma: nocover

    @classmethod
    @abstractmethod
    def parameter_constraints(cls):
        """
        This function should basically return a parameter constraints object, which will be used at instantiation
        each class type thus defines its own constraints on the parameters shared across all instances

        :return: RestrictionsABC object
        """
        raise NotImplementedError  # pragma: nocover

    # endregion


if __name__ == "__main__":
    raise RuntimeError("This module should not be called directly!")  # pragma: nocover
