import inspect
import numpy as np
from abc import abstractmethod, ABCMeta

from neuroflow import units

from neuroflow import utilities


class FactoryABC(metaclass=ABCMeta):

    DEFAULT_UNIT_LOCATIONS = "/home/lzelezni/PycharmProjects/neuroflow/neuroflow/units"
    # TODO: With setup these values should be in a config file

    def __new__(cls,
                factory_superclass,
                module_importer,
                unit_locations=DEFAULT_UNIT_LOCATIONS,
                **kwargs):

        if not inspect.isclass(factory_superclass):
            raise TypeError("Factory must be provided a factory_superclass which must be a class object!")

        if not (inspect.isclass(module_importer) and issubclass(module_importer, utilities.ModuleImporterInterface)):
            raise TypeError("module_importer must be a subclass of ModuleImporterInterface!")

        new_instance = super().__new__(cls)

        setattr(new_instance, "_factory_superclass", factory_superclass)
        setattr(new_instance, "_importer", module_importer(path_or_paths=unit_locations,
                                                           desired_subclass=factory_superclass,
                                                           **kwargs))

        return new_instance

    @property
    def importer(self):
        return getattr(self, "_importer")

    @property
    def factory_superclass(self):
        return getattr(self, "_factory_superclass")

    @abstractmethod
    def create_unit(self, *args, **kwargs):
        raise NotImplementedError  # pragma: nocover

    def get_unit(self, desired_type: str):
        try:
            return self.importer.get_class(desired_type)
        except KeyError as error:
            raise ValueError("Failed creating unit! No such unit class was found to be defined!") from error


class NeuronFactory(FactoryABC):

    def __new__(cls):
        return super().__new__(cls,
                               factory_superclass=units.NeuronABC,
                               module_importer=utilities.ModuleImporter)

    def create_unit(self, neuron_type: str, neuron_name: str, **neuron_parameters):
        return self.get_unit(neuron_type)(neuron_name, **neuron_parameters)


class SynapseFactory(FactoryABC):

    def __new__(cls):
        new_instance = super().__new__(cls,
                                       factory_superclass=units.SynapseABC,
                                       module_importer=utilities.ModuleImporter)
        setattr(new_instance, "_taken_uids", set())
        return new_instance

    @property
    def taken_uids(self):
        return getattr(self, "_taken_uids")

    def _create_new_uid(self) -> int:
        """ Creates new UIDs for synapses, by random, and checks for existence so that they are really unique

        :return: an integer new id
        """
        while True:
            new_uid = np.random.randint(1, 10000)
            if new_uid not in self.taken_uids:
                return new_uid

    def create_unit(self, synapse_type: str, pre: str, post: str, uid=None, **synapse_parameters):
        if uid is None:
            uid = self._create_new_uid()
        else:
            utilities.verify_type("uid", uid, int)
            if uid in self.taken_uids:
                raise ValueError("This UID is already assigned to another synapse!")
            else:
                self.taken_uids.add(uid)

        return self.get_unit(synapse_type)(uid=uid, pre=pre, post=post, **synapse_parameters)


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")  # pragma: no cover


