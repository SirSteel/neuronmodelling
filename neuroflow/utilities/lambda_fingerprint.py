""" This module contains the function to get the lambda code to be able to compare lambdas among each other

Example:
    >>> lx = lambda x: x**2
    >>> ly = lambda y: y**2
    >>> lz = lambda x: x**3

    >>> assert lambda_code_fingerprint(lx) == lambda_code_fingerprint(ly)
    True
    >>> assert lambda_code_fingerprint(lx) == lambda_code_fingerprint(lz)
    False

"""

import operator


def lambda_code_fingerprint(current_lambda):
    code_sig = operator.attrgetter('co_code', 'co_consts')
    return code_sig(current_lambda.__code__)


if __name__ == "__main__":
    raise RuntimeError("This module should not be called directly!")  # pragma: nocover
