""" This module contains the module importer class which can be used to find other

"""
import os
import inspect
import importlib
import importlib.util
from abc import abstractmethod, ABCMeta
from itertools import chain
from functools import partial
from collections import OrderedDict

# region Globals defined

# TODO: Needs to be implemented with a config file
DEFAULT_MOD_EXT = frozenset(['.py', '.pyc', '.pyo'])
DEFAULT_IGN_FILES = frozenset(["__init__", ])
DEFAULT_IGN_FOLDERS = frozenset(["__pycache__", ".cache", "venv", ])

# endregion

# region Explorer Functions


def _is_valid_module(filename, module_extensions=DEFAULT_MOD_EXT, ignored_files=DEFAULT_IGN_FILES):
    """ Checks if the file is a valid python module, based on file extensions and list of ignored filenames

    Checks does file start with any ignored filename, return False for such
    Checks does file have a correct extension, return False if not
    Otherwise return True

    :param filename: The name of the file which to check
    :param module_extensions: Valid extensions
    :param ignored_files: Which files should we ignore
    :return: Returns True if it is a module
    """
    return not any(filename.startswith(ign_name) for ign_name in ignored_files) and \
               any(filename.endswith(ext) for ext in module_extensions)


def _is_valid_path(full_path, ignored_folders=DEFAULT_IGN_FOLDERS):
    """ Given a collection of excluded elements, will check if path contains any of them

    :param full_path: a path string or a path-like python object
    :param ignored_folders: Which folders to ignore
    :return: returns True if it contains excluded
    """

    return not any(path_element in ignored_folders
               for path_element in
               os.path.normpath(full_path).split(os.path.sep))


def get_modules_in_folder(folder_path,
                          with_paths=True,
                          module_extensions=DEFAULT_MOD_EXT,
                          ignored_files=DEFAULT_IGN_FILES):
    """


    :param folder_path:
    :param with_paths:
    :param ignored_files:
    :param module_extensions:
    :return:
    """
    if not os.path.isdir(folder_path):
        raise IOError("The path provided is invalid: {}".format(folder_path))

    module_filter = partial(_is_valid_module, module_extensions=module_extensions, ignored_files=ignored_files)

    if with_paths:

        return list(filter(lambda element: module_filter(element[0]),
                           ((mod_name, os.path.join(folder_path, mod_name))
                            for mod_name in os.listdir(folder_path))))
    else:

        return list(filter(module_filter, os.listdir(folder_path)))


def get_modules_in_subfolders(folder_path,
                              with_paths=True,
                              module_extensions=DEFAULT_MOD_EXT,
                              ignored_files=DEFAULT_IGN_FILES,
                              ignored_folders=DEFAULT_IGN_FOLDERS):
    """ Gets all the python modules in folder, excluding any that might be in the ignores list


    :param folder_path: Can be a string or a path-like object
    :param with_paths: If true it returns module names together with their paths
    :param module_extensions: Valid extensions
    :param ignored_files: Which files should we ignore
    :param ignored_folders: Which folders to ignore
    :return: returns a list of all modules in given folder
    """
    if not os.path.isdir(folder_path):
        raise IOError("The path provided is invalid: {}".format(folder_path))

    path_root_filter = partial(_is_valid_path, ignored_folders=ignored_folders)
    filtered_folder_roots = filter(path_root_filter, chain(folder_root for folder_root, _, _ in os.walk(folder_path)))

    module_getter = partial(get_modules_in_folder,
                            with_paths=with_paths,
                            module_extensions=module_extensions,
                            ignored_files=ignored_files)

    # return the flattened list of all the module files in the folder and its subfolders
    return list(chain(*map(module_getter, filtered_folder_roots)))


def get_module_classes(module_path):
    """ Return a module object, given a path

    :param module_path: A path string or a path-like object
    :return: module_obj
    """
    # separate the root folder path from the filename
    path, filename = os.path.split(module_path)

    # get the module name, removing the file extension
    module_name, _ = os.path.splitext(filename)

    # get the spec from the filename and path
    spec = importlib.util.spec_from_file_location(module_name, module_path)
    module = importlib.util.module_from_spec(spec)

    try:
        spec.loader.exec_module(module)
    except Exception as error:
        raise Exception("An error occurred while trying to execute the module {mod_name} at {mod_loc}"
                        " to parse its class definitions".format(mod_name=module_name,
                                                                 mod_loc=module_path)) from error
    else:
        return inspect.getmembers(module, inspect.isclass)

# endregion


class ModuleImporterInterface(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_class(self, class_name):
        raise NotImplementedError


class ModuleImporter(ModuleImporterInterface):

    def __init__(self,
                 path_or_paths,
                 desired_subclass,
                 go_in_subfolders=True,
                 module_extensions=None,
                 ignored_files=None,
                 ignored_folders=None
                 ):
        super().__init__()

        self._go_in_subfolders = go_in_subfolders
        self._desired_subclass = desired_subclass

        if module_extensions:
            setattr(self.__module__, "DEFAULT_MOD_EXT", module_extensions)

        if ignored_files:
            setattr(self.__module__, "DEFAULT_IGN_FILES", ignored_files)

        if ignored_folders:
            setattr(self.__module__, "DEFAULT_IGN_FOLDERS", ignored_folders)

        if isinstance(path_or_paths, list):
            raise NotImplementedError("This feature is not yet implemented!")
            # TODO: Implement this for lists, just check for conflicts
        else:
            self._class_locations = self._create_class_dict(path_or_paths)

    def _has_class(self, class_name):
        return class_name in self._class_locations

    def get_class(self, class_name):
        if not self._has_class(class_name):
            raise KeyError("The class was not found on the path tree!")
        else:

            # get the filepath from the initially populated tree!
            class_source_module_location = self._class_locations[class_name]

            # get the name of the module, by splitting away the root and extension
            module_name = os.path.splitext(os.path.split(class_source_module_location)[1])[0]

            # importlib syntax!
            spec = importlib.util.spec_from_file_location(module_name, class_source_module_location)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)

            # return the attribute from the module
            return getattr(module, class_name)

    def _create_class_dict(self, path) -> dict:

        class_dictionary = OrderedDict()

        if self._go_in_subfolders:
            modules_on_path = get_modules_in_subfolders(path)
        else:
            modules_on_path = get_modules_in_folder(path)

        for module_name, module_path in modules_on_path:
            for class_obj_name, _ in get_module_classes(module_path):
                if class_obj_name not in class_dictionary:
                    class_dictionary[class_obj_name] = module_path

                    # if class_obj_name in class_dictionary:
                    #     # error_message = "Conflict in the path tree! " \
                    #     #                 "There should not be classes with identical names on the path!" \
                    #     #                 "Conflict was caused by <{curr_class}> at <{corr_cls_loc}>" \
                    #     #                 "which overlaps with identical class at <{overlap_loc}>" \
                    #     #                 "".format(curr_class=class_obj_name,
                    #     #                           corr_cls_loc=module_path,
                    #     #                           overlap_loc=class_dictionary[class_obj_name])
                    #     #
                    #     # raise KeyError(error_message)
                    #     # class_dictionary[class_obj_name].append(module_path)
                    #     continue
                    # else:
                    #     class_dictionary[class_obj_name] = module_path

        return class_dictionary


if __name__ == "__main__":
    raise RuntimeError("This module should not be run directly!")
