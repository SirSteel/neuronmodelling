import pandas as pd
from abc import ABCMeta, abstractmethod


class StorageManagerInterface(metaclass=ABCMeta):

    """
    Usage:

    self.model_storer.save_model(save_to_location=save_to_location,
                                    neuron_ensembles=self.neuron_ensembles,
                                    synapse_ensembles=self.synapse_ensembles)
    """
    @abstractmethod
    def save_dataframe(self, dataframe_name, dataframe_obj):
        raise NotImplementedError

    @abstractmethod
    def load_dataframe(self, dataframe_name):
        raise NotImplementedError

    @abstractmethod
    def remove_dataframe(self, dataframe_name):
        raise NotImplementedError

    @property
    @abstractmethod
    def storage_location(self):
        raise NotImplementedError


class HDF5StorageManager(StorageManagerInterface):

    def __init__(self, hdf5_store_filename):
        setattr(self, "_storage_filename", hdf5_store_filename)

    @property
    def storage_location(self):
        return getattr(self, "_storage_filename")

    @property
    def hdf_store(self):
        return pd.HDFStore(self.storage_location)

    def save_dataframe(self, dataframe_name, dataframe_obj):
        with self.hdf_store as store:
            store[dataframe_name] = dataframe_obj

    def load_dataframe(self, dataframe_name):
        with self.hdf_store as store:
            df = store[dataframe_name]
        return df

    def remove_dataframe(self, dataframe_name):
        with self.hdf_store as store:
            del store[dataframe_name]
