""" This module contains the verifier function which is a helper function used to verify types
and provide the user with helpful error messages

Example:
    >>> verify_type("variable_one", 34, str)
    Traceback (most recent call last):
        ...
    TypeError: The variable <variable_one> must be of type <str>, got <int> instead!
    >>> # if correct type, nothing prints
    >>> verify_type("variable_one", "i am a string!", str)

"""

__all__ = ["verify_type"]

import inspect


def _formatted_error_message(variable_name: str, variable_type: str,  expected_type: str):
    """ This function returns back a formated string for the error message

    Should only be used internally in the module

    :param str variable_name: a string representing the variable name
    :param str variable_type: a string representing the current type of the variable
    :param str expected_type: a string representing the expected type
    :returns str: A formatted message string
    """
    assert isinstance(variable_name, str)
    assert isinstance(variable_type, str)
    assert isinstance(expected_type, str)

    return "The variable <{var_name}> must be of type <{ex_type}>, got <{curr_type}> instead!"\
        .format(var_name=variable_name,
                ex_type=expected_type,
                curr_type=variable_type)


def verify_type(variable_name: str, variable_value, expected_type) -> None:
    """ Verifies whether the variable is of correct type and provides informative error messages

    Example:
        >>> verify_type("variable_one", 34, str)
        Traceback (most recent call last):
            ...
        TypeError: The variable <variable_one> must be of type <str>, got <int> instead!
        >>> # if correct type, nothing prints
        >>> verify_type("variable_one", "i am a string!", str)

    :param variable_name: A string of the name, just to provide a better message
    :param variable_value: Variable value (object passed)
    :param expected_type: The desired type of object
    :raises TypeError: Whenever the type of the variable does not match the expected type
    :return: None
    """
    if not isinstance(variable_name, str):
        raise TypeError("The argument variable_name, must be string!")

    # first check if it is either one class type, or if it is a list
    elif not(isinstance(expected_type, list) or inspect.isclass(expected_type)):

        raise TypeError("The argument expected_type must be either a list, or a class!")

    # if it is not a list it must be a class, therefore check if it is instance, and return the error
    elif not isinstance(expected_type, list) and not isinstance(variable_value, expected_type):

        raise TypeError(_formatted_error_message(variable_name=variable_name,
                                                 variable_type=variable_value.__class__.__name__,
                                                 expected_type=expected_type.__name__))

    # if it is a list, check if all the elements are actually classes
    elif isinstance(expected_type, list) and not all(inspect.isclass(type_el) for type_el in expected_type):

        raise TypeError("If the argument expected_type is a list, it must only contain class elements")

    # lastly if it is a list check if any of the elements matches the instance, if not find the first error
    elif isinstance(expected_type, list) and not any(isinstance(variable_value, type_el) for type_el in expected_type):

        raise TypeError(_formatted_error_message(variable_name=variable_name,
                                                 variable_type=variable_value.__class__.__name__,
                                                 expected_type=", ".join(type_el.__name__ for type_el in expected_type)))


if __name__ == "__main__":
    raise RuntimeError("This module should not be called directly!")  # pragma: nocover
