import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from functools import partial


def rate_of_conductance_change():

    voltages = np.arange(-70, 70, 0.01)

    def fun(voltage, w, c1, c2):
        return (np.tanh((np.divide(voltage - c1, w))) - np.tanh((np.divide(voltage - c2, w)))) / 2.0

    f = partial(fun, w=10**4, c1=-72, c2=-68)
    fv = np.vectorize(f)

    dGdt = np.array(fv(voltages))

    # Note that using plt.subplots below is equivalent to using
    # fig = plt.figure and then ax = fig.add_subplot(111)
    fig, ax = plt.subplots()
    ax.plot(voltages, dGdt)

    ax.set(xlabel='Voltage (mV)', ylabel='dG/dt',
           title='Rate of conductance change')
    ax.grid()

    # fig.savefig("test.png")
    plt.show()


def differential_equations():

    def dgdt(voltage, alpha, beta):
        return (alpha * voltage) + beta

    def model(v1, t, Gleak, C, Vleak):
        mu, sigma = 3., 1.  # mean and standard deviation
        negative_channels = np.random.lognormal(mu, sigma, 1)
        positive_channels = np.random.lognormal(mu, sigma, 1)
        rate_change1 = partial(dgdt, alpha=0.5, beta=70)
        rate_change2 = partial(dgdt, alpha=-0.5, beta=-70)

        g_over_c = Gleak / C
        dydt = g_over_c * (Vleak - v1) - \
               (negative_channels * rate_change1(v1)) + \
               (positive_channels * rate_change2(v1))

        return dydt

    model = partial(model, Gleak=3, C=500, Vleak=-70)

    # initial condition
    y0 = 70

    # time points
    t = np.linspace(0, 2000, 1000)

    # solve ODE
    y = odeint(model, y0, t)

    # plot results
    plt.plot(t, y)
    plt.xlabel('time')
    plt.ylabel('y(t)')
    plt.show()


def main():
    differential_equations()
    # rate_of_conductance_change()


if __name__ == "__main__":
    main()
