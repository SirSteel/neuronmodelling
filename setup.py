from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='neuroflow',

      version='0.1',

      description='Nervous system modelling using machine learning',

      long_description=readme(),

      classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.5',
        'Topic :: Scientific/Engineering :: Neuroscience',
      ],

      keywords='neuron nervous system modelling tensorflow ML machine learning',

      url='https://gitlab.com/SirSteel/neuronmodelling/',

      author='Luka Zeleznik',

      author_email='luka.zeleznik.91@gmail.com',

      license='GNU GPLv3',

      packages=['neuroflow'],

      install_requires=[
          "numpy",
          "scipy",
          "pandas",
          "matplotlib",
          "seaborn",
          "pytest",
          "tensorflow",
      ],

      test_suite='neuroflow.tests',

      tests_require=['pytest'],

      include_package_data=True,

      zip_safe=False)
